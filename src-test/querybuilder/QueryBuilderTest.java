package querybuilder;

import junit.framework.Assert;
import org.junit.Test;
import querybuilder.second.ConditionsList;
import querybuilder.second.Query;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 26.07.12
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class QueryBuilderTest {

    @Test
    public void testTest(){
        QueryBuilder q = new QueryBuilder()
                .getSingle("User").where("Name    ").eq("Peter")
                .and().where("Nachname").eq("Pan")
                .or().where("Alter").lt("12")
                .and().where("Alter").gt("8");

        q.print();
    }

    @Test
    public void alvinQueryTest() {
        Query q = new Query().getSingle().where("Name").isEqual("Peter")
                .and().where("lastName").isEqual("Pan");

        System.out.println(q.toString());
        for(ConditionsList i : q.getList()) {
            System.out.println(i.toString());
        }

        Assert.assertEquals("getSingle" ,q.getList().get(0).getExtension());
        Assert.assertEquals("Name" ,q.getList().get(0).getCompField());
        Assert.assertEquals("eq" ,q.getList().get(0).getCompOp());
        Assert.assertEquals("Peter" ,q.getList().get(0).getCompValue());

        Assert.assertEquals("and" ,q.getList().get(1).getExtension());
        Assert.assertEquals("lastName" ,q.getList().get(1).getCompField());
        Assert.assertEquals("eq", q.getList().get(1).getCompOp());
        Assert.assertEquals("Pan", q.getList().get(1).getCompValue());


        Query p = new Query().getAll().where("Group").greaterThan("Admin")
                .or().where("Permission").lessThan("SuperUser");

        Assert.assertEquals("getAll", p.getList().get(0).getExtension());
        Assert.assertEquals("Group" ,p.getList().get(0).getCompField());
        Assert.assertEquals("gt" ,p.getList().get(0).getCompOp());
        Assert.assertEquals("Admin" ,p.getList().get(0).getCompValue());

        Assert.assertEquals("or" ,p.getList().get(1).getExtension());
        Assert.assertEquals("Permission" ,p.getList().get(1).getCompField());
        Assert.assertEquals("lt" ,p.getList().get(1).getCompOp());
        Assert.assertEquals("SuperUser", p.getList().get(1).getCompValue());

        System.out.println(p.toString());
        for(ConditionsList i : p.getList()) {
            System.out.println(i.toString());
        }
    }
}
