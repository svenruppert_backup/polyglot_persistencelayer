package connectionfactory.rdbmns.impl.jdbcconnection;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 30.08.12
 * Time: 11:37
 * To change this template use File | Settings | File Templates.
 */
public class MySqlJdbcDriverInstantiatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void nullParameterTest() {
        new MySqlJdbcDriverInstantiator().instantiateConnection(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyParameterTest() {
        new MySqlJdbcDriverInstantiator().instantiateConnection("");
    }

    @Test
    public void exceptionTriggerTest() {
        new MySqlJdbcDriverInstantiator().instantiateConnection("failTest");
    }

    @Test
    public void checkNotConnectable() {
        new MySqlJdbcDriverInstantiator().instantiateConnection("http://...");
    }

}
