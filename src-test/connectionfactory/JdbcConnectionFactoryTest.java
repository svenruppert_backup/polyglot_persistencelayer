package connectionfactory;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import junit.framework.Assert;
import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 30.08.12
 * Time: 11:13
 * To change this template use File | Settings | File Templates.
 */
public class JdbcConnectionFactoryTest {
    private static JdbcConnectionFactory jFac;

    @BeforeClass
    public static void setUpOnce() {
        jFac = new JdbcConnectionFactory();
    }

    @Test
    public void setDbTypeTest() {
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        Assert.assertEquals(JdbcConnectionFactory.SQLite, JdbcConnectionFactory.getDbType());
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.MySQL);
        Assert.assertEquals(JdbcConnectionFactory.MySQL, JdbcConnectionFactory.getDbType());
    }

    @Test
    public void closeConnectionNullParamerterTest() {
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.MySQL);
        jFac.closeConnection(null);
    }

    @Test
    public void getAndCloseJdbcConnectionTest() throws SQLException {
//        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.MySQL);
//        Connection conn1 = jFac.getJdbcConnection();
//        Assert.assertNotNull(conn1);
//        jFac.closeConnection(conn1);
//        Assert.assertTrue(conn1.isClosed());

        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        Connection conn2 = jFac.getJdbcConnection();
        Assert.assertNotNull(conn2);
        jFac.closeConnection(conn2);
        Assert.assertTrue(conn2.isClosed());
    }

    @Test
    public void getAndCloseHibernateConnectionTest() throws SQLException {
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.MySQL);
        Session sess1 = jFac.getHibernateSession();
        Assert.assertNotNull(sess1);
        jFac.closeSession(sess1);
        Assert.assertFalse(sess1.isOpen());

        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        Session sess2 = jFac.getHibernateSession();
        Assert.assertNotNull(sess2);
        jFac.closeSession(sess2);
        Assert.assertFalse(sess2.isOpen());
    }

    @Test
    public void getIssueTrackingConnection() throws SQLException {
//        Connection conn = jFac.getIssueTrackingConnection();
//        Assert.assertNotNull(conn);
//        jFac.closeConnection(conn);
//        Assert.assertTrue(conn.isClosed());
    }
}
