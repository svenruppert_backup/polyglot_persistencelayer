package connectionfactory.impl.jdbcdriverinstantiator;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import connectionfactory.rdbmns.impl.jdbcconnection.HsqlDbJdbcDriverInstantiator;
import junit.framework.Assert;
import org.hibernate.exception.spi.SQLExceptionConversionDelegate;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 30.08.12
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */
public class SqliteJdbcDriverInstantiatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void nullParameterTest() {
        new HsqlDbJdbcDriverInstantiator().instantiateConnection(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyParameterTest() {
        new HsqlDbJdbcDriverInstantiator().instantiateConnection("");
    }

    @Test
    public void notExistingPathTest() {
        final String dbName = "testtest.sqlite";
        try {
            Files.deleteIfExists(Paths.get(dbName));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Connection conn = new HsqlDbJdbcDriverInstantiator().instantiateConnection(dbName);
        Assert.assertTrue(Files.exists(Paths.get(dbName + ".log")));
        new JdbcConnectionFactory().closeConnection(conn);

        try {
            Files.deleteIfExists(Paths.get(dbName + ".tmp"));
            Files.deleteIfExists(Paths.get(dbName + ".log"));
            Files.deleteIfExists(Paths.get(dbName + ".properties"));
            Files.deleteIfExists(Paths.get(dbName + ".script"));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void exceptionTriggerTest() {
        new HsqlDbJdbcDriverInstantiator().instantiateConnection("///");
    }
}
