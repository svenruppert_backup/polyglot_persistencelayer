package prototype;

import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssueBaseFluentInterface;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.io.File;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 14:29
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseTestPrototype {
    protected final static IssueBaseFluentInterface issueBaseDao = DaoFactoryFluentInterfaces.getIssueBaseDao();

    protected static void deleteGraphDatabaseDirIssueTracking()
    {
        String graphDbPath = new PathHelper().getPathToGraphDb("issueTracking");
        deleteTreeDir(new File(graphDbPath));
    }

    public static void deleteTreeDir(File path) {

        if(path.listFiles() != null){
            for ( File file : path.listFiles() )
            {
                if ( file.isDirectory() )
                    deleteTreeDir(file);
                file.delete();
            }
            path.delete();
        }
    }

    protected static void fillRelDatabaseWithEntries(int amountEntries) {
        IssueBase newIssue;
        for (int i = 0; i < amountEntries; i++) {

            newIssue = new IssueBase();
            newIssue.setSummary(String.valueOf(i));
            newIssue.setText(String.valueOf(i));
            newIssue.setVersion(String.valueOf(i));
            newIssue.setDueDate_planned(new Date(1));
            newIssue.setDueDate_resolved(new Date(2));
            newIssue.setDueDate_closed(new Date(3));
            issueBaseDao.create(newIssue);
        }
    }

    protected static void clearRelDatabase() {
        new AbstractJdbcExecuter<IssueBase>(IssueBase.class) {

            @Override
            protected List<IssueBase> executeQuery(Object[] args) {
                try {
                    connection.setAutoCommit(false);
                    Statement stmt = connection.createStatement();
                    stmt.execute("DELETE FROM Issue");
                    connection.commit();
                    connection.setAutoCommit(true);
                } catch (SQLException e) {
                    try {
                        connection.rollback();
                        connection.setAutoCommit(true);
                    } catch (SQLException e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return new ArrayList<>();
            }
        }.execute(null);
    }

    protected static void resetRelAutoIncrement() {
        clearRelDatabase();
        new AbstractJdbcExecuter<IssueBase>(IssueBase.class) {

            @Override
            protected List<IssueBase> executeQuery(Object[] args) {
                try {
                    Statement stmt = connection.createStatement();
                    stmt.execute("ALTER TABLE issue AUTO_INCREMENT = 1");
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return null;
            }
        }.execute(null);
    }
}
