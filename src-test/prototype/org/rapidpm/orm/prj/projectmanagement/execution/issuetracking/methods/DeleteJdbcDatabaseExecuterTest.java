package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 24.08.12
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */
public class DeleteJdbcDatabaseExecuterTest extends BaseTestPrototype {

    @BeforeClass
    public static void setUpOnce() {

    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteExeNullParameter() {
        new DeleteJdbcDatabaseExecuter(IssueBase.class) {
        }.execute(null);
    }

    @Test(expected = NullPointerException.class)
    public void deleteExeNullParameter2() {
        Object[] args = {null};

        new DeleteJdbcDatabaseExecuter(IssueBase.class) {
        }.execute(args);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        Object[] args = {};

        new DeleteJdbcDatabaseExecuter(IssueBase.class) {
        }.execute(args);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration() {
        IssueBase issue = issueBaseDao.getAll().get(0);
        Object[] args = {issue, issue};

        new DeleteJdbcDatabaseExecuter(IssueBase.class) {
        }.execute(args);
    }
}
