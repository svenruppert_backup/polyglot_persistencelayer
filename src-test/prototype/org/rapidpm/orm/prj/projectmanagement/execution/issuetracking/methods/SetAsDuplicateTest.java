package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.Before;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class SetAsDuplicateTest extends BaseTestPrototype {

    @Before
    public void setUpOnce() {
        System.out.println("setAsDuplicate_setUp");

        clearRelDatabase();
        deleteGraphDatabaseDirIssueTracking();

        fillRelDatabaseWithEntries(6);
    }

    /******************************************************************************************************************
    *  Testing Executer by invocing them using the Prototypes FluentDaoInterface
     *
     *  Not all Exceptionhandling and error causing cases can be tested here - thos are tested later on.
    *******************************************************************************************************************/


    @Test
    public void setAsDuplicatesStandard() {
        System.out.println("setAsDuplicate_standard");
        List<IssueBase> list = issueBaseDao.getAll();

        IssueBase e1 = list.get(0);
        IssueBase e2 = list.get(1);
        IssueBase e3 = list.get(2);
        IssueBase e4 = list.get(3);
        IssueBase e5 = list.get(4);

        issueBaseDao.setAsDuplicate(e1, e2);
        issueBaseDao.setAsDuplicate(e1, e4);

        List<IssueBase> actList = new ArrayList<>();
        actList.add(e2);
        actList.add(e4);
        assertEquals(actList, issueBaseDao.getDuplicates(e1));
    }

    @Test
    public void setAsDuplicatesStandard2() {
        System.out.println("setAsDuplicate_standard2");
        List<IssueBase> list = issueBaseDao.getAll();

        IssueBase e1 = list.get(0);
        IssueBase e2 = list.get(1);
        IssueBase e3 = list.get(2);
        IssueBase e4 = list.get(3);
        IssueBase e5 = list.get(4);
        IssueBase e6 = list.get(5);

        issueBaseDao.setAsDuplicate(e1, e2);
        issueBaseDao.setAsDuplicate(e2, e3);
        issueBaseDao.setAsDuplicate(e4, e1);
        issueBaseDao.setAsDuplicate(e5, e6);
        issueBaseDao.setAsDuplicate(e4, e5);

        list.remove(0);

        assertEquals(list, issueBaseDao.getDuplicates(e1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setAsDuplicatesEqualParameter() {
        System.out.println("setAsDuplicate_equalParameter");
        List<IssueBase> list = issueBaseDao.getAll();
        IssueBase e1 = list.get(0);
        issueBaseDao.setAsDuplicate(e1, e1);
    }

    @Test(expected = NullPointerException.class)
    public void setAsDuplicatesNullParameter1() {
        System.out.println("setAsDuplicate_nullParameter1");
        List<IssueBase> list = issueBaseDao.getAll();
        IssueBase e1 = list.get(0);
        issueBaseDao.setAsDuplicate(e1, null);
    }

    @Test(expected = NullPointerException.class)
    public void setAsDuplicatesNullParameter2() {
        System.out.println("setAsDuplicate_nullParameter2");
        List<IssueBase> list = issueBaseDao.getAll();
        IssueBase e1 = list.get(0);
        issueBaseDao.setAsDuplicate(null, e1);
    }

    @Test(expected = NullPointerException.class)
    public void setAsDuplicatesNullParameter3() {
        System.out.println("setAsDuplicate_nullParameter2");
        issueBaseDao.setAsDuplicate(null, null);
    }


    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooFewParametersFluentDao {
        public void setAsDuplicate(IssueBase e1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        tooFewParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooFewParametersFluentDao,IssueBase>
                        (tooFewParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.setAsDuplicate(new IssueBase());
    }


    // Malicious Fluent Interface:
    public interface withoutParametersFluentDao {
        public void setAsDuplicate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithoutParametersInMethodDeclaration(){
        withoutParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<withoutParametersFluentDao,IssueBase>
                        (withoutParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.setAsDuplicate();
    }

    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void setAsDuplicate(IssueBase e1, IssueBase e2, IssueBase e3);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.setAsDuplicate(new IssueBase(), new IssueBase(), new IssueBase());
    }

}
