package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.BeforeClass;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.StaticEntity;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class GetAllTest extends BaseTestPrototype {
    private final static int entries = 10;

    @BeforeClass
    public static void setUpOnce() {
        System.out.println("getAll_setUpOnce");

        clearRelDatabase();
        fillRelDatabaseWithEntries(entries);
    }

    @Test
    public void getAllMethodeTestStandard() {
        System.out.println("getAll_standard");
        List<IssueBase> list = issueBaseDao.getAll();
        assertEquals(entries, list.size());
    }


    private interface IllegalAccessExceptionDao {
        List<StaticEntity> getAll();
    }

    //expects an IllegalAccessException, but OnlyStackTrace gets printed
    @Test
    public void illegalAccessException(){
        System.out.println("instanciation exception");
        IllegalAccessExceptionDao illegalAccessExceptionDao =
                new DynamicDaoProxy<IllegalAccessExceptionDao,StaticEntity>
                        (IllegalAccessExceptionDao.class, StaticEntity.class).getDaoInstance();

        List<StaticEntity> liste = illegalAccessExceptionDao.getAll();
    }


    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void getAll(IssueBase e1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.getAll(new IssueBase());
    }
}
