package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;


import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class GetDuplicatesTest extends BaseTestPrototype {

    @BeforeClass
    public static void setUpOnce() {
        System.out.println("getDuplicate_setUpOnce");

        clearRelDatabase();
        deleteGraphDatabaseDirIssueTracking();

        fillRelDatabaseWithEntries(6);

        List<IssueBase> list = issueBaseDao.getAll();

        IssueBase e1 = list.get(0);
        IssueBase e2 = list.get(1);
        IssueBase e3 = list.get(2);
        IssueBase e4 = list.get(3);
        IssueBase e5 = list.get(4);


        /* root & zweite ebene*/
        issueBaseDao.setAsDuplicate(e1, e2);
        issueBaseDao.setAsDuplicate(e2, e3);
        issueBaseDao.setAsDuplicate(e1, e4);
        issueBaseDao.setAsDuplicate(e4, e5);
    }


    /******************************************************************************************************************
     *  Testing Executer by invocing them using the Prototypes FluentDaoInterface
     *
     *  Not all Exceptionhandling and error causing cases can be tested here - thos are tested later on.
     *******************************************************************************************************************/

    @Test
    public void getDuplicatesTestStandard() {
        System.out.println("getDuplicate_standard");
        List<IssueBase> list = issueBaseDao.getAll();
        List<IssueBase> dupList = issueBaseDao.getDuplicates(list.get(0));
        Assert.assertEquals(4, dupList.size());
        list.remove(5);
        list.remove(0);
        Assert.assertEquals(list, dupList);
    }

    @Test
    public void getDuplicatesTestWithoutDuplicates() {
        System.out.println("getDuplicate_withoutDuplicats");
        List<IssueBase> list = issueBaseDao.getAll();
        List<IssueBase> dupList = issueBaseDao.getDuplicates(list.get(5));
        Assert.assertEquals(0, dupList.size());
    }

    @Test(expected = NullPointerException.class)
    public void getDuplicatesTestNullParameter() {
        System.out.println("getDuplicate_nullParameter");
        List<IssueBase> dupList = issueBaseDao.getDuplicates(null);
    }

    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooFewParametersFluentDao {
        public void getDuplicates();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        tooFewParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooFewParametersFluentDao,IssueBase>
                        (tooFewParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.getDuplicates();
    }


    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void getDuplicates(IssueBase e1, IssueBase e2);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.getDuplicates(new IssueBase(), new IssueBase());
    }
}
