package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.BeforeClass;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
public class GetByIdTest extends BaseTestPrototype {

    @BeforeClass
    public static void setUpOnce() {
        System.out.println("getById_setUpOnce");

        clearRelDatabase();
        fillRelDatabaseWithEntries(10);
    }

    @Test
    public void getByIdMethodeTestStandard() {
        System.out.println("getById_standard");
        long id = issueBaseDao.getAll().get(0).getId();
        List<IssueBase> list = issueBaseDao.getById(id);
        assertNotSame(0, list.size());
        assertEquals(id, (long) list.get(0).getId());
    }

    @Test
    public void getByIdMethodeTestNonExistingId() {
        System.out.println("getById_nonExistingId");
        List<IssueBase> list = issueBaseDao.getById(2000);
        assertEquals(0, list.size());
    }


    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooFewParametersFluentDao {
        public void getById();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        tooFewParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooFewParametersFluentDao,IssueBase>
                        (tooFewParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.getById();
    }


    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void getById(IssueBase e1, IssueBase e2);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.getById(new IssueBase(), new IssueBase());
    }
}
