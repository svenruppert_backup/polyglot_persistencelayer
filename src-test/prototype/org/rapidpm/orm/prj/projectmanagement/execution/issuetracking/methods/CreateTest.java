package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.BeforeClass;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
public class CreateTest extends BaseTestPrototype {

    @BeforeClass
    public static void setUpOnce() {
        System.out.println("create_setUpOnce");
        clearRelDatabase();
    }

    @Test
    public void createTestStandard() {
        System.out.println("create_standard");
        IssueBase issue = new IssueBase();
        long i = issueBaseDao.getAll().size() + 1;

        issue.setSummary(String.valueOf(i));
        issue.setText(String.valueOf(i));
        issue.setVersion(String.valueOf(i));
        issue.setDueDate_planned(new Date(1));
        issue.setDueDate_resolved(new Date(2));
        issue.setDueDate_closed(new Date(3));
        issueBaseDao.create(issue);

        List<IssueBase> list = issueBaseDao.getAll();
        IssueBase testIssue = list.get(list.size() - 1);
        assertEquals(issue.getSummary(), testIssue.getSummary());
        assertEquals(issue.getText(), testIssue.getText());
        assertEquals(issue.getVersion(), testIssue.getVersion());
        assertEquals(issue.getDueDate_planned().toString(), testIssue.getDueDate_planned().toString());
        assertEquals(issue.getDueDate_resolved().toString(), testIssue.getDueDate_resolved().toString());
        assertEquals(issue.getDueDate_closed().toString(), testIssue.getDueDate_closed().toString());
    }

    @Test(expected = NullPointerException.class)
    public void createTestNullParameter() {
        System.out.println("create_nullParameter");
        issueBaseDao.create(null);
    }

    @Test
    public void createTestNewParameter() {
        System.out.println("create_newParamter");
        issueBaseDao.create(new IssueBase());
    }

    @Test
    public void createTestOnlyId() {
        System.out.println("create_onlyId");
        IssueBase issue = new IssueBase();
        issue.setId((long)100);
        issueBaseDao.create(issue);
    }

    @Test
    public void createTestWrongDateType() {
        System.out.println("create_wrongDateType");
        IssueBase issue = new IssueBase();
        issue.setDueDate_planned(new java.util.Date());
        issueBaseDao.create(issue);
    }

    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooFewParametersFluentDao {
        public void create();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        tooFewParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooFewParametersFluentDao,IssueBase>
                        (tooFewParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.create();
    }


    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void create(IssueBase e1, IssueBase e2);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.create(new IssueBase(), new IssueBase());
    }
}
