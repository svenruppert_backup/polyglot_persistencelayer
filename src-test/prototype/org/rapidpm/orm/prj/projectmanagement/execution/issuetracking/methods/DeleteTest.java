package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import org.junit.BeforeClass;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.DynamicDaoProxy;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 23.08.12
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
public class DeleteTest extends BaseTestPrototype {

    @BeforeClass
    public static void setUpOnce() {
        System.out.println("delete_setUpOnce");

        clearRelDatabase();
        deleteGraphDatabaseDirIssueTracking();

        fillRelDatabaseWithEntries(6);

        List<IssueBase> list = issueBaseDao.getAll();

        IssueBase e1 = list.get(0);
        IssueBase e2 = list.get(1);
        IssueBase e3 = list.get(2);
        IssueBase e4 = list.get(3);
        IssueBase e5 = list.get(4);


        /* root & zweite ebene*/
        issueBaseDao.setAsDuplicate(e1, e2);
        issueBaseDao.setAsDuplicate(e2, e3);
        issueBaseDao.setAsDuplicate(e1, e4);
        issueBaseDao.setAsDuplicate(e4, e5);
    }


    /*
        Delete the last entry in the relational database.
        Search by id for the deleted entry and check if the listsize is zero.
     */
    @Test
    public void deleteMethodTestStandardWithRelations() {
        System.out.println("delete_standardWithRelations");
        List<IssueBase> list = issueBaseDao.getAll();
        if (list.size() > 1) {
            IssueBase issue = list.get(3);
            issueBaseDao.delete(issue);
            assertEquals(0, issueBaseDao.getById(issue.getId()).size());
            issue = list.get(0);
            issueBaseDao.delete(issue);
            assertEquals(0, issueBaseDao.getById(issue.getId()).size());
        }
    }

    @Test
    public void deleteMethodTestStandardWithoutRelations() {
        System.out.println("delete_standardWithoutRelations");
        List<IssueBase> list = issueBaseDao.getAll();
        if (list.size() > 1) {
            IssueBase issue = list.get(list.size()-1);
            issueBaseDao.delete(issue);
            assertEquals(0, issueBaseDao.getById(issue.getId()).size());
        }
    }

    @Test
    public void deleteMethodTestNonExistingId() {
        System.out.println("delete_nonExistingId");
        IssueBase issue = new IssueBase();
        issue.setId((long)2000);
        issueBaseDao.delete(issue);

    }

    /*
        The parameter is null.
        Expect NullPointerException to get thrown.
     */
    @Test(expected = NullPointerException.class)
    public void deleteMethodTestNullParameter() {
        System.out.println("deleteNullParameter");
        issueBaseDao.delete(null);

    }

    /*
        The parameter is a new instance of the type.
        Expect a NullPointerException to get thrown.
     */
    @Test(expected = NullPointerException.class)
    public void deleteMethodTestNewParamter() {
        System.out.println("deleteNewParameter");
        issueBaseDao.delete(new IssueBase());

    }

    /******************************************************************************************************************
     *  Testing Executer by invocing it's Methods using either a malisous defined FluentDaoInterface or by calling
     *  them directly.
     *
     *  This is neccessary, because not every exeptional behavior can be provoked by using an error-less
     *  FluentDaoInterface.
     ******************************************************************************************************************/

    // Malicious Fluent Interface:
    public interface tooFewParametersFluentDao {
        public void delete();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooFewParametersInMethodDeclaration(){
        tooFewParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooFewParametersFluentDao,IssueBase>
                        (tooFewParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.delete();
    }


    // Malicious Fluent Interface:
    public interface tooManyParametersFluentDao {
        public void delete(IssueBase e1, IssueBase e2);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testTooManyParametersInMethodDeclaration(){
        tooManyParametersFluentDao maliciousIssueBaseDao =
                new DynamicDaoProxy<tooManyParametersFluentDao,IssueBase>
                        (tooManyParametersFluentDao.class, IssueBase.class).getDaoInstance();
        maliciousIssueBaseDao.delete(new IssueBase(), new IssueBase());
    }
}
