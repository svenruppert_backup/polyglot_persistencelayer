package prototype;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/23/12
 * Time: 7:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PathHelper {

    public String getPathToGraphDb(String directoryName){
        String path = directoryName;
        if(getClass().getClassLoader().getResource(directoryName) != null)
            path = getClass().getClassLoader().getResource(directoryName).getPath();
        return path;
    }
}
