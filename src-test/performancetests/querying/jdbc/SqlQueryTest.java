package performancetests.querying.jdbc;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;
import performancetests.querying.sql.SqlPerformancetestQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 30.08.12
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
public class SqlQueryTest extends ToSubstituteTestDbsSwitcher {
    private static List<String[]> compareList = new ArrayList<>();

    @BeforeClass
    public static void setUpOnce() {
        compareList.add(new String[]{"58636", "Iserlohn"});
        compareList.add(new String[]{"58638", "Iserlohn"});
        compareList.add(new String[]{"58640", "Iserlohn"});
        compareList.add(new String[]{"58642", "Iserlohn"});
        compareList.add(new String[]{"58644", "Iserlohn"});

//        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
    }

    @Test
    public void test() {
        SqlPerformancetestQuery query = new SqlPerformancetestQuery();
        query.openDatabaseConnection();
        query.performTestQuery();
        query.closeDatabaseConnection();

        List<String[]> result = query.getResultList();

        System.out.println(result.size());
        for (int i = 0; i < result.size(); i++) {
            Assert.assertArrayEquals(compareList.get(i), result.get(i));
        }
    }

}
