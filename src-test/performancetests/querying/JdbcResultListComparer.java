package performancetests.querying;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import performancetests.querying.sql.SqlPerformancetestQuery;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */


public class JdbcResultListComparer{

    static List<String[]> jdbcResultList = null;
    static {
        if(jdbcResultList == null || jdbcResultList.size() <1){
            JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
            SqlPerformancetestQuery sqlTest = new SqlPerformancetestQuery();
            sqlTest.openDatabaseConnection();
            sqlTest.performTestQuery();
            sqlTest.closeDatabaseConnection();
            jdbcResultList = sqlTest.getResultList();
        }
    }

    public boolean equalResults(AbstractPerformancetestQuery testQuery){
        testQuery.openDatabaseConnection();
        testQuery.performTestQuery();
        testQuery.closeDatabaseConnection();

        List<String[]> otherList = testQuery.getResultList();

        System.out.println(jdbcResultList.size());
        System.out.println(otherList.size());


        boolean areListsEquall = false;
        if(jdbcResultList.size() != jdbcResultList.size())
            return areListsEquall;

        int jdbcLength = jdbcResultList.size();
        boolean[] jdbcFound = new boolean[jdbcLength];

        int otherLength = otherList.size();
        boolean[] otherFound = new boolean[otherLength];

        for(int i=0; i<jdbcLength; i++){
            String hibZip = jdbcResultList.get(i)[0];
            String hibName = jdbcResultList.get(i)[1];
            for(int j=0; j<otherLength; j++){
                Object[] otherValues = otherList.get(j);
                String neoZip = otherValues[0].toString();
                String neoName = otherValues[1].toString();

                if(hibZip.equals(neoZip) && hibName.equals(neoName) && !jdbcFound[i] && !otherFound[j]){
                    jdbcFound[i] = true;
                    otherFound[j] = true;
                    break;
                }
            }
        }


        areListsEquall = checkBooleanArrayForFalse(jdbcFound) && checkBooleanArrayForFalse(otherFound);
        return areListsEquall;
    }

    private boolean checkBooleanArrayForFalse(boolean[] boolArray){
        int length = boolArray.length;
        for(int i=0; i<length; i++)
            if(!boolArray[i])
                return false;

        return true;
    }
}
