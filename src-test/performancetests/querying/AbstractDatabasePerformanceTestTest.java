package performancetests.querying;

import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDatabasePerformanceTestTest {

    private class TestPerformancetestQuery<T> extends AbstractPerformancetestQuery<T> {

        protected TestPerformancetestQuery(String fileName) {
            super(fileName);
        }

        @Override
        public void openDatabaseConnection() {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void performTestQuery() {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void closeDatabaseConnection() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalAmountOfTestTest(){
        new TestPerformancetestQuery(null);
    }

    @Test
    public void getPerformanceTestResultListNotNullTest(){
        List<Double> resultList = new TestPerformancetestQuery("filename").getPeformancetestResults();
        assertNotNull(resultList);
    }

    @Test
    public void getResultListNotNullTest(){
        List<Double> resultList = new TestPerformancetestQuery("filename").getResultList();
        assertNotNull(resultList);
    }
}
