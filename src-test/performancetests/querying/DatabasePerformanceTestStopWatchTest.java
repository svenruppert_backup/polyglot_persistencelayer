package performancetests.querying;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class DatabasePerformanceTestStopWatchTest {
    
    private class SleepForOneSecondPerformancetestQuery extends AbstractPerformancetestQuery<Double> {

        protected SleepForOneSecondPerformancetestQuery(String fileName) {
            super(fileName);
        }

        @Override
        public void openDatabaseConnection() {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void performTestQuery() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) { }
        }

        @Override
        public void closeDatabaseConnection() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    @Test(expected = NullPointerException.class)
    public void nullPointerParameterTest(){
        new PerformanceTestStopWatch().execute(null, 1);
    }

    @Test
    public void stopWatchTest(){
        AbstractPerformancetestQuery testPerformancetestQuery =
                new SleepForOneSecondPerformancetestQuery("filename");
        List<AbstractPerformancetestQuery> perfTestList = new ArrayList<AbstractPerformancetestQuery>();
        perfTestList.add(testPerformancetestQuery);
        new PerformanceTestStopWatch().execute(perfTestList, 1);

        List<Double> neededTimes = testPerformancetestQuery.getPeformancetestResults();
        double neededTime = neededTimes.get(0);
        Assert.assertTrue(neededTime > 1 && neededTime < 1.010);
    }

}
