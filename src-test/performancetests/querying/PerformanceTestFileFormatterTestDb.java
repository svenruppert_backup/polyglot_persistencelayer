package performancetests.querying;

import org.junit.*;
import performancetests.querying.sql.SqlPerformancetestQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 8:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class PerformanceTestFileFormatterTestDb extends ToSubstituteTestDbsSwitcher {

    private static String fileName;

    @BeforeClass
    public static void createTest() throws IOException {
        AbstractPerformancetestQuery sqlPerformancetestQuery = new
                SqlPerformancetestQuery();

        sqlPerformancetestQuery.getPeformancetestResults().add(0.0);
        sqlPerformancetestQuery.getPeformancetestResults().add(1.0);
        sqlPerformancetestQuery.getPeformancetestResults().add(2.0);

        List<AbstractPerformancetestQuery> sqlTest = new ArrayList<>();
        sqlTest.add(sqlPerformancetestQuery);

        PerformanceTestFileFormatter performanceTestFileFormatter =
                new PerformanceTestFileFormatter(sqlTest);
        performanceTestFileFormatter.writeResultsToFile();

        fileName = sqlPerformancetestQuery.getResultFilePath();
    }

    @AfterClass
    public static void deleteTest() throws IOException {
        Files.delete(Paths.get(fileName));
    }

    @Test(expected = NullPointerException.class)
    public void performanceTestQueryNullParameter(){
        new PerformanceTestFileFormatter(null);
    }


    @Test(expected = NullPointerException.class)
    public void pathToFileNullParameter() throws IOException {
        SqlPerformancetestQuery sqlPerformancetestQuery = new
                SqlPerformancetestQuery();

        List<AbstractPerformancetestQuery> sqlTest = new ArrayList<>();
        sqlTest.add(sqlPerformancetestQuery);
        new PerformanceTestFileFormatter(null).writeResultsToFile();
    }


    //TODO Deleted expected Exception
    @Test
    public void pathToDirectoryTest() throws IOException {
        SqlPerformancetestQuery sqlPerformancetestQuery = new
                SqlPerformancetestQuery();

        List<AbstractPerformancetestQuery> sqlTest = new ArrayList<>();
        sqlTest.add(sqlPerformancetestQuery);
        new PerformanceTestFileFormatter(sqlTest).writeResultsToFile();
    }

    @Test
    public void fileAlreadyExists() throws IOException {
        SqlPerformancetestQuery sqlPerformancetestQuery = new
                SqlPerformancetestQuery();

        List<AbstractPerformancetestQuery> sqlTest = new ArrayList<>();
        sqlTest.add(sqlPerformancetestQuery);
        new PerformanceTestFileFormatter(sqlTest).writeResultsToFile();
    }

    @Test
    public void fileIsCreatedTest(){
        final Path pathPath = Paths.get(fileName);
        final boolean fileExists = Files.exists(Paths.get(fileName));
        assertTrue(fileExists);
    }

    @Test
    public void amountOfLinesTest() throws Exception {
        List<String> lines = getFileLinesList();
        assertEquals (lines.size(), 5);
    }

    @Test
    public void lineFormatTest() throws Exception {
        List<String> lines = getFileLinesList();

        for(int i=0; i<3; i++){
            String line = lines.get(i);
            String knownLine = String.format("%2d.\t%f", i,Double.valueOf(i) );
            System.out.println(line);

            assertEquals(knownLine, line);
        }

        assertEquals(lines.get(3), "");
        String germanString = "Average Time: 1,000000";
        String englishString = "Average Time: 1.000000";
        String averageTimeString = lines.get(4);
        boolean interlanguageCorrectString = germanString.equals(averageTimeString) || englishString.equals(englishString);
        assertTrue(interlanguageCorrectString);
    }

    private List<String> getFileLinesList() throws IOException {
        List<String> lines = new ArrayList<>();

        String line;
        try(final BufferedReader reader = Files.newBufferedReader(Paths.get(fileName), Charset.defaultCharset()))
        {
            while ((line = reader.readLine()) != null)
                lines.add(line);
        }

        return lines;
    }
}
