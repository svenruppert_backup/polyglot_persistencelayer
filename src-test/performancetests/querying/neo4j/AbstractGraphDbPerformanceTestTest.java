package performancetests.querying.neo4j;

import org.junit.Test;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.fullyconnectedgraph.BasicFullyConnectedQuery;
import performancetests.querying.neo4j.fullyconnectedgraph.FullyConnectedBasicApiGraphDbPerformanceTestTestDb;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractGraphDbPerformanceTestTest {

    private class GraphDbPerformancetestQuery extends AbstractGraphDbPerformancetestQuery {

        public GraphDbPerformancetestQuery(long amountOfTests,
                                           Class<? extends AbstractGraphDbPerformancetestQuery> graphShape,
                                           Class<? extends IResultBuilder> valueStrategie) throws IllegalAccessException, InstantiationException {
            super(graphShape, valueStrategie);
        }

        @Override
        public void performTestQuery() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    @Test(expected = NullPointerException.class)
    public void testSecondParameterNull() throws InstantiationException, IllegalAccessException {
        new GraphDbPerformancetestQuery(1, null, NodeAttributesResultBuilder.class);
    }

    @Test(expected = NullPointerException.class)
    public void testThirdParameterNull() throws InstantiationException, IllegalAccessException {
        new GraphDbPerformancetestQuery(1, BasicFullyConnectedQuery.class, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInterfaceAsParameter() throws InstantiationException, IllegalAccessException {
        new GraphDbPerformancetestQuery(1, BasicFullyConnectedQuery.class, IResultBuilder.class);
    }
}
