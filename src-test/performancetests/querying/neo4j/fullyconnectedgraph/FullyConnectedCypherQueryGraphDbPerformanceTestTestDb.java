package performancetests.querying.neo4j.fullyconnectedgraph;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.JdbcResultListComparer;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.PropertyNodesResultBuilder;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class FullyConnectedCypherQueryGraphDbPerformanceTestTestDb extends ToSubstituteTestDbsSwitcher {

        @Test
        public void testForNodePropertiesStrategie() throws Exception {
            AbstractGraphDbPerformancetestQuery basic =
                    new CypherFullyConnectedQuery(new PropertyNodesResultBuilder());
            boolean matchesJdbcResultList = new JdbcResultListComparer().equalResults(basic);
            assertTrue(matchesJdbcResultList);
        }

        @Test
        public void testForPropertiesAsNodes() throws Exception {
            AbstractGraphDbPerformancetestQuery basic =
                    new CypherFullyConnectedQuery(new NodeAttributesResultBuilder());
            boolean matchesJdbcResultList = new JdbcResultListComparer().equalResults(basic);
            assertTrue(matchesJdbcResultList);
        }
}
