package performancetests.querying.neo4j.treeshapedgraph;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.JdbcResultListComparer;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.PropertyNodesResultBuilder;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class TreeShapedCyptherQueryGraphDbPerformanceTestTestDb extends ToSubstituteTestDbsSwitcher {
    @Test
    public void testForNodePropertiesStrategie() throws Exception {
        CypherTreeShapedQuery basic =
                new CypherTreeShapedQuery(new PropertyNodesResultBuilder());
        boolean matchesJdbcResultList = new JdbcResultListComparer().equalResults(basic);
        assertTrue(matchesJdbcResultList);
    }

    @Test
    public void testForPropertiesAsNodes() throws Exception {
        CypherTreeShapedQuery basic =
                new CypherTreeShapedQuery(new NodeAttributesResultBuilder());
        boolean matchesJdbcResultList = new JdbcResultListComparer().equalResults(basic);
        assertTrue(matchesJdbcResultList);
    }
}
