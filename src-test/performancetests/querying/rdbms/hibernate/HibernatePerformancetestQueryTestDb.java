package performancetests.querying.rdbms.hibernate;

import org.junit.Ignore;
import org.junit.Test;
import performancetests.querying.JdbcResultListComparer;
import performancetests.querying.ToSubstituteTestDbsSwitcher;
import performancetests.querying.hibernate.HibernatePerformanceTest;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class HibernatePerformancetestQueryTestDb extends ToSubstituteTestDbsSwitcher {
    @Test
    public void testResultList(){
        HibernatePerformanceTest hibernateTest = new HibernatePerformanceTest();
        boolean listsEqualEachOther = new JdbcResultListComparer().equalResults(hibernateTest);
        System.out.println(listsEqualEachOther);
        assertTrue(listsEqualEachOther);
    }

}
