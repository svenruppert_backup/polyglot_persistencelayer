package performancetests.querying;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import org.junit.BeforeClass;
import performancetests.GraphServiceFactory;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 5:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class ToSubstituteTestDbsSwitcher {
    @BeforeClass
    public static void switchToSqliteDbFile(){
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        GraphServiceFactory.setInTestMode(true);
    }

}
