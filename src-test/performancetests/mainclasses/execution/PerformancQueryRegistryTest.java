package performancetests.mainclasses.execution;

import org.junit.Test;
import performancetests.querying.AbstractPerformancetestQuery;

import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/6/12
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class PerformancQueryRegistryTest {

    @Test
    public void checkAmountOfInstantiatedClasses(){
        List<AbstractPerformancetestQuery> instanceList =  new PerformanceQueryRegistry().getInstancList();
        assertEquals(instanceList.size(), 14);
    }

}
