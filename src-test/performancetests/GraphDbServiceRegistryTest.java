package performancetests;

import junit.framework.TestCase;
import org.junit.Test;
import performancetests.converter.impl.attributepersistingsstrategie.NodeAttributesNodeBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.treeshapedgraph.BasicTreeShapedQuery;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/31/12
 * Time: 4:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class GraphDbServiceRegistryTest {
    @Test
    public void correctReturnValueTest() throws Exception {
        GraphDbServiceRegistry serviceRegistry = new GraphDbServiceRegistry();
        Class class1 = BasicTreeShapedQuery.class;
        Class class2 = NodeAttributesResultBuilder.class;
        serviceRegistry.getGraphDbName(class1, class2);
        String TestString = class1.getSimpleName() + class1.getSimpleName();
    }

    @Test(expected = NullPointerException.class)
    public void firstParameterNullTest() throws Exception {
        new GraphDbServiceRegistry().getGraphDbName(null, NodeAttributesNodeBuilder.class);
    }

    @Test(expected = NullPointerException.class)
    public void secondParameterNullTest() throws Exception {
        new GraphDbServiceRegistry().getGraphDbName(BasicTreeShapedQuery.class, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void firstClassUnknownTest() throws Exception {
        new GraphDbServiceRegistry().getGraphDbName(String.class, NodeAttributesNodeBuilder.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void secondClassUnknownTest() throws Exception {
        new GraphDbServiceRegistry().getGraphDbName(BasicTreeShapedQuery.class, String.class);
    }

}
