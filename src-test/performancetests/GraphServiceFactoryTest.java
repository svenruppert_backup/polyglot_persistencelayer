package performancetests;

import junit.framework.Assert;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import prototype.BaseTestPrototype;
import prototype.PathHelper;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 24.08.12
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class GraphServiceFactoryTest {


    @Test(expected = IllegalArgumentException.class)
    public void getGraphEmptyStringTest() {
        GraphDatabaseService graph = GraphServiceFactory.getGraphService("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getGraphNullParameterTest() {
        GraphDatabaseService graph = GraphServiceFactory.getGraphService(null);
    }


    @Test
    public void graphAccessTest() {
        final String dbName = "./GraphDatabases/testGraphDb";
        GraphDatabaseService graph = GraphServiceFactory.getGraphService(dbName);
        Node node = null;
        Transaction tx = graph.beginTx();
        try{
            node = graph.createNode();
            tx.success();
        } finally {
            tx.finish();
            GraphServiceFactory.shutDownDatabase(dbName);
        }

        Assert.assertNotNull(node);
        BaseTestPrototype.deleteTreeDir(new File(new PathHelper().getPathToGraphDb(dbName)));
    }
}
