package performancetests;

import junit.framework.Assert;
import org.junit.Test;
import org.neo4j.graphdb.DynamicRelationshipType;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 27.08.12
 * Time: 11:48
 * To change this template use File | Settings | File Templates.
 */
public class RelationsRegistryTest {

    @Test
    public void relationsRegistryTestGetValid() {
        RelationsRegistry relReg = new RelationsRegistry();
        Assert.assertEquals(DynamicRelationshipType.withName("KONTINENT").name(), relReg.getRelation(1).name());
        Assert.assertEquals(DynamicRelationshipType.withName("BUNDESLAND").name(), relReg.getRelation(3).name());
        Assert.assertEquals(DynamicRelationshipType.withName("ORTSTEIL").name(), relReg.getRelation(9).name());
    }



    @Test(expected = IllegalArgumentException.class)
    public void relationsRegistryTestGetInvalid() {
        RelationsRegistry relReg = new RelationsRegistry();
        relReg.getRelation(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void relationsRegistryGetTooHighLevel(){
        RelationsRegistry relReg = new RelationsRegistry();
        relReg.getRelation(100);
    }
}
