package performancetests.inserting;



import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Traverser;
import performancetests.inserting.PerformanceTest;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 27.08.12
 * Time: 10:04
 * To change this template use File | Settings | File Templates.
 */
public class PerformanceTestTest {
    PerformanceTest perf;
    GraphDatabaseService graph;


    @Before
    public void setUp() {
        PerformanceTest.deleteDBDir();
        perf = new PerformanceTest(20, false);
        graph = perf.getGraphDB();
    }

    @After
    public void tearDown() {
        perf.shutdown();
    }

    @Test
    public void createFullyConnectedTest() {
        perf.createFullyConnectedNodes();
        Node node = graph.getNodeById(0);

        int j = 0;
        Traverser trav = perf.searchAllNodesFrom(node);
        for (Node singelNode : trav.nodes()) {
            j++;
        }

        Assert.assertEquals(20, j);
    }

    @Test
    public void addOneToFullyConnected() {
        perf.createFullyConnectedNodes();

        perf.addOneNodeFullyConnected();
        Node node = graph.getNodeById(0);

        int j = 0;
        Traverser trav = perf.searchAllNodesFrom(node);
        for (Node singelNode : trav.nodes()) {
            j++;
        }

        Assert.assertEquals(21, j);

        j=0;
        for (Relationship rel : graph.getNodeById(21).getRelationships()) {
            j++;
        }
        Assert.assertEquals(21, j);
    }


    @Test
    public void createTreeShapedTest() {
        perf.createTreeNodes(2);
        Node node = graph.getNodeById(0);
        int j = 0;

        Traverser trav = perf.searchAllNodesFrom(node);
        for (Node singelNode : trav.nodes()) {
            j++;
        }
        Assert.assertEquals(20, j);

        j=0;
        for (Relationship rel : node.getRelationships()) {
            j++;
        }
        Assert.assertEquals(2, j);
    }

    @Test
    public void addOneToTreeShaped() {
        perf.createTreeNodes(2);

        perf.addOneNodeToTree();
        Node node = graph.getNodeById(0);

        int j = 0;
        Traverser trav = perf.searchAllNodesFrom(node);
        for (Node singelNode : trav.nodes()) {
            j++;
        }

        Assert.assertEquals(21, j);
    }
}
