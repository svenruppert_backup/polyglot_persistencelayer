package performancetests.inserting;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import prototype.BaseTestPrototype;
import prototype.PathHelper;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 29.08.12
 * Time: 13:20
 * To change this template use File | Settings | File Templates.
 */
public class MeasureExecuterTest {
    private PerformanceTest perf;
    private final static String OUTFILE = "./measureExecuterTest.txt";

    @Before
    public void setUp() {
        PerformanceTest.deleteDBDir();
        perf = new PerformanceTest(1, true);
    }

    @After
    public void tearDown(){
        perf.shutdown();
    }

    @AfterClass
    public static void tearDownOnce(){
        try {
            Files.delete(Paths.get(OUTFILE));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        BaseTestPrototype.deleteTreeDir(new File(new PathHelper().getPathToGraphDb("./target")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void NullParameterPerf() {
        new MeasureExecuter(null , 1 , OUTFILE, false) {

            @Override
            public void dotask() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String outputInfo() {
                return "";  //To change body of implemented methods use File | Settings | File Templates.
            }
        }.execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void NullParameterOutFile() {
        new MeasureExecuter(perf , 1 , null, false) {

            @Override
            public void dotask() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String outputInfo() {
                return "";  //To change body of implemented methods use File | Settings | File Templates.
            }
        }.execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void EmptyParameterOutFile() {
        new MeasureExecuter(perf , 1 , "", false) {

            @Override
            public void dotask() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String outputInfo() {
                return "";  //To change body of implemented methods use File | Settings | File Templates.
            }
        }.execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void NegativParameterAmountNodes() {
        new MeasureExecuter(perf , -1 , null, false) {

            @Override
            public void dotask() {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public String outputInfo() {
                return "";  //To change body of implemented methods use File | Settings | File Templates.
            }
        }.execute();
    }

    @Test
    public void testMeasureExecuter() {

        final String outPutString = "MeasureExecuterTest";
        MeasureExecuter mexe = new MeasureExecuter(perf, 1, OUTFILE, false) {

            @Override
            public void dotask() {
                perf.createFullyConnectedNodes();
            }

            @Override
            public String outputInfo() {
                return outPutString;
            }
        };

        mexe.execute();

        String inLine;
        List<String> str = new ArrayList<>();

        try {
            FileReader reader = new FileReader(OUTFILE);
            BufferedReader in  = new BufferedReader(reader);
            while ((inLine = in.readLine()) != null) {
                str.add(inLine);
            }
            reader.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Assert.assertEquals(1, mexe.getAmountNodes());
        Assert.assertEquals(perf, mexe.getPerftest());
        Assert.assertTrue(str.get(0).startsWith(outPutString));
    }
}
