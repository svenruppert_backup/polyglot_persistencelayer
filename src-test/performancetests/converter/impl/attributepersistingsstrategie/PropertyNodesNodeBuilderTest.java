package performancetests.converter.impl.attributepersistingsstrategie;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.*;
import performancetests.GraphServiceFactory;
import performancetests.GraphTxExecuter;
import performancetests.converter.impl.jdbcexecuter.GetGermanStandardCityNameAndValidUntilExecuter;
import performancetests.converter.impl.jdbcexecuter.GetLevelIdsJdbcExecuter;
import performancetests.converter.impl.jdbcexecuter.GetZipCodeAndValidUntilJdbcExecuter;
import prototype.BaseTestPrototype;
import prototype.PathHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 29.08.12
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class PropertyNodesNodeBuilderTest {
    @BeforeClass
    public static void switchToSqliteDbFile(){
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
    }

    @Test
    public void checkLevelSixNodeAttributeCreation(){
        List<Long> lvlSixIdsList = new GetLevelIdsJdbcExecuter(6).execute();
        for(long lvl6Id : lvlSixIdsList){
            boolean areAttributesCorrect = checkAttributePersitingStrategieForId(lvl6Id);
            assertTrue(areAttributesCorrect);
        }
    }

    public Boolean checkAttributePersitingStrategieForId(final long testId) {
        final List<Boolean> areAllAttributesCoorect = new ArrayList<>();

        new GraphTxExecuter(GraphServiceFactory.FC_NODEATTRIBUTES) {
            @Override
            protected void doTransaction() {
                Node testNode = graph.createNode();
                testNode.setProperty("loc_id", testId);
                new PropertyNodesNodeBuilder().persistAttributes(testId, testNode, graph);

                RelationshipType nameRelation = DynamicRelationshipType.withName("NAME");
                RelationshipType zipRelation = DynamicRelationshipType.withName("ZIP_CODE");

                List<String[]> cityZips = new GetZipCodeAndValidUntilJdbcExecuter(testId).execute();
                List<String[]> cityNames = new GetGermanStandardCityNameAndValidUntilExecuter(testId).execute();

                boolean areZipcCorrect = checkAttributes(testNode, cityZips, zipRelation);
                boolean areNamesCorrect = checkAttributes(testNode, cityNames, nameRelation);
                boolean areBothOk = areNamesCorrect && areZipcCorrect;
                areAllAttributesCoorect.add(areBothOk);
            }
        }.execute();
        return areAllAttributesCoorect.get(0);
    }

    private boolean checkAttributes(Node node, List<String[]> attributeValues, RelationshipType attributeRelation){

        boolean allAttributesOk = false;
        for(String[] attribiteValue : attributeValues){
            allAttributesOk= false;
            for(Relationship relation : node.getRelationships(attributeRelation, Direction.OUTGOING)){
                Node propertyNode = relation.getEndNode();
                String propertyNodeValue = (String) propertyNode.getProperty("value");
                boolean isValueOk = attribiteValue[0].equals(propertyNodeValue);
                String relationValidUntil = (String) relation.getProperty("valid_until");
                boolean validUntilOk = relationValidUntil.equals(attribiteValue[1]);
                if(isValueOk && validUntilOk){
                    allAttributesOk = true;
                    break;
                }
            }
            if(!allAttributesOk){
                break;
            }
        }
        return allAttributesOk;
    }

    @Test(expected = IllegalArgumentException.class)
    public void NegativeParameterLvlId() {
        PropertyNodesNodeBuilder tmp = new PropertyNodesNodeBuilder();
        tmp.persistAttributes(-1, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void NullParameterNode() {
        PropertyNodesNodeBuilder tmp = new PropertyNodesNodeBuilder();
        tmp.persistAttributes(1, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void NullParameterService() {
        final String dbName = "./GraphDatabases/testGraphDb";
        final GraphDatabaseService graph = GraphServiceFactory.getGraphService(dbName);

        PropertyNodesNodeBuilder tmp = new PropertyNodesNodeBuilder();
        Transaction tx = graph.beginTx();
        Node node;
        try {
            node = graph.createNode();
            tmp.persistAttributes(1, node , null);
            tx.success();
        }                finally {
            tx.finish();
            GraphServiceFactory.shutDownDatabase(dbName);
        }

        BaseTestPrototype.deleteTreeDir(new File(new PathHelper().getPathToGraphDb(dbName)));
    }


}
