package performancetests.converter.impl.attributepersistingsstrategie;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import performancetests.GraphServiceFactory;
import performancetests.GraphTxExecuter;
import performancetests.converter.impl.jdbcexecuter.GetGermanStandardCityNameAndValidUntilExecuter;
import performancetests.converter.impl.jdbcexecuter.GetLevelIdsJdbcExecuter;
import performancetests.converter.impl.jdbcexecuter.GetZipCodeAndValidUntilJdbcExecuter;
import prototype.BaseTestPrototype;
import prototype.PathHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 29.08.12
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public class NodeAttributesNodeBuilderTest {
    private final static String dbName = "./GraphDatabases/testGraphDb";

    @BeforeClass
    public static void switchToSqliteDbFile(){
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
    }

    @AfterClass
    public static void tearDownOnce() {
        BaseTestPrototype.deleteTreeDir(new File(new PathHelper().getPathToGraphDb(dbName)));
    }

    @Test
    public void checkLevelSixNodeAttributeCreation(){
        List<Long> lvlSixIdsList = new GetLevelIdsJdbcExecuter(6).execute();
        for(long lvl6Id : lvlSixIdsList){
            boolean areAttributesCorrect = checkAttributePersitingStrategieForId(lvl6Id);
            assertTrue(areAttributesCorrect);
        }
    }

    public Boolean checkAttributePersitingStrategieForId(final long testId) {
        final List<Boolean> areAllAttributesCoorect = new ArrayList<>();
        areAllAttributesCoorect.add(true);

        new GraphTxExecuter(GraphServiceFactory.FC_NODEATTRIBUTES) {
            @Override
            protected void doTransaction() {
                Node testNode = graph.createNode();
                testNode.setProperty("loc_id", testId);
                new NodeAttributesNodeBuilder().persistAttributes(testId, testNode, graph);

                List<String[]> cityZips = new GetZipCodeAndValidUntilJdbcExecuter(testId).execute();
                List<String[]> cityNames = new GetGermanStandardCityNameAndValidUntilExecuter(testId).execute();
                boolean allAttributesCorrect;
                for(String[] zip : cityZips){
                    boolean correct = false;
                    for(String[] name : cityNames){
                        if(zip[1].equals(name[1]) && areAllAttributesCoorect.get(0)){
                            boolean exists = testNode.hasProperty(zip[0]);
                            String nodeCityName = (String) testNode.getProperty(zip[0]);
                            boolean isCorrectName = nodeCityName.equals(name[0]);
                            correct = exists && isCorrectName;
                        }
                        else{
                            boolean exists = testNode.hasProperty(zip[0]);
                            String nodeCityName = (String) testNode.getProperty(zip[0]);
                            boolean isCorrectName = !nodeCityName.equals(name[0]);
                            correct = exists && isCorrectName;
                        }
                    }
                    if(!correct){
                        areAllAttributesCoorect.remove(0);
                        areAllAttributesCoorect.add(false);
                        break;
                    }
                }
            }
        }.execute();
        return areAllAttributesCoorect.get(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NegativeParameterLvlId() {
        NodeAttributesNodeBuilder tmp = new NodeAttributesNodeBuilder();
        tmp.persistAttributes(-1, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void NullParameterNode() {
        NodeAttributesNodeBuilder tmp = new NodeAttributesNodeBuilder();
        tmp.persistAttributes(1, null, null);
    }

    @Test(expected = NullPointerException.class)
    public void NullParameterService() {
        final GraphDatabaseService graph = GraphServiceFactory.getGraphService(dbName);

        NodeAttributesNodeBuilder tmp = new NodeAttributesNodeBuilder();
        Transaction tx = graph.beginTx();
        Node node;
        try {
            node = graph.createNode();
            tmp.persistAttributes(1, node , null);
            tx.success();
        }                finally {
            tx.finish();
            GraphServiceFactory.shutDownDatabase(dbName);
        }


    }
}
