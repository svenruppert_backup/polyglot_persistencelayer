package performancetests.converter.impl.jdbcexecuter;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetCityNamesAndValidUntilExecuterTestDb {

    @Test
    public void testExecute() throws Exception {
        List<String[]> resultList = new GetGermanStandardCityNameAndValidUntilExecuter(18873).execute();

        assertEquals(resultList.size(), 1);
        String iserlohnName = resultList.get(0)[0];
        assertEquals("Iserlohn", iserlohnName);

        String iserlohnValidUntil = resultList.get(0)[1];
        assertEquals("1970-01-01", iserlohnValidUntil);
    }

    @Test
    public void testExecuteNotExistingNode() throws Exception {
        List<String[]> resultList = new GetGermanStandardCityNameAndValidUntilExecuter(200_000).execute();

        assertEquals(resultList.size(), 0);
    }
}
