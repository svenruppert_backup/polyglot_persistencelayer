package performancetests.converter.impl.jdbcexecuter;

import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetZipCodeAndValidUntilJdbcExecuterTest  {
    @Test
    public void amountOfResultsTest() throws Exception {
        List<String[]> resultList = new GetZipCodeAndValidUntilJdbcExecuter(18873).execute();
        System.out.println(resultList);
        assertEquals(resultList.size(), 5);
    }

    @Test
    public void valuesOfResultTest(){
        List<String[]> resultList = new GetZipCodeAndValidUntilJdbcExecuter(18873).execute();
        List<String[]> knownList = new ArrayList<>();

        knownList.add(new String[]{"58636", "3000-01-01"});
        knownList.add(new String[]{"58638", "3000-01-01"});
        knownList.add(new String[]{"58640", "3000-01-01"});
        knownList.add(new String[]{"58642", "3000-01-01"});
        knownList.add(new String[]{"58644", "3000-01-01"});

        boolean[] found = new boolean[5];
        for(int i=0; i<5; i++){
            String resultZip = resultList.get(i)[0].trim();
            String resultVilidUntil = resultList.get(i)[1].trim();
            for(int j=0; j<5; j++){
                String knownZip = knownList.get(j)[0].trim();
                String knownValidUntil = knownList.get(j)[1].trim();

                if(knownZip.equals(resultZip)  && knownValidUntil.equals(resultVilidUntil)){
                    found[i] = true;
                    break;
                }
            }
        }


    }
}
