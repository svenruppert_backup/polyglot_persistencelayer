package performancetests.converter.impl.jdbcexecuter;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

import java.util.List;

import static org.jgroups.util.Util.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetParentLevelIdsJdbcExecuterTestDb extends ToSubstituteTestDbsSwitcher {

    @Test
    public void resultCountTest() throws Exception {
        List<Long> resultList = new GetParentLevelIdsJdbcExecuter(6, 18873, 2) .execute();
        long germanyParentNode = resultList.get(0);
        assertEquals(resultList.size(), 1);
    }

    @Test
    public void resultEqualsTest(){
        List<Long> resultList = new GetParentLevelIdsJdbcExecuter(6, 18873, 2) .execute();
        long germanyParentNode = resultList.get(0);
        long knownGermanyParentNode  = 105L;
        assertEquals(germanyParentNode, knownGermanyParentNode);
    }


}
