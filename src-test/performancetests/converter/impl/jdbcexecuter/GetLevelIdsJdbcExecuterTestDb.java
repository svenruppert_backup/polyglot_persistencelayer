package performancetests.converter.impl.jdbcexecuter;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class GetLevelIdsJdbcExecuterTestDb extends ToSubstituteTestDbsSwitcher {

    @Test
    public void testExecute() throws Exception {
        List<Long> resultList = new GetLevelIdsJdbcExecuter(6).execute();

        List<Long>  knownTestList = new ArrayList<>();
        knownTestList.add(18873L);

        assertEquals(knownTestList, resultList);
    }
}
