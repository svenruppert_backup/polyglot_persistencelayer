package performancetests.converter;
import connectionfactory.rdbmns.JdbcConnectionFactory;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import performancetests.GraphServiceFactory;
import performancetests.GraphTxExecuter;
import performancetests.converter.impl.ToFullyConnectedConverter;
import performancetests.converter.impl.attributepersistingsstrategie.NodeAttributesNodeBuilder;
import performancetests.converter.impl.attributepersistingsstrategie.PropertyNodesNodeBuilder;
import performancetests.converter.impl.jdbcexecuter.GetLevelIdsJdbcExecuter;
import prototype.BaseTestPrototype;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class OpengeoDbToTreeShapedGraphDbConverterTest extends AbstractTreeStructureTest {

    private static String propertyNodesGraphDb = "./DatabaseTest/TR_PropertyNodesTestDb";
    private static String nodeAttributesGraphDb = "./DatabaseTest/TR_NodesAttributesTestDb";
    @BeforeClass
    public static void createTestDbs(){
        new ToFullyConnectedConverter(propertyNodesGraphDb, new PropertyNodesNodeBuilder()).execute();
        new ToFullyConnectedConverter(nodeAttributesGraphDb, new NodeAttributesNodeBuilder()).execute();

        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        GraphServiceFactory.setInTestMode(true);
    }

    @AfterClass
    public static void deleteTestDbs(){
        BaseTestPrototype.deleteTreeDir(new File(propertyNodesGraphDb + "_TEST"));
        BaseTestPrototype.deleteTreeDir(new File(nodeAttributesGraphDb + "_TEST"));
    }

    @Test(expected = NullPointerException.class)
         public void firstConstructorFirstParameterNull() {
        new ToFullyConnectedConverter(null, new PropertyNodesNodeBuilder()).execute();
    }

    @Test(expected = NullPointerException.class)
    public void firstConstructorsecondParameterNull() {
        new ToFullyConnectedConverter(propertyNodesGraphDb, null).execute();
    }

    @Test(expected = NullPointerException.class)
    public void secondConstructorFirstParameterNull() {
        new ToFullyConnectedConverter(null).execute();
    }

    @Test
    public void openOverSecondConstructor() {
        Assert.assertNotNull(new ToFullyConnectedConverter(new NodeAttributesNodeBuilder()));
    }

    @Test
    public void OpengeoDbToFullyConnecteWithNodeAttributes() {
        boolean isStructureForLeveOk;
        for(int i=1; i<10; i++){
            isStructureForLeveOk = checkStructure(i, nodeAttributesGraphDb);
            assertTrue(isStructureForLeveOk);
        }
    }

    @Test
    public void OpengeoDbToFullyConnecteWithAttributeNodes() {
        boolean isStructureForLeveOk;
        for(int i=1; i<10; i++){
            isStructureForLeveOk = checkStructure(i, propertyNodesGraphDb);
            assertTrue(isStructureForLeveOk);
        }
    }

    public boolean checkStructure(final int level,final String graphDb){
        final List<Long> levelIdList = new GetLevelIdsJdbcExecuter(level).execute();
        final List<Boolean> returnValue = new ArrayList<>();
        returnValue.add(true);
        final int childLevel = 1 +level;

        if(levelIdList.size() > 0)
            new GraphTxExecuter(graphDb) {
                @Override
                protected void doTransaction() {
                    for(final long levelLocId : levelIdList ){
                        Node levelNode =  indexRegistry.getLocIdIndex().get("loc_id", levelLocId).getSingle();
                        final List<Long> chilLeveldIdList = getChildLevelIds(level, level-1, levelLocId);
                        for(final long childId : chilLeveldIdList){
                            boolean found = false;

                            for(Relationship relation : levelNode.getRelationships(relationsRegistry.getRelation(childLevel), Direction.OUTGOING)){
                                long childLocId = (Long) relation.getEndNode().getProperty("loc_id");
                                if(childId == childLocId){
                                    found = true;
                                    break;
                                }
                            }
                            if(!found && returnValue.get(0)){
                                returnValue.remove(0);
                                returnValue.add(false);
                                break;
                            }
                        }
                    }
                }
            }.execute();

        return returnValue.get(0);
    }
}
