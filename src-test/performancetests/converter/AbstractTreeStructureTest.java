package performancetests.converter;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import org.hibernate.bytecode.spi.NotInstrumentedException;
import org.junit.BeforeClass;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import performancetests.JdbcQueryExecuter;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/30/12
 * Time: 10:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractTreeStructureTest extends ToSubstituteTestDbsSwitcher {
    @BeforeClass
    public static void switchToSqliteDbFile(){
        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
    }


    protected List<Long> getChildLevelIds(final int parentLevel, final int childLevel, final long parentLocId){
        List<Long> childLevelIds = new JdbcQueryExecuter<Long>(){
            @Override
            protected void createResultList() throws SQLException {
                if(resultSet != null){
                    long loc_id = resultSet.getLong("loc_id");
                    resultList.add(loc_id);
                }
            }

            @Override
            protected String returnQueryString() throws SQLException {
                String sqlString =  "SELECT loc_id " +
                                    "FROM geodb_hierarchies " +
                                    "WHERE  id_lvl%s=%s " +
                                    "AND level=%s";

                return String.format(sqlString, parentLevel, parentLocId, childLevel);
            }
        }.execute();

        return childLevelIds;
    }
}