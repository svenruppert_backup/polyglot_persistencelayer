package performancetests;

import org.junit.Test;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/28/12
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class JdbcQueryExecuterTestDb extends ToSubstituteTestDbsSwitcher {


    @Test
    public void testMaliciousSqlQueryString(){

        System.out.println("Malicious sql query string");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                // not needed, due to not executable sql query
            }

            @Override
            protected String returnQueryString() {
                return "SELECT notExistingColumn FROM notExistingTable";
            }
        }.execute();

        assertNotNull(result);
    }

    @Test
    public void testNotExistingColumnInResultSet(){
        System.out.println("Reading not existing column from resultSet");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                String testString = resultSet.getString("DOESNT_EXIST");
            }

            @Override
            protected String returnQueryString() {
                return "SELCT * from geodb_textdata";
            }
        }.execute();

        assertNotNull(result);
    }

    @Test(expected = NullPointerException.class )
    public void testReturnNullAsSqlQueryString(){
        System.out.println("Return null as sql query string");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                //not needed
            }

            @Override
            protected String returnQueryString() {
                return null;
            }
        }.execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReturnEmptyQueryString(){
        System.out.println("Return empty as sql query string");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                //not needed
            }

            @Override
            protected String returnQueryString() {
                return "";
            }
        }.execute();
    }

    @Test
    public void setConnectionToNullTest(){
        System.out.println("Set connection to null");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                //not needed
            }

            @Override
            protected String returnQueryString() {
                connection = null;
                return "SELECT * FROM TABLE";
            }
        }.execute();

        assertNotNull(result);
    }

    @Test
    public void closeConnectionPrematurely(){
        System.out.println("closed connection too early");
        List<String> result = new JdbcQueryExecuter<String>() {
            @Override
            protected void createResultList() throws SQLException {
                //not needed
            }

            @Override
            protected String returnQueryString() {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return "SELECT * FROM TABLE";
            }
        }.execute();

        assertNotNull(result);
    }

    @Test
    public void readSqlDataTest(){
        System.out.println("Querying germanys loc_id");
        List<Long> result = new JdbcQueryExecuter<Long>() {
            @Override
            protected void createResultList() throws SQLException {
                long idLvl2 = resultSet.getLong("id_lvl2");
                resultList.add(idLvl2);
            }

            @Override
            protected String returnQueryString() {
                return "SELECT id_lvl2 FROM geodb_hierarchies WHERE loc_id=105";
            }
        }.execute();

        List<Long> knownResult = new ArrayList<>();
        knownResult.add(105L);

        assertEquals(result, knownResult);

    }
}
