package performancetests;

import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.converter.impl.ToFullyConnectedConverter;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/6/12
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractRdbmsToGRaphConverterTest {

    private class NotIinstantiableIAttrubtePersistingStrategie implements IAttributePersistingStrategie {

        private NotIinstantiableIAttrubtePersistingStrategie(){
            // static class - has to be empty
        }

        @Override
        public void persistAttributes(long lvlId, Node node, GraphDatabaseService graphDbService) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    @Test
    public void uninstantiableIAttributePersistingStrategieParameter(){
        new ToFullyConnectedConverter("DOESNT_MATTER", new NotIinstantiableIAttrubtePersistingStrategie());
    }
}
