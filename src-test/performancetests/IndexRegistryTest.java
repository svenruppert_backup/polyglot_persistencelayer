package performancetests;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import junit.framework.Assert;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.graphdb.GraphDatabaseService;
import performancetests.converter.impl.ToFullyConnectedConverter;
import performancetests.converter.impl.attributepersistingsstrategie.PropertyNodesNodeBuilder;
import performancetests.querying.ToSubstituteTestDbsSwitcher;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 27.08.12
 * Time: 09:11
 * To change this template use File | Settings | File Templates.
 */
public class IndexRegistryTest extends ToSubstituteTestDbsSwitcher {
    GraphDatabaseService graph;

    private static String testGraphDb = GraphServiceFactory.FC_NODEATTRIBUTES;

    @BeforeClass
    public static void createTestDbs(){
        new ToFullyConnectedConverter(testGraphDb,
                new PropertyNodesNodeBuilder()).execute();

        JdbcConnectionFactory.setDbType(JdbcConnectionFactory.SQLite);
        GraphServiceFactory.setInTestMode(true);
    }

    @After
    public void tearDown() {
        GraphServiceFactory.shutDownDatabase(testGraphDb);
    }


    @Test(expected = IllegalArgumentException.class)
    public void indexRegistryTestNullParameter() {
        IndexRegistry indRel = new IndexRegistry(null);
    }

    @Test
    public void indexRegistryTestGetIndexNameValid() {
        graph = GraphServiceFactory.getGraphService(testGraphDb);
        IndexRegistry indRel = new IndexRegistry(graph);
        Assert.assertEquals("kontinent", indRel.indexName(1));
        Assert.assertEquals("bundesland", indRel.indexName(3));
        Assert.assertEquals("ortschaft", indRel.indexName(7));
        Assert.assertEquals("ortsteil", indRel.indexName(9));
    }

    @Test(expected = IllegalArgumentException.class)
    public void indexRegistryTestGetIndexNameInvalid() {
        graph = GraphServiceFactory.getGraphService(testGraphDb);
        IndexRegistry indRel = new IndexRegistry(graph);
        Assert.assertNull(indRel.indexName(0));
    }

    @Test
    public void indexRegistryTestGraphIndex() {
        graph = GraphServiceFactory.getGraphService(testGraphDb);
        IndexRegistry indRel = new IndexRegistry(graph);
        Assert.assertNotNull(indRel.getLocIdIndex());
        Assert.assertNotNull(indRel.getTypeIndex());
    }
}
