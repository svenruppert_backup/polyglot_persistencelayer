\select@language {ngerman}
\contentsline {section}{\numberline {1}Aufgabenbeschreibung}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Motivation}{6}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Ziel der Ausarbeitung}{6}{subsection.1.2}
\contentsline {section}{\numberline {2}Begrifflichkeiten}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Polyglot Persistance}{7}{subsection.2.1}
\contentsline {section}{\numberline {3}Verwendete Framworks}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Java}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Neo4J}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}MySQL}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Hibernate}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}HSQL DB}{9}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Git}{9}{subsection.3.6}
\contentsline {section}{\numberline {4}Testumgebungen}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Testmaschine 1}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Testmaschine 2}{10}{subsection.4.2}
\contentsline {section}{\numberline {5}Umsetzung der OpengeDb in Neo4j}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Allgemeines}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Aufbau der Datenbank}{11}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Notwendige Modifikationen am SQL Skript}{12}{subsection.5.3}
\contentsline {section}{\numberline {6}Vor\IeC {\"u}berlegungen zu den Performancetests}{13}{section.6}
\contentsline {subsection}{\numberline {6.1}Graphen Strukturen}{14}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Baumf\IeC {\"o}rmige Graphendatenbanken}{14}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Graphendatenbank mit vollvermaschten Zweigen}{14}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {6.2}Persistierungsstrategien f\IeC {\"u}r Daten}{15}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Daten als Knoten(Propertynodes)}{15}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Daten als Node Attribute (Nodeattributes)}{16}{subsubsection.6.2.2}
\contentsline {subsection}{\numberline {6.3}Verfolgte Ans\IeC {\"a}tze}{17}{subsection.6.3}
\contentsline {section}{\numberline {7}Implemetierung der Performancetests}{18}{section.7}
\contentsline {subsection}{\numberline {7.1}Trennung der Baumstruktur von der Datenhaltungsstrategie}{18}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Implementierte Abfragetypen}{19}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}SQL Abfrage \IeC {\"u}ber JDBC}{19}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}HQL Abfrage}{20}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Erl\IeC {\"a}uterungen Basic API}{20}{subsubsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.4}Erl\IeC {\"a}uterung Traversal API}{21}{subsubsection.7.2.4}
\contentsline {subsubsection}{\numberline {7.2.5}Erl\IeC {\"a}uterung Cypher API}{22}{subsubsection.7.2.5}
\contentsline {subsection}{\numberline {7.3}Performancetests Unit Tests}{23}{subsection.7.3}
\contentsline {section}{\numberline {8}Performancetests}{26}{section.8}
\contentsline {subsection}{\numberline {8.1}Erstellung einer Graphen-Datenbanke}{26}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Einf\IeC {\"u}gen eines neuen Knotens}{27}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Abfrage Performancetest Ergebnisse}{28}{subsection.8.3}
\contentsline {subsubsection}{\numberline {8.3.1}SQL Query \IeC {\"u}ber JDBC vs. HQL Query \IeC {\"u}ber Hibernate}{28}{subsubsection.8.3.1}
\contentsline {subsubsection}{\numberline {8.3.2}Baumartige Graphen}{29}{subsubsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.3}Vollvermaschte Zweige}{31}{subsubsection.8.3.3}
\contentsline {subsubsection}{\numberline {8.3.4}Auswertung}{32}{subsubsection.8.3.4}
\contentsline {subsection}{\numberline {8.4}Fazit}{33}{subsection.8.4}
\contentsline {section}{\numberline {9}Prototyp}{34}{section.9}
\contentsline {subsection}{\numberline {9.1}Datenmodell}{34}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Anforderungen an den Prototypen}{34}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}Abgrenzungen der Anforderungen}{34}{subsubsection.9.2.1}
\contentsline {subsubsection}{\numberline {9.2.2}Vor\IeC {\"u}berlegungen Graphendatenbankoperationen.}{35}{subsubsection.9.2.2}
\contentsline {subsection}{\numberline {9.3}Erl\IeC {\"a}uterungen}{35}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}Begrifflichkeitsabgrenzung}{35}{subsection.9.4}
\contentsline {subsection}{\numberline {9.5}Fluent DAOs mit Hilfe von Dynamic Proxies}{36}{subsection.9.5}
\contentsline {subsection}{\numberline {9.6}Codegeneration/Query Builder}{39}{subsection.9.6}
\contentsline {subsection}{\numberline {9.7}Gegen\IeC {\"u}berstellung, Schlussfolgerung}{42}{subsection.9.7}
\contentsline {section}{\numberline {10}Prototyp Implementierung}{43}{section.10}
\contentsline {subsection}{\numberline {10.1}Abgrenzung}{43}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Transaktionsmanagement}{43}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Vor\IeC {\"u}berlegungen zu Duplikat-Beziehungen}{43}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}Operationen auf die Graphendatenbank}{45}{subsection.10.4}
\contentsline {subsection}{\numberline {10.5}Indexierung der Graphendatenbank}{47}{subsection.10.5}
\contentsline {subsection}{\numberline {10.6}Mapping der DAO Methoden}{48}{subsection.10.6}
\contentsline {subsection}{\numberline {10.7}Klassen-/Methodenbeschreibung}{49}{subsection.10.7}
\contentsline {subsubsection}{\numberline {10.7.1}Klassen}{49}{subsubsection.10.7.1}
\contentsline {paragraph}{DAOFactory}{49}{section*.16}
\contentsline {paragraph}{DynamicDaoProxy}{49}{section*.17}
\contentsline {paragraph}{DaoMethodRegistry}{49}{section*.18}
\contentsline {paragraph}{IssueBaseFluentInterface}{49}{section*.19}
\contentsline {paragraph}{AbstractDatabaseQueryExecuter}{49}{section*.20}
\contentsline {paragraph}{AbstractJdbcExecuter}{49}{section*.21}
\contentsline {paragraph}{AbstractGraphExecuter}{49}{section*.22}
\contentsline {subsubsection}{\numberline {10.7.2}Methoden}{49}{subsubsection.10.7.2}
\contentsline {paragraph}{Create}{50}{section*.23}
\contentsline {paragraph}{Delete}{50}{section*.24}
\contentsline {paragraph}{GetAll}{50}{section*.25}
\contentsline {paragraph}{GetById}{50}{section*.26}
\contentsline {paragraph}{SetAsDuplicate}{50}{section*.27}
\contentsline {paragraph}{GetDuplicates}{50}{section*.28}
\contentsline {subsection}{\numberline {10.8}Testf\IeC {\"a}lle}{52}{subsection.10.8}
\contentsline {subsection}{\numberline {10.9}Prototyp Bewertung}{52}{subsection.10.9}
\contentsline {section}{\numberline {11}Fazit/Ausblick}{53}{section.11}
\contentsline {subsection}{\numberline {11.1}Allgemein}{53}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Prototyp}{53}{subsection.11.2}
\contentsline {section}{Literaturverzeichnis}{54}{subsection.11.2}
\contentsline {section}{Anhang}{55}{section*.30}
\contentsline {section}{\numberline {A}Ausf\IeC {\"u}hren der Performancetests}{55}{appendix.A}
\contentsline {section}{\numberline {B}Erstellung der produktiven Graphendatenbanken aus der OpengeoDb heraus}{55}{appendix.B}
\contentsline {section}{\numberline {C}Ausf\IeC {\"u}hrung Unit Tests}{55}{appendix.C}
