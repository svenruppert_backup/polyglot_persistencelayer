package connectionfactory.rdbmns;

import org.hibernate.Session;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 9:29 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IHibernateSessionFactory {


    /**
     * Nimmt einen Datenbanknahmen, oder Pfad entgegen
     * un gibt ein Hibernate Session Objekt zurück.
     * @param dbName Datenbankname oder Pfad.
     * @return Hibernate Session Instanz.
     */
    public Session getHibernateSession(String dbName);
}
