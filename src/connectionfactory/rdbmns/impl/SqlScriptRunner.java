package connectionfactory.rdbmns.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/29/12
 * Time: 2:27 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Erstellt eine neue SQLite Datenbank
 */
public class SqlScriptRunner {

    /**
     * Öffnet eine JDBC Connection auf eine HSQLDB, öffnet
     * und stößt den import des SQL Skriptes an,
     *
     * @param dbName    Pfad zur HSQLDB.
     * @param pathToSqlScript Pfad zum SQL Script.
     */
    public void doImport(String dbName, String pathToSqlScript) {
        Connection connection = null;
        try {
            Class.forName("org.hsqldb.jdbcDriver").newInstance();
            final String url = "jdbc:hsqldb:file:" + dbName;
            connection = DriverManager.getConnection(url, "sa", "");

            System.out.println(dbName);

            final Path path = Paths.get(pathToSqlScript);
            try(final BufferedReader reader =Files.newBufferedReader(path, Charset.defaultCharset())){
                runSqlScript(connection, reader);
            } catch (IOException e) {
                System.err.println("Could not read file to import the database used for unit tests");
            }
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use // File | Settings | File Templates.
        }
        finally {
            try {
                if(connection != null && !connection.isClosed())
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    /**
     * Setzt jedes Statement des SQL Sktiptes gegen die HSQL DB ab. hierfür ist anzumerken, dass mehrere
     * Statements in einer Zeile nicht möglich sind, da nach ";" am Ende einer Zeile gesucht wird, Sollte
     * ein impotz nicht vollständig Funktionieren überprüfen Sie am besten als erstes ihr Skript dahingehend,
     * ob es diesen Anforderungen entspricht.
     *
     * @param connection    Connectionobjekt zur HSQLDB.
     * @param reader        Instanz eines Bufferes readers, der das SQL Skript geöffnet hat.
     * @throws SQLException Sollte die ausführung eines Statements fehlschlagen wird eine SQLException
     * geworden.
     * @throws IOException Sollte während des lesen aus der Datei etwas schief laufen wird eine
     * IOException geworden.
     */
    private void runSqlScript(Connection connection, BufferedReader reader) throws SQLException, IOException {
        String line;
        StringBuilder statementBuilder = new StringBuilder();
        Statement statement = connection.createStatement();
        while((line = reader.readLine()) != null){
            if(line != null &&  !line.isEmpty())
                statementBuilder.append(line);

            if(line.trim().endsWith(";")){
                System.out.println(statementBuilder.toString());
                statement.execute(statementBuilder.toString());
                statementBuilder = new StringBuilder();
            }
        }
        statement.close();
    }
}
