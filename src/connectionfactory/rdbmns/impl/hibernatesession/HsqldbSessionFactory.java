package connectionfactory.rdbmns.impl.hibernatesession;

import connectionfactory.rdbmns.IHibernateSessionFactory;
import performancetests.querying.hibernate.entities.GeodbHierarchiesEntity;
import performancetests.querying.hibernate.entities.GeodbTextdataEntity;
import performancetests.querying.hibernate.entities.GeodbTypeNamesEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class HsqldbSessionFactory implements IHibernateSessionFactory {

    /**
     * Insitialisiert und instanziiert eine Hibernate Session zu einem
     * auf localhost laufenden MySQL zu der Datenbank "dbName".
     * Hierfür müssen mindestens die drei Tabellen geodb_hierarchies,
     * geodb_textdata und geodb_typenames vorhanden sein.
     * @param dbName Name der localen MySql Datenbank.
     *
     * @return Hibernate Session Instanz.
     */
    @Override
    public Session getHibernateSession(String dbName) {
        SessionFactory hibernateSessionFactory = new Configuration()
                .setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect")
                .setProperty("hibernate.connection.url", "jdbc:hsqldb:file:" + dbName)
                .setProperty("hibernate.connection.driver_class  ", "org.hsqldb.jdbcDriver")
                .setProperty("hibernate.connection.username", "sa")
                .setProperty("hibernate.connection.password", "")
                .setProperty("hibernate.show_sql", "true")
                .addAnnotatedClass(GeodbHierarchiesEntity.class)
                .addAnnotatedClass(GeodbTextdataEntity.class)
                .addAnnotatedClass(GeodbTypeNamesEntity.class)
                .buildSessionFactory();

        return hibernateSessionFactory.openSession();
    }
}
