package connectionfactory.rdbmns.impl.hibernatesession;

import connectionfactory.rdbmns.IHibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import performancetests.querying.hibernate.entities.*;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 9:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class MySqlSessionFactory implements IHibernateSessionFactory {


    /**
     * Insitialisiert und instanziiert eine Hibernate Session zu einer
     * embedded HSQLDB im Pfad dbName..
     * Hierfür müssen mindestens die drei Tabellen geodb_hierarchies,
     * geodb_textdata und geodb_typenames vorhanden sein.
     * @param dbName Name der localen MySql Datenbank.
     *
     * @return Hibernate Session Instanz.
     */
    @Override
    public Session getHibernateSession(String dbName) {
        SessionFactory hibernateSessionFactory = new Configuration()
                .setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLInnoDBDialect")
                .setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/" + dbName)
                .setProperty("hibernate.connection.driver_class  ", "com.mysql.jdbc.Driver")
                .setProperty("hibernate.connection.username", "root")
                .setProperty("hibernate.connection.password", "")
                .setProperty("hibernate.show_sql", "true")
                .addAnnotatedClass(GeodbHierarchiesEntity.class)
                .addAnnotatedClass(GeodbTextdataEntity.class)
                .addAnnotatedClass(GeodbTypeNamesEntity.class)
                .addAnnotatedClass(GeodbTypeNamesEntity.class)
                .buildSessionFactory();

        return hibernateSessionFactory.openSession();
    }
}
