package connectionfactory.rdbmns.impl.jdbcconnection;

import connectionfactory.rdbmns.IJdbcDriverInstantiator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/29/12
 * Time: 12:45 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Instantiiert eine Verbindung über den JDBC-Treiber zur MySQL-Datenbank
 */
public class MySqlJdbcDriverInstantiator implements IJdbcDriverInstantiator {

    /**
     * Insitialisiert und instanziiert eine JDBC Connection zu eine
     * embedded HSQLDB mit dem Pfad "dbName".
     *
     * @param databaseName Pfad zur HSQLDB.
     *
     * @return JDBC Connection Instanz.
     */
    @Override
    public Connection instantiateConnection(String databaseName) {
        if (databaseName == null || databaseName == "")
            throw new IllegalArgumentException("databaseName must not be null or emtpy.");
        Connection connection = null;
        try {
            Class.forName ("com.mysql.jdbc.Driver").newInstance ();
            String userName = "root";
            String password = "";
            String url = String.format("jdbc:mysql://localhost/%s", databaseName);
            connection = DriverManager.getConnection(url, userName, password);
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
