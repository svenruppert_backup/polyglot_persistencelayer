package connectionfactory.rdbmns.impl.jdbcconnection;

import connectionfactory.rdbmns.IJdbcDriverInstantiator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/29/12
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Instantiiert eine Verbindung über den JDBC-Treiber zur SQLite-Datenbank
 */
public class HsqlDbJdbcDriverInstantiator implements IJdbcDriverInstantiator {

    /**
     * Insitialisiert und instanziiert eine JDBC Connection zu einem
     * auf localhost laufenden MySQL zu der Datenbank "dbName".
     *
     * @param databaseName Name der localen MySql Datenbank.
     *
     * @return JDBC Connection Instanz.
     */
    @Override
    public Connection instantiateConnection(String databaseName) {
        if (databaseName == null || databaseName == "")
            throw new IllegalArgumentException("databaseName must not be null or emtpy.");

        Connection connection = null;
        try {
            Class.forName("org.hsqldb.jdbcDriver").newInstance();
            final String url = "jdbc:hsqldb:file:" + databaseName;
            connection = DriverManager.getConnection(url, "sa", "");
        }catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return  connection;
    }
}
