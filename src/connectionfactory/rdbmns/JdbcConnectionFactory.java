package connectionfactory.rdbmns;

import connectionfactory.rdbmns.impl.SqlScriptRunner;
import connectionfactory.rdbmns.impl.hibernatesession.HsqldbSessionFactory;
import connectionfactory.rdbmns.impl.hibernatesession.MySqlSessionFactory;
import connectionfactory.rdbmns.impl.jdbcconnection.HsqlDbJdbcDriverInstantiator;
import connectionfactory.rdbmns.impl.jdbcconnection.MySqlJdbcDriverInstantiator;
import org.hibernate.Session;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/28/12
 * Time: 11:57 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Factory zur Verwaltung von Datenbankverbindungen
 */
public class JdbcConnectionFactory<T> {

    private static boolean isInTestMode = false;
    public static final boolean MySQL = false;
    public static final boolean SQLite = true;
    private final String mysqlOpengeoDbName = "opengeodb";
    private final String hsqldbOpengeoDbName = "Databases/UnitTestOpengeoDb.sqlite";
    private final String sqliteIssueTrackingDb = "UnitTestIssueTrackinig.sqlite";


    /**
     * Getter für statische Klasssenvariable, die festleg ob die
     * Klasse Connection-/Sessioninstanzen zu den Produktiv, oder
     * Testdatenbanken zurückgibt.
     *
     * @return
     */
    public static boolean getDbType() {
        return isInTestMode;
    }

    /**
     * Setzen des Verbindungstypen
     * @param dbType Legt fest, ob die Testdatenbank, oder die Produktivdatenbank genutzt wird.
     */
    public static void setDbType(boolean dbType) {
        JdbcConnectionFactory.isInTestMode = dbType;
    }

    /**
     * Aufbauen einer JDBC Datenbank Verbindung
     * @return Datenbankconnection
     */
    public Connection getJdbcConnection() {
        if(isInTestMode && !Files.exists(Paths.get(hsqldbOpengeoDbName + ".log")))
            new SqlScriptRunner().doImport(hsqldbOpengeoDbName, "./Data/SqlScripts/OpengeotestDb.sql");

        Connection newConnection = null;
        if(isInTestMode)
            newConnection = new HsqlDbJdbcDriverInstantiator().instantiateConnection(hsqldbOpengeoDbName);
        else
            newConnection = new MySqlJdbcDriverInstantiator().instantiateConnection(mysqlOpengeoDbName);

        return newConnection;
    }

    /**
     * Aufbauen einer Hibernate Datenbank Session
     * @return Datenbanksession
     */
    public Session getHibernateSession(){
        if(isInTestMode && !Files.exists(Paths.get(hsqldbOpengeoDbName + ".log")))
            new SqlScriptRunner().doImport(hsqldbOpengeoDbName, "./Data/SqlScripts/OpengeotestDb.sql");

        Session session = null;

        if(isInTestMode)
            session = new HsqldbSessionFactory().getHibernateSession(hsqldbOpengeoDbName);
        else {
            session = new MySqlSessionFactory().getHibernateSession(mysqlOpengeoDbName);
        }

        return session;
    }

//    /**
//     * Gibt eine Verbindung zur IssueTrackingDatenbank zurück
//     * @return
//     */
//    public Connection getIssueTrackingConnection(){
//        return new HsqlDbJdbcDriverInstantiator().instantiateConnection(sqliteIssueTrackingDb);
//    }

    /**
     * Schließt die übergebene Verbindung
     * @param connection zu schließende Verbindung
     */
    public void closeConnection(Connection connection){
        try {
            if(connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
    }

    /**
     * Schließt die übergebene Session
     * @param session zu schließenden Session
     */
    public void closeSession(Session session){
        if(session != null && session.isOpen())
            session.close();
    }
}
