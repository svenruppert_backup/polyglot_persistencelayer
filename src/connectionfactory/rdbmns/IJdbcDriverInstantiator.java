package connectionfactory.rdbmns;

import java.io.IOException;
import java.sql.Connection;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/29/12
 * Time: 12:45 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IJdbcDriverInstantiator {

    /**
     * Gibt zu einem gegebenen Datenbanknamen oder Pfad eine JDBC Connection
     * zurück.
     *
     * @param databaseName   Datenbankname oder Pfad.
     * @return JDBC Connection Instanz.
     */
    public Connection instantiateConnection(String databaseName);
}
