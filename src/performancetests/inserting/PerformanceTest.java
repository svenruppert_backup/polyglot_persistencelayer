package performancetests.inserting;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.kernel.Traversal;
import org.neo4j.test.ImpermanentGraphDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 06.06.12
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */

/**
 * Klasse zum erstellen von Graphendatenbanken verschiedener Größe und Struktur (Vollvermascht und Baumförmig).
 */
public class PerformanceTest {
    private GraphDatabaseService graphDB;
    RelationshipType REL = DynamicRelationshipType.withName("REL");
    //List<Node> nodes = new ArrayList<Node>(1000000);
    private int amountNodes;
    private static final String DIR="./GraphDatabases/testGraphDb";

    /**
     *  Konstruktor
     * @param amountNodes Anzahl zu erstellender Knoten.
     * @param impermanent Soll die Db nur im Speicher gehalten werden?
     */
    public PerformanceTest(int amountNodes, boolean impermanent) {
        this.amountNodes = amountNodes;
        if (impermanent)
            graphDB = new ImpermanentGraphDatabase();
        else
            graphDB = new EmbeddedGraphDatabase(DIR);
        registerShutdownHook(graphDB);
    }

    /**
     * Erzeuge eine Vollvermaschte Datenbank
     */
    public void createFullyConnectedNodes() {
        Transaction tx = graphDB.beginTx();
        try{
            Node node;
            for (int i = 0; i < amountNodes; i++) {
                node = graphDB.createNode();
                for (Relationship n : graphDB.getNodeById(i).getRelationships()) {
                    node.createRelationshipTo(n.getEndNode(), REL);
                }
                    node.createRelationshipTo(graphDB.getNodeById(i), REL);
            }
            tx.success();
        } finally {
            tx.finish();
        }
    }

    /**
     * Füge einen Knoten in die Vollvermaschte Datenbank ein
     */
    public void addOneNodeFullyConnected() {
        Transaction tx = graphDB.beginTx();
        try{
            Node node = graphDB.createNode();
            for (Relationship n : graphDB.getNodeById(amountNodes - 1).getRelationships()) {
                node.createRelationshipTo(n.getEndNode(), REL);
            }
            node.createRelationshipTo(graphDB.getNodeById(amountNodes - 1), REL);

            tx.success();
        } finally {
            tx.finish();
        }
    }

    /**
     * Erzeuge eine Baumförmige Datenbank
     * @param amountChildren Anzahl der Kinder pro Blatt
     */
    public void createTreeNodes(int amountChildren) {
        Transaction tx = graphDB.beginTx();
        try{
            Node nodeParent, nodeChild;
            int countNodes = 1;
            breakpoint:
            for (int j = 0; j < amountNodes; j++) {
                nodeParent = graphDB.getNodeById(j);
                for (int i = 0; i < amountChildren; i++) {
                    nodeChild = graphDB.createNode();
                    nodeChild.createRelationshipTo(nodeParent, REL);
                    if (countNodes >= amountNodes) break breakpoint;
                    countNodes++;
                }
            }


            tx.success();
        } finally {
            tx.finish();
        }
    }

    /**
     * Füge einen Knoten in die Baumförmige Datenbank ein
     */
    public void addOneNodeToTree() {
        Transaction tx = graphDB.beginTx();
        try{
            Node node = graphDB.createNode();
            node.createRelationshipTo(graphDB.getNodeById(amountNodes - 1),REL);
            tx.success();
        } finally {
            tx.finish();
        }
    }


    /**
     * Suche alle Knoten unterhalb von...
     * @param node  Knoten unter dem gesucht werden soll
     * @return  Traverser mit den Pfaden der Knoten
     */
    public Traverser searchAllNodesFrom(final Node node) {
        TraversalDescription td = Traversal.description()
                .breadthFirst()
                .relationships(REL, Direction.BOTH)
                .evaluator(Evaluators.excludeStartPosition());
        return td.traverse( node );
    }

    public GraphDatabaseService getGraphDB() {
        return graphDB;
    }

    public void shutdown() {
        graphDB.shutdown();
    }


    private static void registerShutdownHook(final GraphDatabaseService graphDb)
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running example before it's completed)
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    /**
     * Löscht den Datenbankordner
     * @return Erfolgreich?
     */
    public static boolean deleteDBDir() {
        File dbdir = new File(DIR);
        return deleteDir(dbdir);
    }


    // Deletes all files and subdirectories under dir.
    // Returns true if all deletions were successful.
    // If a deletion fails, the method stops attempting to delete and returns false.
    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }
}
