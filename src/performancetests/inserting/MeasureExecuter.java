package performancetests.inserting;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 06.06.12
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer zum Messen von Ausführungszeiten
 */
public abstract class MeasureExecuter {

    private FileWriter fstream;
    private final PerformanceTest perftest;
    private final int amountNodes;

    /**
     * Konstruktor
     * @param perf          PerformanceTest objekt auf das der Messung ausgeführt wird
     * @param amountNodes   Anzahl an Knoten die Erstellt werden sollen
     * @param outFile       Name der Ausgabedatei
     * @param append        Soll an die Datei angehängt werden?
     */
    protected MeasureExecuter(PerformanceTest perf, int amountNodes, String outFile, boolean append) {
        if (perf == null)
            throw new IllegalArgumentException("Perf was null.");

        if (amountNodes < 0)
            throw new IllegalArgumentException("amountNodes must be positiv.");

        if (outFile == null || outFile == "")
            throw new IllegalArgumentException("outFile was null or empty.");

        perftest = perf;
        this.amountNodes = amountNodes;
        try {
            fstream = new FileWriter(outFile, append);
        } catch (IOException e) { e.printStackTrace(); }
    }


    public PerformanceTest getPerftest() {
        return perftest;
    }

    public int getAmountNodes() {
        return amountNodes;
    }

    public abstract void dotask();

    /**
     * Text der bei der Ausgabe vorangestellt wird
     * @return
     */
    public abstract String outputInfo();

    public void execute() {
        final long startTime = System.nanoTime();
        dotask();
        final long endTime = System.nanoTime();
        try{
            int mb = 1024 * 1024;
            BufferedWriter out = new BufferedWriter(fstream);
                    String outString = (outputInfo() + "    " + (endTime - startTime)/Math.pow(10, 9) + "   " +
                    (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())/mb + "\n");
            out.write(outString);
            out.close();
            System.out.printf(outString);
        }catch (Exception e){ System.err.println("Error: " + e.getMessage()); }
    }
}
