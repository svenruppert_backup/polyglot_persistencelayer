package performancetests;

import performancetests.converter.impl.ToFullyConnectedConverter;
import performancetests.converter.impl.ToRdbmsToTreeShaped;
import performancetests.converter.impl.attributepersistingsstrategie.NodeAttributesNodeBuilder;
import performancetests.converter.impl.attributepersistingsstrategie.PropertyNodesNodeBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.PropertyNodesResultBuilder;
import performancetests.querying.neo4j.fullyconnectedgraph.BasicFullyConnectedQuery;
import performancetests.querying.neo4j.fullyconnectedgraph.CypherFullyConnectedQuery;
import performancetests.querying.neo4j.fullyconnectedgraph.TraversalFullyConnectedQuery;
import performancetests.querying.neo4j.treeshapedgraph.BasicTreeShapedQuery;
import performancetests.querying.neo4j.treeshapedgraph.CypherTreeShapedQuery;
import performancetests.querying.neo4j.treeshapedgraph.TraversalTreeShapedQuery;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/31/12
 * Time: 12:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class GraphDbServiceRegistry {

    private Map<String, String> pathToClassNames = new HashMap<>();

    /**
     * Konstructor.
     * Füllt eine lokale Map, mit hilfe derer, für alle Abfrage Klassen
     * und die Converter, anhand der Klassennamen bestimmt wird, welcher
     * Graphendatenbankpfad/welche Graphendatenbank benutzt werden muss.
     */
    public GraphDbServiceRegistry(){
        pathToClassNames.put(concatClassNames(BasicFullyConnectedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.FC_NODEATTRIBUTES);
        pathToClassNames.put(concatClassNames(CypherFullyConnectedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.FC_NODEATTRIBUTES);
        pathToClassNames.put(concatClassNames(TraversalFullyConnectedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.FC_NODEATTRIBUTES);

        pathToClassNames.put(concatClassNames(BasicFullyConnectedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.FC_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(CypherFullyConnectedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.FC_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(TraversalFullyConnectedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.FC_PROPERTYNODES);

        pathToClassNames.put(concatClassNames(BasicTreeShapedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.TR_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(CypherTreeShapedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.TR_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(TraversalTreeShapedQuery.class, PropertyNodesResultBuilder.class),
                GraphServiceFactory.TR_PROPERTYNODES);

        pathToClassNames.put(concatClassNames(BasicTreeShapedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.TREE_NODEATTRIBUTES);
        pathToClassNames.put(concatClassNames(CypherTreeShapedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.TREE_NODEATTRIBUTES);
        pathToClassNames.put(concatClassNames(TraversalTreeShapedQuery.class, NodeAttributesResultBuilder.class),
                GraphServiceFactory.TREE_NODEATTRIBUTES);



        pathToClassNames.put(concatClassNames(ToFullyConnectedConverter.class,
                PropertyNodesNodeBuilder.class),
                GraphServiceFactory.FC_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(ToFullyConnectedConverter.class,
                NodeAttributesNodeBuilder.class),
                GraphServiceFactory.FC_NODEATTRIBUTES);

        pathToClassNames.put(concatClassNames(ToRdbmsToTreeShaped.class,
                PropertyNodesNodeBuilder.class),
                GraphServiceFactory.TR_PROPERTYNODES);
        pathToClassNames.put(concatClassNames(ToRdbmsToTreeShaped.class,
                NodeAttributesNodeBuilder.class),
                GraphServiceFactory.TREE_NODEATTRIBUTES);

    }

    /**
     * Erstellt den Schlüssel, unter dem der Pfad der
     * Datenbank in der Map abgelegt ist.
     *
     * @param firstClass    Erste Klasse (Baumtypus)
     * @param secondClass Zweige Klasse (Speicherart für Daten)
     * @return Schlüssel für lokale Map.
     */
    private String concatClassNames(final Class firstClass, final Class secondClass){
        final String firstClassName = firstClass.getSimpleName();
        final String secondClassName = secondClass.getSimpleName();
        final StringBuilder concatClassNameBuilder = new StringBuilder()
                .append(firstClassName)
                .append(secondClassName);
        return concatClassNameBuilder.toString();
    }

    /**
     * Gibt den Pfad zu der Graphendatenbank zurück, die durch
     * Graphenform und Datenspeicherungs Strategie bestimmt wird.
     *
     * @param firstClass Graphenform.
     * @param secondClass Datenspeicherungs Strategie
     * @return Pfad zur Graphendatenbank.
     */
    public String getGraphDbName(Class firstClass, Class secondClass){
        final String key = concatClassNames(firstClass, secondClass);
        String path = null;
        if(pathToClassNames.containsKey(key)){
            path = pathToClassNames.get(key);
            if(GraphServiceFactory.isInTestMode())
                path = path + "_TEST";
        }
        else {
            String message = String.format("No GrapgDatabaseService mapping found for %s and %s. ",
                    firstClass.getSimpleName(), secondClass.getSimpleName());
            throw new IllegalArgumentException(message);
        }

        return path;
    }
}
