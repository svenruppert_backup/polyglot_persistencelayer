package performancetests.querying.sql;

import org.intellij.lang.annotations.Language;
import performancetests.querying.AbstractPerformancetestQuery;
import connectionfactory.rdbmns.JdbcConnectionFactory;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 19.06.12
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public class SqlPerformancetestQuery extends AbstractPerformancetestQuery<String[]> {
    @Language("SQL")
    private final static String queryString  =   "select t0.text_val as ZIP, t1.text_val as City " +
            "from geodb_textdata t0, geodb_textdata t1 " +
            "where t0.loc_id=t1.loc_id " +
            "and t0.valid_until=t1.valid_until " +
            "and t1.text_locale ='de' " +
            "and t0.text_type=(select n.type_id from geodb_type_names n where n.name LIKE 'Postleitzahl') " +
            "and t1.text_type=(select n.type_id from geodb_type_names n where n.name LIKE 'Name') " +
            "and t0.loc_id IN ( " +
            "select t0.loc_id from geodb_textdata t0 " +
            "where t0.loc_id IN ( " +
            "select h.loc_id from geodb_hierarchies h " +
            "where h.level=6 " +
            "and h.id_lvl2=105 " +
            "))";

    private final JdbcConnectionFactory connectionFactory = new JdbcConnectionFactory();
    protected Connection connection;

    /**
     * Konstruktor,
     * Übergabe des Dateinamens für das Performancetestergebnis.
     */
    public SqlPerformancetestQuery() {
        super("SqlPerformanceTest");
    }

    /**
     * Öffnen der JDBC Connetion zur Datenbank.
     */
    @Override
    public void openDatabaseConnection() {
        connection = connectionFactory.getJdbcConnection();
    }

    /**
     * Ausführung der Testabfrage.
     */
    @Override
    public void performTestQuery() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(queryString);
            while(resultSet.next())
                createResultList(resultSet);

        } catch (SQLException e) { e.printStackTrace(); }
    }

    /**
     * ErgebnisList aus ErgebnisSet zusammenbauen.
     *
     * @param resultSet ResultSet der SQL Abfrage
     * @throws SQLException
     */
    protected void createResultList(ResultSet resultSet) throws SQLException{
    if(resultSet != null){
        String[] result = new String[2];
        result[0] = resultSet.getString("ZIP").trim();
        result[1] = resultSet.getString("City").trim();
        resultList.add(result);
    }
}

    /**
     * Schliessen der Datenbankverbindung
     */
    @Override
    public void closeDatabaseConnection() {
        connectionFactory.closeConnection(connection);
    }
}
