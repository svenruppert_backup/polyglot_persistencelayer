package performancetests.querying.hibernate;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import performancetests.querying.AbstractPerformancetestQuery;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 18.06.12
 * Time: 23:32
 * To change this template use File | Settings | File Templates.
 */
public class HibernatePerformanceTest extends AbstractPerformancetestQuery<String[]> {
    private Session hibernateSession;
    private JdbcConnectionFactory jdbcConnectionFactory;
    private final static String queryString =   "select t0.textVal, t1.textVal   " +
                                                "from GeodbTextdataEntity as t0, GeodbTextdataEntity as t1   " +
                                                "where t0.locId=t1.locId   " +
                                                "and t0.validUntil=t1.validUntil   " +
                                                "and t0.textType=(select n1.typeId from GeodbTypeNamesEntity as n1 where n1.name LIKE 'Postleitzahl')   " +
                                                "and t1.textType=(select n2.typeId from GeodbTypeNamesEntity as n2 where n2.name LIKE 'Name')   " +
                                                "and t1.nativeLang=1   " +
                                                "and t1.defaultName=1   " +
                                                "and t0.locId IN (   " +
                                                "select t0.locId from GeodbTextdataEntity as t0   " +
                                                "where t0.locId IN (   " +
                                                "select h1.locId from GeodbHierarchiesEntity as h1   " +
                                                "where h1.level=6   " +
                                                "and h1.idLvl2=105   " +
                                                "))";

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public HibernatePerformanceTest() {
        super("HibernatePerformancetest.txt");
        jdbcConnectionFactory = new JdbcConnectionFactory();
    }

    /**
     * Öffnen der Datenbankverbindung
     */
    @Override
    public void openDatabaseConnection() {
        hibernateSession = jdbcConnectionFactory.getHibernateSession();
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        Query hqlQuery = hibernateSession.createQuery(queryString);
        resultList.addAll(hqlQuery.list());
    }

    /**
     * Schließen der Datenbankverbindung.
     */
    @Override
    public void closeDatabaseConnection() {
        jdbcConnectionFactory.closeSession(hibernateSession);
    }
}
