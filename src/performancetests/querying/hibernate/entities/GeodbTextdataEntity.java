package performancetests.querying.hibernate.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 18.06.12
 * Time: 23:42
 * To change this template use File | Settings | File Templates.
 */
@javax.persistence.Table(name = "geodb_textdata")
@Entity
public class GeodbTextdataEntity {
    private int locId;

    @javax.persistence.Column(name = "loc_id")
    @Basic
    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    private int textType;

    @javax.persistence.Column(name = "text_type")
    @Basic
    public int getTextType() {
        return textType;
    }

    public void setTextType(int textType) {
        this.textType = textType;
    }

    private String textVal;

    @javax.persistence.Column(name = "text_val")
    @Basic
    public String getTextVal() {
        return textVal;
    }

    public void setTextVal(String textVal) {
        this.textVal = textVal;
    }

    private String textLocale;

    @javax.persistence.Column(name = "text_locale")
    @Basic
    public String getTextLocale() {
        return textLocale;
    }

    public void setTextLocale(String textLocale) {
        this.textLocale = textLocale;
    }

    private short isNativeLang;

    @javax.persistence.Column(name = "is_native_lang")
    @Basic
    public short getNativeLang() {
        return isNativeLang;
    }

    public void setNativeLang(short nativeLang) {
        isNativeLang = nativeLang;
    }

    private short isDefaultName;

    @javax.persistence.Column(name = "is_default_name")
    @Basic
    public short getDefaultName() {
        return isDefaultName;
    }

    public void setDefaultName(short defaultName) {
        isDefaultName = defaultName;
    }

    private Date validSince;

    @javax.persistence.Column(name = "valid_since")
    @Basic
    public Date getValidSince() {
        return validSince;
    }

    public void setValidSince(Date validSince) {
        this.validSince = validSince;
    }

    private int dateTypeSince;

    @javax.persistence.Column(name = "date_type_since")
    @Basic
    public int getDateTypeSince() {
        return dateTypeSince;
    }

    public void setDateTypeSince(int dateTypeSince) {
        this.dateTypeSince = dateTypeSince;
    }

    private Date validUntil;

    @javax.persistence.Column(name = "valid_until")
    @Basic
    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    private int dateTypeUntil;

    @javax.persistence.Column(name = "date_type_until")
    @Basic
    public int getDateTypeUntil() {
        return dateTypeUntil;
    }

    public void setDateTypeUntil(int dateTypeUntil) {
        this.dateTypeUntil = dateTypeUntil;
    }

    private int id;

    @javax.persistence.Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeodbTextdataEntity that = (GeodbTextdataEntity) o;

        if (dateTypeSince != that.dateTypeSince) return false;
        if (dateTypeUntil != that.dateTypeUntil) return false;
        if (id != that.id) return false;
        if (isDefaultName != that.isDefaultName) return false;
        if (isNativeLang != that.isNativeLang) return false;
        if (locId != that.locId) return false;
        if (textType != that.textType) return false;
        if (textLocale != null ? !textLocale.equals(that.textLocale) : that.textLocale != null) return false;
        if (textVal != null ? !textVal.equals(that.textVal) : that.textVal != null) return false;
        if (validSince != null ? !validSince.equals(that.validSince) : that.validSince != null) return false;
        if (validUntil != null ? !validUntil.equals(that.validUntil) : that.validUntil != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = locId;
        result = 31 * result + textType;
        result = 31 * result + (textVal != null ? textVal.hashCode() : 0);
        result = 31 * result + (textLocale != null ? textLocale.hashCode() : 0);
        result = 31 * result + (int) isNativeLang;
        result = 31 * result + (int) isDefaultName;
        result = 31 * result + (validSince != null ? validSince.hashCode() : 0);
        result = 31 * result + dateTypeSince;
        result = 31 * result + (validUntil != null ? validUntil.hashCode() : 0);
        result = 31 * result + dateTypeUntil;
        result = 31 * result + id;
        return result;
    }
}
