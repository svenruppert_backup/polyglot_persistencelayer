package performancetests.querying.hibernate.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 18.06.12
 * Time: 23:42
 * To change this template use File | Settings | File Templates.
 */
@javax.persistence.Table(name = "geodb_hierarchies")
@Entity
public class GeodbHierarchiesEntity {
    private int locId;

    @javax.persistence.Column(name = "loc_id")
    @Basic
    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    private int level;

    @javax.persistence.Column(name = "level")
    @Basic
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    private int idLvl1;

    @javax.persistence.Column(name = "id_lvl1")
    @Basic
    public int getIdLvl1() {
        return idLvl1;
    }

    public void setIdLvl1(int idLvl1) {
        this.idLvl1 = idLvl1;
    }

    private int idLvl2;

    @javax.persistence.Column(name = "id_lvl2")
    @Basic
    public int getIdLvl2() {
        return idLvl2;
    }

    public void setIdLvl2(int idLvl2) {
        this.idLvl2 = idLvl2;
    }

    private int idLvl3;

    @javax.persistence.Column(name = "id_lvl3")
    @Basic
    public int getIdLvl3() {
        return idLvl3;
    }

    public void setIdLvl3(int idLvl3) {
        this.idLvl3 = idLvl3;
    }

    private int idLvl4;

    @javax.persistence.Column(name = "id_lvl4")
    @Basic
    public int getIdLvl4() {
        return idLvl4;
    }

    public void setIdLvl4(int idLvl4) {
        this.idLvl4 = idLvl4;
    }

    private int idLvl5;

    @javax.persistence.Column(name = "id_lvl5")
    @Basic
    public int getIdLvl5() {
        return idLvl5;
    }

    public void setIdLvl5(int idLvl5) {
        this.idLvl5 = idLvl5;
    }

    private int idLvl6;

    @javax.persistence.Column(name = "id_lvl6")
    @Basic
    public int getIdLvl6() {
        return idLvl6;
    }

    public void setIdLvl6(int idLvl6) {
        this.idLvl6 = idLvl6;
    }

    private int idLvl7;

    @javax.persistence.Column(name = "id_lvl7")
    @Basic
    public int getIdLvl7() {
        return idLvl7;
    }

    public void setIdLvl7(int idLvl7) {
        this.idLvl7 = idLvl7;
    }

    private int idLvl8;

    @javax.persistence.Column(name = "id_lvl8")
    @Basic
    public int getIdLvl8() {
        return idLvl8;
    }

    public void setIdLvl8(int idLvl8) {
        this.idLvl8 = idLvl8;
    }

    private int idLvl9;

    @javax.persistence.Column(name = "id_lvl9")
    @Basic
    public int getIdLvl9() {
        return idLvl9;
    }

    public void setIdLvl9(int idLvl9) {
        this.idLvl9 = idLvl9;
    }

    private Date validSince;

    @javax.persistence.Column(name = "valid_since")
    @Basic
    public Date getValidSince() {
        return validSince;
    }

    public void setValidSince(Date validSince) {
        this.validSince = validSince;
    }

    private int dateTypeSince;

    @javax.persistence.Column(name = "date_type_since")
    @Basic
    public int getDateTypeSince() {
        return dateTypeSince;
    }

    public void setDateTypeSince(int dateTypeSince) {
        this.dateTypeSince = dateTypeSince;
    }

    private Date validUntil;

    @javax.persistence.Column(name = "valid_until")
    @Basic
    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    private int dateTypeUntil;

    @javax.persistence.Column(name = "date_type_until")
    @Basic
    public int getDateTypeUntil() {
        return dateTypeUntil;
    }

    public void setDateTypeUntil(int dateTypeUntil) {
        this.dateTypeUntil = dateTypeUntil;
    }

    private int id;

    @javax.persistence.Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeodbHierarchiesEntity that = (GeodbHierarchiesEntity) o;

        if (dateTypeSince != that.dateTypeSince) return false;
        if (dateTypeUntil != that.dateTypeUntil) return false;
        if (id != that.id) return false;
        if (idLvl1 != that.idLvl1) return false;
        if (idLvl2 != that.idLvl2) return false;
        if (idLvl3 != that.idLvl3) return false;
        if (idLvl4 != that.idLvl4) return false;
        if (idLvl5 != that.idLvl5) return false;
        if (idLvl6 != that.idLvl6) return false;
        if (idLvl7 != that.idLvl7) return false;
        if (idLvl8 != that.idLvl8) return false;
        if (idLvl9 != that.idLvl9) return false;
        if (level != that.level) return false;
        if (locId != that.locId) return false;
        if (validSince != null ? !validSince.equals(that.validSince) : that.validSince != null) return false;
        if (validUntil != null ? !validUntil.equals(that.validUntil) : that.validUntil != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = locId;
        result = 31 * result + level;
        result = 31 * result + idLvl1;
        result = 31 * result + idLvl2;
        result = 31 * result + idLvl3;
        result = 31 * result + idLvl4;
        result = 31 * result + idLvl5;
        result = 31 * result + idLvl6;
        result = 31 * result + idLvl7;
        result = 31 * result + idLvl8;
        result = 31 * result + idLvl9;
        result = 31 * result + (validSince != null ? validSince.hashCode() : 0);
        result = 31 * result + dateTypeSince;
        result = 31 * result + (validUntil != null ? validUntil.hashCode() : 0);
        result = 31 * result + dateTypeUntil;
        result = 31 * result + id;
        return result;
    }
}
