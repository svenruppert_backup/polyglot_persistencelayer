package performancetests.querying.hibernate.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 18.06.12
 * Time: 23:42
 * To change this template use File | Settings | File Templates.
 */
@javax.persistence.Table(name = "geodb_type_names")
@Entity
public class GeodbTypeNamesEntity {
    private int typeId;

    @javax.persistence.Column(name = "type_id")
    @Basic
    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    private String typeLocale;

    @javax.persistence.Column(name = "type_locale")
    @Basic
    public String getTypeLocale() {
        return typeLocale;
    }

    public void setTypeLocale(String typeLocale) {
        this.typeLocale = typeLocale;
    }

    private String name;

    @javax.persistence.Column(name = "name")
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int id;

    @javax.persistence.Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeodbTypeNamesEntity that = (GeodbTypeNamesEntity) o;

        if (id != that.id) return false;
        if (typeId != that.typeId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (typeLocale != null ? !typeLocale.equals(that.typeLocale) : that.typeLocale != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = typeId;
        result = 31 * result + (typeLocale != null ? typeLocale.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
