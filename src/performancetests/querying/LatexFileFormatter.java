package performancetests.querying;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 02.07.12
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */
public class LatexFileFormatter {
    private final List<AbstractPerformancetestQuery> performancetestQueryList;

    public LatexFileFormatter(List<AbstractPerformancetestQuery> performancetestQueryList) {
        if(performancetestQueryList == null)
            throw new NullPointerException("PerformancetestFileFormatter: performanceTestQuery was null.");

        this.performancetestQueryList = performancetestQueryList;
    }

    public void writeResultsToFile(){
        double sumTimeNeede = 0;
        try{
            final BufferedWriter writer =
                    Files.newBufferedWriter(Paths.get("performancetestresults.csv"), Charset.defaultCharset(), StandardOpenOption.CREATE_NEW);

            StringBuilder cloumnCaptions = new StringBuilder();
            cloumnCaptions.append("count")
            .append("\t");
            for(int i=0; i<performancetestQueryList.size(); i++)
                cloumnCaptions.append(performancetestQueryList.get(i).getResultFilePath()).append("\t");
            cloumnCaptions.append("\n");
            writer.write(cloumnCaptions.toString());

            for(int i=0; i<50; i++){
                StringBuilder lineBuilder = new StringBuilder();
                lineBuilder.append(i)
                        .append("\t");
                for(int j=0; j<performancetestQueryList.size(); j++){
                    lineBuilder.append(performancetestQueryList.get(j).getPeformancetestResults().get(i))
                            .append("\t");
                }
                lineBuilder.append("\n");
                writer.write(lineBuilder.toString());
            }

            StringBuilder lineB = new StringBuilder();
            for(int i=0;i<performancetestQueryList.size();i++){
                double averageTimeNeeded = 0;
                List<Double> results = performancetestQueryList.get(i).getPeformancetestResults();
                for(int j=1; j<results.size();j++){
                    averageTimeNeeded += results.get(j);
                }
                averageTimeNeeded = averageTimeNeeded / 49;
                lineB.append(averageTimeNeeded).append("\t");
            }

            writer.write(lineB.toString());
            writer.write("\n");

            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.err.println("Could not write Result of file " );
        }
    }
}
