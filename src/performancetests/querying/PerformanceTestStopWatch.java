package performancetests.querying;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 06.06.12
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
public class PerformanceTestStopWatch {

    /**
     * Sorgt dafür, dass die Performancetests durchgeführt, und die Zeit gemessen wird.
     * Fügt die gemessenen Zeiten an die Liste an.
     *
     * @param performancetestQueryList Liste an durchzuführenden Performancetests.
     * @param amount Anzahl an Test Durchläufen.
     */
    public void execute(List<AbstractPerformancetestQuery> performancetestQueryList, long amount ) {
        if(performancetestQueryList == null)
            throw new NullPointerException("PerformancetestStopWatch: PerformancetestQuery was null");

        for(AbstractPerformancetestQuery performancetestQuery : performancetestQueryList){
            performancetestQuery.openDatabaseConnection();
            for(int i=0; i<amount; i++){
                performancetestQuery.getResultList().clear();
                final long startTime = System.nanoTime();
                performancetestQuery.performTestQuery();
                final long endTime = System.nanoTime();
                performancetestQuery.getPeformancetestResults().add((endTime - startTime)/Math.pow(10, 9));
            }
            performancetestQuery.closeDatabaseConnection();
        }
    }
}