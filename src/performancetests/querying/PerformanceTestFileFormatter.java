package performancetests.querying;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 02.07.12
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */
public class PerformanceTestFileFormatter {
    private final List<AbstractPerformancetestQuery> performancetestQueryList;

    /**
     * Konstruktor.
     * Nimmt eine Liste an Performancetest als Parameter, alle hieriin gespeicherten
     * Ergebniszeiten werden in eine Dateo geschrieben.
     * Zudem wird die durchschnittlich benötigte Zeit berechnet und auch
     * in die Datei geschrieben.
     *
     * @param performancetestQueryList
     */
    public PerformanceTestFileFormatter(List<AbstractPerformancetestQuery> performancetestQueryList) {
        if(performancetestQueryList == null)
            throw new NullPointerException("PerformancetestFileFormatter: performanceTestQuery was null.");

        this.performancetestQueryList = performancetestQueryList;
    }

    public void writeResultsToFile(){
        for(AbstractPerformancetestQuery performancetestQuery : performancetestQueryList){
            final List<Double> results = performancetestQuery.getPeformancetestResults();

            double sumTimeNeede = 0;
            String filePath = performancetestQuery.getResultFilePath();
            try{
                Path path = Paths.get(filePath);
                final BufferedWriter writer =
                        Files.newBufferedWriter(path, Charset.defaultCharset(), StandardOpenOption.CREATE_NEW);
                int length = results.size();
                for(int i=0; i<length; i++){
                    double result = results.get(i);
                    String line = String.format("%2d.\t%f\n", i, result);
                    sumTimeNeede += result;
                    writer.write(line);
                }

                sumTimeNeede -= results.get(0);
                int sumTestCount = results.size() - 1;
                double averageTimeNeeded = sumTimeNeede / sumTestCount;

                String line = String.format("\nAverage Time: %f", averageTimeNeeded);
                writer.write(line);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                System.err.println("Could not write Result of file " + filePath );
            }
        }
    }
}
