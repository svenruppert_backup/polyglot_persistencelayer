package performancetests.querying;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 19.06.12
 * Time: 00:24
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractPerformancetestQuery<T> {
    protected final List<Double> performanceTestResults = new ArrayList<>();
    protected final List<T> resultList = new ArrayList<>();
    private final String fileName;

    /**
     * Konstruktor.
     * @param fileName Dateiname für Performancetest Ergebnis Datei.
     */
    protected AbstractPerformancetestQuery(String fileName) {
        if (fileName == null || fileName == "")
            throw new IllegalArgumentException("resultFilePath must not be null or emtpy.");
        this.fileName = fileName;
    }

    abstract public void openDatabaseConnection();
    abstract public void performTestQuery();
    abstract public void closeDatabaseConnection();


    /**
     * @return Getter für die Ergebnisliste der Performancetests.
     */
    public List<T> getResultList() {
        return resultList;
    }

    /**
     * @return Getter für die gemessenen Zeiten.
     */
    public List<Double> getPeformancetestResults(){
        return performanceTestResults;
    }

    /**
     * @return Getter für den Pfad der ErgebnisDateo.
     */
    public String getResultFilePath() {
        return fileName;
    }
}
