package performancetests.querying.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import performancetests.GraphDbServiceRegistry;
import performancetests.GraphServiceFactory;
import performancetests.IndexRegistry;
import performancetests.RelationsRegistry;
import performancetests.querying.AbstractPerformancetestQuery;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/27/12
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractGraphDbPerformancetestQuery extends AbstractPerformancetestQuery<String[]> {

    protected final RelationsRegistry relationsRegistry= new RelationsRegistry();
    protected  GraphDatabaseService graphDbService;
    protected  IndexRegistry indexRegistry;
    private    String graphDb;
    protected IResultBuilder resultBuilder;
    protected String resultFilePath;

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public AbstractGraphDbPerformancetestQuery(Class<? extends AbstractGraphDbPerformancetestQuery> graphShape,
                                               Class<? extends IResultBuilder> valueStrategie) throws IllegalAccessException, InstantiationException {

        super(prepare(graphShape, valueStrategie));
        this.graphDb = new GraphDbServiceRegistry().getGraphDbName(graphShape, valueStrategie);
        createResultFilePath(graphShape, valueStrategie);
        resultBuilder = valueStrategie.newInstance();
    }

    /**
     * Erzeugt den Dateinamen die die Performancetestergebnisse aus den beiden übergebenen Klassen.
     *
     * @param graphShape
     * @param valueStrategie
     * @return
     */
    private static String prepare(Class<? extends AbstractGraphDbPerformancetestQuery> graphShape,
                                  Class<? extends IResultBuilder> valueStrategie) {
        if(graphShape == null)
            throw new NullPointerException("AbstractGraphDbPerformancetestQuery: Given Class Class (extending AbstractGraphDbPerformanceQuery) was null.");
        if(valueStrategie == null)
            throw new NullPointerException("AbstractGraphDbPerformancetestQuery: Given Class Class (extending IResultBuilder) was null..");
        return graphShape.getSimpleName()  + valueStrategie.getSimpleName() + ".txt";
    }


    /**
     * Öffnen der Datenbankverbindung.
     */
    @Override
    public void openDatabaseConnection() {
        graphDbService = GraphServiceFactory.getGraphService(graphDb);
        indexRegistry = new IndexRegistry(graphDbService);
    }

    /**
     * Schließen der Datenbankverbindung.
     */
    @Override
    public void closeDatabaseConnection() {
        GraphServiceFactory.shutDownDatabase(graphDb);
    }

    /**
     * Getter für den Dateinamen.
     * @return String, der den Pfad zur Ergebnisdatei enthält.
     */
    public String getResultFilePath() {
        return resultFilePath;
    }

    /**
     * Erzeugt den Dateinamen aus der Concatination der beiden übergenenen Klassen.
     * @param graphShape
     * @param valueStrategie
     */
    private void createResultFilePath(Class<? extends AbstractGraphDbPerformancetestQuery> graphShape, Class<? extends IResultBuilder> valueStrategie) {
        StringBuilder resultPathBuilder = new StringBuilder();
        resultPathBuilder.append(graphShape.getSimpleName())
                .append("_")
                .append(valueStrategie.getSimpleName())
                .append(".txt");
        resultFilePath = resultPathBuilder.toString();
    }
}
