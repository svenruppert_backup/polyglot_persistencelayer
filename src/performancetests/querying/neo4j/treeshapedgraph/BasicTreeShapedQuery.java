package performancetests.querying.neo4j.treeshapedgraph;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 29.06.12
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
public class BasicTreeShapedQuery extends AbstractGraphDbPerformancetestQuery {


    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public BasicTreeShapedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(BasicTreeShapedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        Node deutschlandNode = indexRegistry.getLocIdIndex().get("loc_id", 105).getSingle();
        if (deutschlandNode != null) {
            recursiveTestQuery(deutschlandNode, 6);
        }
    }


    private void recursiveTestQuery(Node startNode, int lvl){
        for(int i=1; i<=lvl; i++)
            for(Relationship relation : startNode.getRelationships(relationsRegistry.getRelation(i), Direction.OUTGOING))
                if(relation.isType(relationsRegistry.getRelation(lvl)))
                    resultBuilder.addResultToResultList(relation.getEndNode(), resultList);
                else
                    recursiveTestQuery(relation.getEndNode(), lvl);

    }
}
