package performancetests.querying.neo4j.treeshapedgraph;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Node;
import org.neo4j.helpers.collection.IteratorUtil;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 02.07.12
 * Time: 00:33
 * To change this template use File | Settings | File Templates.
 */
public class CypherTreeShapedQuery extends AbstractGraphDbPerformancetestQuery {
    private final static String queryString =   "START n=node:loc_Id(loc_id=\"105\") " +
                                                "MATCH (n)-[*]->(c)-[:POL_GLIEDERUNG]->(x) " +
                                                "RETURN x";

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public CypherTreeShapedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(CypherTreeShapedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        ExecutionEngine executionEngine = new ExecutionEngine(graphDbService);
        ExecutionResult resultNodes = executionEngine.execute(queryString);

        Iterator<Node> n_column = resultNodes.columnAs("x");
        for ( Node cityNode : IteratorUtil.asIterable(n_column) )
            resultBuilder.addResultToResultList(cityNode, resultList);
    }
}
