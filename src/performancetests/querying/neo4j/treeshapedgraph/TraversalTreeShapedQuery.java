package performancetests.querying.neo4j.treeshapedgraph;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.traversal.*;
import org.neo4j.graphdb.traversal.Traverser;
import org.neo4j.kernel.Traversal;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 29.06.12
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 */
public class TraversalTreeShapedQuery<T> extends AbstractGraphDbPerformancetestQuery {

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public TraversalTreeShapedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(TraversalTreeShapedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        Node deutschlandNode;
        deutschlandNode = indexRegistry.getLocIdIndex().get("loc_id", 105).getSingle();
        if (deutschlandNode != null) {
            TraversalDescription td = Traversal.description()
                    .depthFirst()
                    .relationships(relationsRegistry.getRelation(2), Direction.OUTGOING)
                    .relationships(relationsRegistry.getRelation(3), Direction.OUTGOING)
                    .relationships(relationsRegistry.getRelation(4), Direction.OUTGOING)
                    .relationships(relationsRegistry.getRelation(5), Direction.OUTGOING)
                    .relationships(relationsRegistry.getRelation(6), Direction.OUTGOING)
                    .evaluator(Evaluators.excludeStartPosition())
                    .evaluator(Evaluators.returnWhereLastRelationshipTypeIs(relationsRegistry.getRelation(6)));

                for(Path path : td.traverse(deutschlandNode))
                    resultBuilder.addResultToResultList(path.endNode(), resultList);
        }
    }

}
