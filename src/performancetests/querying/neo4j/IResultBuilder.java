package performancetests.querying.neo4j;

import org.neo4j.graphdb.Node;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 3:17 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IResultBuilder {

    /**
     * Interface für zum auslesen der Dazen durch implemtierungen
     * dieser Datenspeicherungsstrategoe.
     *
     * @param cityNode Node deren Daten ausgelesen werden.
     * @param resultList Liste, an die das ergebnis angehängt wird.
     */
    public void addResultToResultList(Node cityNode, List<String[]> resultList);

}
