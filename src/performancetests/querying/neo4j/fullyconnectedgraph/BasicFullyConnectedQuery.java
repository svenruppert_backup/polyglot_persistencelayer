package performancetests.querying.neo4j.fullyconnectedgraph;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/26/12
 * Time: 12:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class BasicFullyConnectedQuery extends AbstractGraphDbPerformancetestQuery {

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public BasicFullyConnectedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(BasicFullyConnectedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        Node deutschlandNode = indexRegistry.getLocIdIndex().get("loc_id", 105).getSingle();

        if (deutschlandNode != null) {
            for(Relationship relation : deutschlandNode.getRelationships(relationsRegistry.getRelation(6)))
                resultBuilder.addResultToResultList(relation.getEndNode(), resultList);
        }
    }
}
