package performancetests.querying.neo4j.fullyconnectedgraph;

import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.Node;
import org.neo4j.helpers.collection.IteratorUtil;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/26/12
 * Time: 1:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class CypherFullyConnectedQuery extends AbstractGraphDbPerformancetestQuery {
    private final static String queryString =   "START n=node:loc_Id(loc_id=\"105\") " +
                                                "MATCH (n)-[:POL_GLIEDERUNG]->(x) " +
                                                "RETURN x";

    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public CypherFullyConnectedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(CypherFullyConnectedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        ExecutionEngine executionEngine = new ExecutionEngine(graphDbService);
        ExecutionResult resultNodes = executionEngine.execute(queryString);

        Iterator<Node> n_column = resultNodes.columnAs("x");
        for ( Node node : IteratorUtil.asIterable(n_column) )
            resultBuilder.addResultToResultList(node, resultList);
    }
}
