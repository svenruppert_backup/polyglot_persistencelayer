package performancetests.querying.neo4j.fullyconnectedgraph;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 29.06.12
 * Time: 15:16
 * To change this template use File | Settings | File Templates.
 */
public class TraversalFullyConnectedQuery extends AbstractGraphDbPerformancetestQuery {


    /**
     * Konstruktor.
     * Gibt den Dateinamen für die Performancetest Zeitmessungen
     * den super-Konstruktor weiter.
     */
    public TraversalFullyConnectedQuery(IResultBuilder resultBuilder) throws InstantiationException, IllegalAccessException {
        super(TraversalFullyConnectedQuery.class, resultBuilder.getClass());
    }

    /**
     * Durchfühung der Abfrage
     */
    @Override
    public void performTestQuery() {
        Node deutschlandNode  = indexRegistry.getLocIdIndex().get("loc_id", 105).getSingle();

        if (deutschlandNode != null) {
            TraversalDescription td = Traversal.description()
                    .breadthFirst()
                    .evaluator(Evaluators.atDepth(1))
                    .evaluator(Evaluators.returnWhereLastRelationshipTypeIs(relationsRegistry.getRelation(6)));

            for(Path path : td.traverse(deutschlandNode))
                resultBuilder.addResultToResultList(path.endNode(), resultList);
        }
    }
}
