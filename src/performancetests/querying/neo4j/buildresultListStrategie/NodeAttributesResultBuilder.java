package performancetests.querying.neo4j.buildresultListStrategie;

import org.neo4j.graphdb.Node;
import performancetests.querying.neo4j.IResultBuilder;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 3:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class NodeAttributesResultBuilder implements IResultBuilder {

    @Override
    public void addResultToResultList(Node cityNode, List<String[]> resultList) {
        Iterable<String> nodePropertiesKeysIterator = cityNode.getPropertyKeys();
        for(String nodePropertyKey : nodePropertiesKeysIterator){
            if(!nodePropertyKey.equals("loc_id")){
                String nodePropertyValue = (String) cityNode.getProperty(nodePropertyKey);
                String[] nodePropertieValueArray = nodePropertyValue.split("<NEXT>");

                for(String nodePropertyValueElement : nodePropertieValueArray){
                    String[] result = new String[2];
                    result[0] = nodePropertyKey;
                    result[1] = nodePropertyValueElement;
                    resultList.add(result);
                }
            }
        }
    }
}
