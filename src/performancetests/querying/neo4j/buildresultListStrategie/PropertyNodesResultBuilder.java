package performancetests.querying.neo4j.buildresultListStrategie;

import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import performancetests.querying.neo4j.IResultBuilder;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 3:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropertyNodesResultBuilder implements IResultBuilder {

    @Override
    public void addResultToResultList(Node cityNode, List<String[]> resultList) {
        final DynamicRelationshipType nameRelationshipType =
                DynamicRelationshipType.withName("NAME");

        final DynamicRelationshipType zipCodeRelationshipType =
                DynamicRelationshipType.withName("ZIP_CODE");

        for(Relationship zipRelationhip : cityNode.getRelationships(zipCodeRelationshipType)){
            final String zipValidUntil = (String) zipRelationhip.getProperty("valid_until");
            final String zipValue = (String) zipRelationhip.getEndNode().getProperty("value");

            for(Relationship nameRelationship : cityNode.getRelationships(nameRelationshipType)){
                final String nameValidUntil = (String) nameRelationship.getProperty("valid_until");

                if(zipValidUntil.equals(nameValidUntil)){
                    final String nameValue = (String) nameRelationship.getEndNode().getProperty("value");
                    String[] result = new String[2];
                    result[0] = zipValue;
                    result[1] = nameValue;
                    resultList.add(result);
                }
            }

        }
    }
}
