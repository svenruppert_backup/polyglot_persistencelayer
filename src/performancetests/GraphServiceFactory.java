package performancetests;

import org.neo4j.graphdb.*;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 05.06.12
 * Time: 11:40
 * To change this template use File | Settings | File Templates.
 */

/**
 *  Liefert eine Instanz einer Graphendatenbank
 */
public class GraphServiceFactory {

    public static boolean isInTestMode = false;
    public static final String TR_PROPERTYNODES     = "./Databases/TreeShapedGraphDb/PropertyNodes";
    public static final String TREE_NODEATTRIBUTES  = "./Databases/TreeShapedGraphDb/NodeAttributes";
    public static final String FC_PROPERTYNODES     = "./Databases/FullyConnectedGraphDb/PropertyNodes";
    public static final String FC_NODEATTRIBUTES    = "./Databases/FullyConnectedGraphDb/NodeAttributes";
    public static final String ISSUE_TRACKING       = "./Databases/IssuTracking";

    private static Map<String, GraphDatabaseService> graphDatabaseServiceMap = new HashMap<>();

    /**
     * Gibt die Instanz zu dem Datenbanknamen wieder.
     * @param dbName Name der Datenbank
     * @return Instanz der Datenbank
     */
    public static GraphDatabaseService getGraphService(String dbName) {
        if (dbName == null || dbName == "")
            throw new IllegalArgumentException("Name was null or an empty string.");
        if(!graphDatabaseServiceMap.containsKey(dbName)){
            GraphDatabaseService graphDB = new EmbeddedGraphDatabase(dbName);
            registerShutdownHook(graphDB);
            graphDatabaseServiceMap.put(dbName, graphDB);
        }

        return graphDatabaseServiceMap.get(dbName);
    }

    /**
     * Registers a shutdown hook for the Neo4j instance so that it
     * shuts down nicely when the VM exits (even if you "Ctrl-C" the
     * running example before it's completed)
     *
     * @param graphDb zu registrierendes Datenbankobjekt
     */
    private static void registerShutdownHook(final GraphDatabaseService graphDb)
    {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    /**
     * Schließt die Datenbank.
     * @param dbName Name der Datenbank
     */
    public static void shutDownDatabase(String dbName){
        if(graphDatabaseServiceMap.containsKey(dbName)){
            graphDatabaseServiceMap.get(dbName).shutdown();
            graphDatabaseServiceMap.remove(dbName);
        }
    }

    /**
     * Getter für Klassenvariable, und legt fest welche
     * Art GrapphendatenbankService zurückgegeben wird.
     * @return Zustand
     */
    public static boolean isInTestMode() {
        return isInTestMode;
    }

    /**
     * Getter Methode, um festzustellen in welchen Zustand
     * sich die Klassen befindet, und welche Art Klasse
     * momentan zurückgegeben wird.
     * @param inTestMode
     */
    public static void setInTestMode(boolean inTestMode) {
        isInTestMode = inTestMode;
    }
}
