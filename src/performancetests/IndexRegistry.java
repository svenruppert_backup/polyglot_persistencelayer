package performancetests;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 26.06.12
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */

/**
 * Registry der Indices in der Graphendatenbank der OpengeoDb
 */
public class IndexRegistry  {

    private IndexManager indexManager;
    private Map<Integer, String> indexNameMap;
    private Index<Node> locIdIndex;
    private Index<Node> typeIndex;


    /**
     * Erzeigt eine Map,die denNamen des Level, und dessen
     * numerische repräsentation enthält.
     * @param graphDbService
     */
    public IndexRegistry(GraphDatabaseService graphDbService){
        if (graphDbService == null)
            throw new IllegalArgumentException("Parameter must not be null");

        indexNameMap = new HashMap<>();
        indexManager =  graphDbService.index();

        indexNameMap.put(1, "kontinent");
        indexNameMap.put(2, "land");
        indexNameMap.put(3, "bundesland");
        indexNameMap.put(4, "regierungsbezirk");
        indexNameMap.put(5, "landkreis");
        indexNameMap.put(6, "politischegliederung");
        indexNameMap.put(7, "ortschaft");
        indexNameMap.put(8, "postleitzahlengebiet");
        indexNameMap.put(9, "ortsteil");

        locIdIndex = indexManager.forNodes("loc_Id");
        typeIndex = indexManager.forNodes("type");
    }

    /**
     * @return Gibt den Index auf Level/Knotentypen zurück.
     */
    public Index<Node> getTypeIndex(){
        return typeIndex;
    }

    /**
     * @return Gibt den Index auf die loc_ids zurück.
     */
    public Index<Node> getLocIdIndex(){
        return locIdIndex;
    }


    /**
     * Gibt den Namen eines level zurück.
     * @param lvl Level dessen Name angefragt wird.
     * @return Name des angfragten Level.
     */
    public String indexName(int lvl){
        String indexName = "";
        if(indexNameMap.containsKey(lvl))
            indexName = indexNameMap.get(lvl);
        else
            throw new IllegalArgumentException("No index found for level: " + lvl);

        return indexName;
    }

}
