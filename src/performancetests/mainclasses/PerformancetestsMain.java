package performancetests.mainclasses;

import performancetests.mainclasses.execution.PerformancetestsRunner;

/**
* Created with IntelliJ IDEA.
* User: donnie
* Date: 8/31/12
* Time: 4:45 AM
* To change this template use File | Settings | File Templates.
*/

/**
 * Mainklasse um alle PerformanceTest auszuführen.
 */
public class PerformancetestsMain {

    /**
     *  Stößt die Ausführung aller Performancetest an (Einfügen, und Abfragen).
     *
     * @param args
     */
    public static void main(String[] args){
        //  create if not exists - taked a couple of hours.
        //runInsertPerformancetests();
        new PerformancetestsRunner().runPerformancetest(50);
    }
}
