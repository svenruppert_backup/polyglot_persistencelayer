package performancetests.mainclasses;

import performancetests.mainclasses.execution.GraphDbConverter;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/31/12
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Mainklasse um die Graphendatenbanken für die Testklassen anzulegen.
 * Sie enthalten nur Ausschnitte der kompletten Datenbank,
 * um einfacher die richtige Funktionen testen zu können.
 */
public class CreateTestGraphDbsMain {

    /**
     * Stößt die erzeigung der Testgraphendatenbanken an.
     * @param args
     */
    public static void main(String[] args) {
        new GraphDbConverter().convertToGraphDb(true);
    }
}
