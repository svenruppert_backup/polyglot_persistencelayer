package performancetests.mainclasses.execution;

import performancetests.inserting.MeasureExecuter;
import performancetests.inserting.PerformanceTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 06.06.12
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
public class CreateDbPerformanceTest {

    private static final boolean IMPERMANENT = false;

    private static final int MAXNODES           =     50;    //501
    private static final List<Integer> INTARRAY =
            new ArrayList<Integer>(Arrays.asList(10,50,100,500,1_000,5_000,10_000,25_000,50_000,100_000,150_000,200_000));//));

    public final static boolean FULLYCONNECTED = false;
    public final static boolean TREESHAPED = true;


    public CreateDbPerformanceTest(boolean isTreeshapedInsertingPerformancetest) {
        if (isTreeshapedInsertingPerformancetest == TREESHAPED)
            testTreeNodes();
        else
            testFullyConnectedNodes();
    }

    private void testFullyConnectedNodes() {
        final String OUTPUTFILE = "FullyConnected.txt";
        PerformanceTest perf;

        for (int i : INTARRAY) {
        perf = new PerformanceTest(i, IMPERMANENT);
        new MeasureExecuter(perf, i, OUTPUTFILE, true){

            @Override
            public void dotask() {
                getPerftest().createFullyConnectedNodes();
            }

            @Override
            public String outputInfo() {
                return "" + getAmountNodes() + "";
            }
        }.execute();
        perf.shutdown();


        perf = new PerformanceTest(i, IMPERMANENT);
        new MeasureExecuter(perf, i, OUTPUTFILE, true){

            @Override
            public void dotask() {

                getPerftest().addOneNodeFullyConnected();
            }

            @Override
            public String outputInfo() {
                return "";
            }

        }.execute();
        perf.shutdown();
        PerformanceTest.deleteDBDir();
        }
    }


    private void testTreeNodes() {
        final String OUTPUTFILE = "TreeShaped.txt";
        PerformanceTest perf;

        for (int i : INTARRAY) {
            perf = new PerformanceTest(i, IMPERMANENT);
            new MeasureExecuter(perf, i, OUTPUTFILE, true){

                @Override
                public void dotask() {
                    getPerftest().createTreeNodes(4);
                }

                @Override
                public String outputInfo() {
                    return "" + getAmountNodes() + "";
                }
            }.execute();
            perf.shutdown();

            perf = new PerformanceTest(i, IMPERMANENT);
            new MeasureExecuter(perf, i, OUTPUTFILE, true){

                @Override
                public void dotask() {

                    getPerftest().addOneNodeToTree();
                }

                @Override
                public String outputInfo() {
                    return "";
                }
            }.execute();
            perf.shutdown();

            PerformanceTest.deleteDBDir();
        }
    }



//    public void testSearchNode() {
//        final String OUTPUTFILE = "Search.txt";
//        PerformanceTest perf;
//
//        perf = new PerformanceTest(MAXNODES, IMPERMANENT);
//        perf.createTreeNodes(4);
//        perf.addOneNodeToTree();
//        //perf.createFullyConnectedNodes(MAXNODES);
//        new MeasureExecuter(perf, MAXNODES, OUTPUTFILE, true){
//
//            @Override
//            public void dotask() {
//                Node basenode = getPerftest().getGraphDB().getNodeById(5);
//                Traverser trav = getPerftest().searchAllNodesFrom(basenode);
//                for (Node node: trav.nodes())  {
//                    if (node.getId() != 1) {
//                        System.out.printf(node.getProperty("Name").toString() + "\n");
//                        if (basenode.getProperty("Name").toString() == node.getProperty("Name").toString())
//                            System.out.printf(basenode.toString() + ", " + node.toString() + "\n");
//                    }
//
//                }
//            }
//
//            @Override
//            public String outputInfo() {
//                return "Tree " + getAmountNodes() + " +1";
//            }
//
//        }.execute();
//        perf.shutdown();
//    }
}

