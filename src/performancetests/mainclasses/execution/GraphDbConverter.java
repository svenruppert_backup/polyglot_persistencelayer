package performancetests.mainclasses.execution;

import connectionfactory.rdbmns.JdbcConnectionFactory;
import performancetests.GraphServiceFactory;
import performancetests.converter.impl.ToFullyConnectedConverter;
import performancetests.converter.impl.ToRdbmsToTreeShaped;
import performancetests.converter.impl.attributepersistingsstrategie.NodeAttributesNodeBuilder;
import performancetests.converter.impl.attributepersistingsstrategie.PropertyNodesNodeBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/31/12
 * Time: 1:47 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Steuerklasse für die erstellung von Graphendatenbanken, für entweder komplette Datenbanken oder
 * Datenbanken für Testzwecke die nur Teilmengen der Daten enthalten.
 */
public class GraphDbConverter {

    /**
     * Stößt die erstellung aller Graphendatenbanken an.
     * @param createTestDb Legt fest ob Produktiv- oder Testdatenbanken erzeugt
     *                     werden sollen.
     */
    public void convertToGraphDb(boolean createTestDb){
        GraphServiceFactory.setInTestMode(createTestDb);
        JdbcConnectionFactory.setDbType(createTestDb);

        final NodeAttributesNodeBuilder nodeAttributesNodeBuilder = new NodeAttributesNodeBuilder();
        final PropertyNodesNodeBuilder propertyNodesNodeBuilder = new PropertyNodesNodeBuilder();

        new ToFullyConnectedConverter(nodeAttributesNodeBuilder).execute();
        new ToFullyConnectedConverter(propertyNodesNodeBuilder).execute();
        new ToRdbmsToTreeShaped(nodeAttributesNodeBuilder).execute();
        new ToRdbmsToTreeShaped(propertyNodesNodeBuilder).execute();
    }

}
