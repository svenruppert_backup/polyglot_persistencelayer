package performancetests.mainclasses.execution;

import performancetests.querying.AbstractPerformancetestQuery;
import performancetests.querying.LatexFileFormatter;
import performancetests.querying.PerformanceTestFileFormatter;
import performancetests.querying.PerformanceTestStopWatch;

import java.util.List;

/**
* Created with IntelliJ IDEA.
* User: donnie
* Date: 8/31/12
* Time: 4:45 AM
* To change this template use File | Settings | File Templates.
*/

/**
 * Führt Performancetests aus.
 */
public class PerformancetestsRunner {

    /**
     * Stößt alle Abfrage Performancetest an. 50 Durchläufe, und es werden sowohl
     * eine Datei pro Abfrage, als auch eine Tab-Separierte Datei für die Verwendung
     * in Latex Dokumenten erzeugt.
     */
    public void runPerformancetest(int amountOfTest){

        final PerformanceQueryRegistry perormanceQueryRegistry = new PerformanceQueryRegistry();
        List<AbstractPerformancetestQuery> instancesList = perormanceQueryRegistry.getInstancList();
        new PerformanceTestStopWatch().execute(instancesList, amountOfTest);
        new PerformanceTestFileFormatter(instancesList).writeResultsToFile();
        new LatexFileFormatter(instancesList).writeResultsToFile();

        new CreateDbPerformanceTest(true);
        new CreateDbPerformanceTest(false);
    }

}
