package performancetests.mainclasses.execution;

import performancetests.querying.AbstractPerformancetestQuery;
import performancetests.querying.hibernate.HibernatePerformanceTest;
import performancetests.querying.neo4j.IResultBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.NodeAttributesResultBuilder;
import performancetests.querying.neo4j.buildresultListStrategie.PropertyNodesResultBuilder;
import performancetests.querying.neo4j.fullyconnectedgraph.BasicFullyConnectedQuery;
import performancetests.querying.neo4j.fullyconnectedgraph.CypherFullyConnectedQuery;
import performancetests.querying.neo4j.fullyconnectedgraph.TraversalFullyConnectedQuery;
import performancetests.querying.neo4j.treeshapedgraph.BasicTreeShapedQuery;
import performancetests.querying.neo4j.treeshapedgraph.CypherTreeShapedQuery;
import performancetests.querying.neo4j.treeshapedgraph.TraversalTreeShapedQuery;
import performancetests.querying.sql.SqlPerformancetestQuery;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PerformanceQueryRegistry {


    /**
     * Gibt eine Liste mit Instanzen aller PerformancetestAbfragen zurück.
     * Hierzu werden für die Grapgendatenbank alle Permutationen von
     * Abfragen und Datenspeicherungsstrategien erstellt, und
     * der Liste hinzugefügt.
     * Die SQL und HSQL Instanzen werden einzeln erzeigt und hinzugefügt.
     *
     * @return Gibt eine Liste mit Instanzen aller PerformancetestAbfragen zurück.
     */
    public List<AbstractPerformancetestQuery> getInstancList() {

        List<Class<? extends AbstractPerformancetestQuery>> performanceTestQueriesList = getPerformancetestQueryList();
        List<Class<? extends IResultBuilder>> resultBuilderList = getResultBuilderList();

        final List<AbstractPerformancetestQuery> instancesList = new LinkedList<>();
        try {

            for (Class graphShape : performanceTestQueriesList)
                for (Class valueStrategie : resultBuilderList) {
                    Constructor graphShapeConstructor = graphShape.getDeclaredConstructor(IResultBuilder.class);
                    Object valueStrategieInstance = valueStrategie.newInstance();
                    Object graphShapeInstance = graphShapeConstructor.newInstance(valueStrategieInstance);
                    instancesList.add((AbstractPerformancetestQuery) graphShapeInstance);
                }

        } catch (IllegalAccessException| NoSuchMethodException| InvocationTargetException| InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        instancesList.add(new SqlPerformancetestQuery());
        instancesList.add(new HibernatePerformanceTest());
        return instancesList;
    }

    /**
     * Gibt eine Liste aller hinterlegten Performancetestabfragen für die Grapgendatenbank zurück.
     *
     * @return Eine Liste von Klassen.
     */
    private List<Class<? extends AbstractPerformancetestQuery>> getPerformancetestQueryList() {
        final List<Class<? extends AbstractPerformancetestQuery>> performanceTestQueriesList
                = new ArrayList<>();
        performanceTestQueriesList.add(BasicFullyConnectedQuery.class);
        performanceTestQueriesList.add(CypherFullyConnectedQuery.class);
        performanceTestQueriesList.add(TraversalFullyConnectedQuery.class);
        performanceTestQueriesList.add(BasicTreeShapedQuery.class);
        performanceTestQueriesList.add(CypherTreeShapedQuery.class);
        performanceTestQueriesList.add(TraversalTreeShapedQuery.class);
        return performanceTestQueriesList;
    }

    /**
     * Gibt eine Liste aller hinterlegten Datenspeicherungsstrategien für die Grapgendatenbank zurück.
     *
     * @return Eine Liste von Klassen.
     */
    private List<Class<? extends IResultBuilder>> getResultBuilderList() {
        final List<Class<? extends IResultBuilder>> resultBuilderList = new ArrayList<>();
        resultBuilderList.add(PropertyNodesResultBuilder.class);
        resultBuilderList.add(NodeAttributesResultBuilder.class);
        return resultBuilderList;
    }
}