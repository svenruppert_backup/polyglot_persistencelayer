package performancetests.mainclasses;

import performancetests.mainclasses.execution.GraphDbConverter;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/31/12
 * Time: 1:45 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Mainklasse um die kompletten Graphendatenbanken für die PerformanceTests anzulegen.
 */
public class CreateCompleteGraphDbsMain {

    /**
     * Stößt die erzegung von Produktivgraphedatenbanken an.
     * @param args
     */
    public static void main(String[] args) {
        new GraphDbConverter().convertToGraphDb(false);
    }
}
