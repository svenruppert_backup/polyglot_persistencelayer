package performancetests;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.IndexManager;
import performancetests.converter.AbstractRdbmsToGraphConverter;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.querying.neo4j.AbstractGraphDbPerformancetestQuery;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 20.06.12
 * Time: 15:03
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer für Transaktionen auf der Graphendatenbank
 */
public abstract class GraphTxExecuter {

    protected final GraphDatabaseService graph;
    protected final IndexManager indexManager;
    protected final RelationsRegistry relationsRegistry;
    protected IndexRegistry indexRegistry;

    /**
     * Konstruktor,
     * Instanziiert und initialisiert alle Objekte, die
     * benötigt werden um eine Transaktion auf eine
     * Graphendatenbank durchzuführen
     *
     * @param dbName Path as String to the GraphDb,.
     */
    protected GraphTxExecuter(String dbName) {
        if (dbName == null || dbName == "")
            throw new NullPointerException("dbName must not be null or empty");
        graph = GraphServiceFactory.getGraphService(dbName);
        indexManager = graph.index();
        indexRegistry = new IndexRegistry(graph);
        relationsRegistry = new RelationsRegistry();
    }

    /**
     * Kapselung der Transaktion
     */
    public void execute(){
        Transaction tx = graph.beginTx();
        try{
            doTransaction();
            tx.success();
        }
        finally{
            tx.finish();
        }
    }

    /**
     * Implementierung der Aktion(en) innerhalb der Transaktion.
     */
    protected abstract void doTransaction();
}


