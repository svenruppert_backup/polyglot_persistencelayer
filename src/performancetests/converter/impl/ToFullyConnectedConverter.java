package performancetests.converter.impl;

import org.neo4j.graphdb.Node;
import performancetests.converter.AbstractRdbmsToGraphConverter;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.converter.impl.jdbcexecuter.GetLevelIdsJdbcExecuter;
import performancetests.converter.impl.jdbcexecuter.GetParentLevelIdsJdbcExecuter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/26/12
 * Time: 4:02 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Convertiert die OpengeoDb in einen vollvermaschten Graphen
 */
public class ToFullyConnectedConverter extends AbstractRdbmsToGraphConverter {

    /**
     * Konstruktor
     * @param attributePersistingStrategie  Strategie nach der die Daten persistiert werden sollen
     */
    public ToFullyConnectedConverter(IAttributePersistingStrategie attributePersistingStrategie) {
        super(ToFullyConnectedConverter.class, attributePersistingStrategie.getClass());
    }

    /**
     * Konstruktor, der den Namen der Datenbank und eine Instanz der Datenspeicherungsstrategoe
     * als Parameter übernimmt.
     *
     * @param dbName Der Pfad zur Graphendatenbank in Stringform.
     * @param valueStrategie Instanz der Datenspeicherungsstrategie.
     */
    public ToFullyConnectedConverter(String dbName, IAttributePersistingStrategie valueStrategie){
        super(dbName, valueStrategie);
    }

    /**
     * Erzeugt eine Graphendatenbank mit vollvermaschten Zweigen, die
     * alle Einträge der Tabelle opengeodb_herarchies enthält.
     * Der Graph wird von unten nach oben aufgebaut.
     *
     * @param level zu convertierendes Level
     */
    @Override
    protected void convert(int level) {
        int parentLevel = level - 1;
        System.out.println("converting lvl ->" + level );

        final List<Long> levelIds = new GetLevelIdsJdbcExecuter(level).execute();
        for (Long levelLocId : levelIds) {
            final Node levelNode = getNodeByIdOrCreate(levelLocId, level);

            if(level == CITY_LEVEL)
                attributePersistingStrategie.persistAttributes(levelLocId, levelNode, graph);

            for (int parentLevelCounter = parentLevel; parentLevelCounter > 0; parentLevelCounter--) {
                final List<Long> parentLevelIds =
                        new GetParentLevelIdsJdbcExecuter(level, levelLocId, parentLevelCounter).execute();

                if (parentLevelIds != null && parentLevelIds.size() > 0) {
                    final long parendLvlId = parentLevelIds.get(0);
                    final Node parentNode = getNodeByIdOrCreate(parendLvlId, parentLevelCounter);
                    parentNode.createRelationshipTo(levelNode, relationsRegistry.getRelation(level));
                }
            }
        }
    }
}
