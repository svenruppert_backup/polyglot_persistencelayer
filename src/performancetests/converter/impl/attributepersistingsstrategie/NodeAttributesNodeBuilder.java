package performancetests.converter.impl.attributepersistingsstrategie;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.converter.impl.jdbcexecuter.GetGermanStandardCityNameAndValidUntilExecuter;
import performancetests.converter.impl.jdbcexecuter.GetZipCodeAndValidUntilJdbcExecuter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/27/12
 * Time: 11:36 AM
 * To change this template use File | Settings | File Templates.
 */

public class NodeAttributesNodeBuilder implements IAttributePersistingStrategie {


    /**
     * Speichert die zur erzeugung der Testabfrage notwendigen Daten als
     * Schlüssel Werte Paare in der Übergebenen Node.
     * Hierbei wird beireits beim speichern die gleichheit des valid_until
     * Wertes ausgewertet, und nur für die Testabfrage gültige Datentuppel
     * werden gespeichert.
     *
     * @param lvlId loc_id der node
     * @param node node, in der die Daten gespeichert werden sollen
     * @param graphDbService Instanz der Graphendatenbank.
     */
    @Override
    public void persistAttributes(long lvlId, Node node, GraphDatabaseService graphDbService) {
        if (lvlId < 0)
            throw new IllegalArgumentException("LevelID must be positiv.");
        if (node == null)
            throw new NullPointerException("Given node is null.");
        if (graphDbService == null)
            throw new NullPointerException("DatabaseService is null.");

        final List<String[]> names = new GetGermanStandardCityNameAndValidUntilExecuter(lvlId).execute();
        final List<String[]> zipCodes = new GetZipCodeAndValidUntilJdbcExecuter(lvlId).execute();
        final Map<String, StringBuilder> nodeProperties = new HashMap<>();

        if(names.size() > 0 && zipCodes.size() > 0) {
            for(String[] zipCode : zipCodes){
                final String zipValue = zipCode[0];
                final String zipValidUntil = zipCode[1];
                for(String[] name : names){
                    final String nameValue = name[0];
                    final String nameValidUntil = name[1];

                    if(zipValidUntil.equals(nameValidUntil)){
                        if(!nodeProperties.containsKey(zipValue))
                            nodeProperties.put(zipValue, new StringBuilder());

                        nodeProperties.get(zipValue)
                                .append(nameValue)
                                .append("<NEXT>");
                    }
                }
            }

            for(String nodePropertyKey : nodeProperties.keySet()){
                StringBuilder nodePropertyValueStringBuilder = nodeProperties.get(nodePropertyKey);
                int positionOfLastSeparator = nodePropertyValueStringBuilder.lastIndexOf("<NEXT>");
                int propertyValueStringLength = nodePropertyValueStringBuilder.length();
                nodePropertyValueStringBuilder.delete(positionOfLastSeparator, propertyValueStringLength);
                String nodeValue = nodePropertyValueStringBuilder.toString();
                if(nodeValue != null && nodePropertyKey != null) {
                    node.setProperty(nodePropertyKey, nodeValue);
                }
            }
        }
    }
}
