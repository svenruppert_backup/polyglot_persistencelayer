package performancetests.converter.impl.attributepersistingsstrategie;

import org.infinispan.configuration.cache.LegacyConfigurationAdaptor;
import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.converter.impl.jdbcexecuter.GetGermanStandardCityNameAndValidUntilExecuter;
import performancetests.converter.impl.jdbcexecuter.GetZipCodeAndValidUntilJdbcExecuter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/27/12
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class PropertyNodesNodeBuilder implements IAttributePersistingStrategie {


    /**
     * Stößt das speichern aller für die Testabfrage notwenidgen Daten in eigenen
     * Propertynodes an.
     * @param lvlId
     * @param node
     * @param graphDbService
     */
    @Override
    public void persistAttributes(long lvlId, Node node, GraphDatabaseService graphDbService) {
        if (lvlId < 0)
            throw new IllegalArgumentException("LevelID must be positiv.");
        if (node == null)
            throw new NullPointerException("Given node is null.");
        if (graphDbService == null)
            throw new NullPointerException("DatabaseService is null.");

        addNameNodes(lvlId, node, graphDbService);
        addZipCodeNodes(lvlId, node, graphDbService);
    }

    /**
     * Speichert alle für lvlId im RDBMS gespeicherten Namen in eigenen Nodes,
     * und speichert den valid_until Wert in der Relation
     *
     * @param lvlId RDBMS Id der Node (loc_id)
     * @param levelNode Elternknoten des Propertynode.
     * @param graph Instanz der Graphendatenbank.
     */
    protected void addNameNodes(Long lvlId, Node levelNode, GraphDatabaseService graph) {
        final List<String[]> names = new GetGermanStandardCityNameAndValidUntilExecuter(lvlId).execute();
        for(String[] name : names){
            final Node nameNode = graph.createNode();
            nameNode.setProperty("value", name[0]);
            final DynamicRelationshipType nameRelationship = DynamicRelationshipType.withName("NAME");
            final Relationship relationship = levelNode.createRelationshipTo(nameNode, nameRelationship);
            relationship.setProperty("valid_until", name[1]);
        }
    }

    /**
     * Speichert alle für lvlId im RDBMS gespeicherten Postleitzahlen in eigenen Nodes,
     * und speichert den valid_until Wert in der Relation
     *
     * @param lvlId RDBMS Id der Node (loc_id)
     * @param levelNode Elternknoten des Propertynode.
     * @param graph Instanz der Graphendatenbank.
     */
    protected void addZipCodeNodes(Long lvlId, Node levelNode, GraphDatabaseService graph) {
        final List<String[]> zipCodes = new GetZipCodeAndValidUntilJdbcExecuter(lvlId).execute();
        for(String[] zipCode: zipCodes){
            final Node zipCodeNode = graph.createNode();
            zipCodeNode.setProperty("value", zipCode[0]);
            final Relationship relationship = levelNode.
                    createRelationshipTo(zipCodeNode, DynamicRelationshipType.withName("ZIP_CODE"));
            relationship.setProperty("valid_until", zipCode[1]);
        }
    }
}
