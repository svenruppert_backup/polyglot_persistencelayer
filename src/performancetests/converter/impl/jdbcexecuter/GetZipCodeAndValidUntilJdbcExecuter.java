package performancetests.converter.impl.jdbcexecuter;

import performancetests.JdbcQueryExecuter;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/24/12
 * Time: 5:35 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt die gültigen Postleitzahlen der gegebenen Id aus der Relationalen Datenbank
 */
public class GetZipCodeAndValidUntilJdbcExecuter extends JdbcQueryExecuter<String[]> {
    private final long id;

    /**
     * Konstruktor
     * @param id Id des Datensatzes
     */
    public GetZipCodeAndValidUntilJdbcExecuter(long id) {
        this.id = id;
    }

    /**
     * Erzeugt die Rückgabeliste der Testabfrage als List<T>. Prüft
     * resultSet auf null.
     *
     * @throws SQLException
     */
    @Override
    protected void createResultList() throws SQLException {
        if(resultSet != null){
            String[] result = new String[2];
            result[0] = resultSet.getString("text_val");
            result[1] = resultSet.getString("valid_until");
            resultList.add(result);
        }
    }

    /**
     * Gibr die auszuführende SQL Abfrage als String zurück.
     * @return SQL Abfrage String.
     */
    @Override
    protected String returnQueryString() {
        return String.format("SELECT text_val, valid_until FROM geodb_textdata WHERE loc_id=%s AND text_type=500300000;", id);
    }
}
