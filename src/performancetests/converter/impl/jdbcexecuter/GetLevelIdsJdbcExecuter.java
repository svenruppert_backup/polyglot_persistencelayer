package performancetests.converter.impl.jdbcexecuter;

import performancetests.JdbcQueryExecuter;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/24/12
 * Time: 5:33 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt alle Ids des gegebenen Levels aus der Relationalen Datenbank
 */
public class GetLevelIdsJdbcExecuter extends JdbcQueryExecuter<Long> {
    private final int level;

    /**
     * Konstruktor
     * @param level Levelnummer
     */
    public GetLevelIdsJdbcExecuter(int level) {
        this.level = level;
    }

    /**
     * Erzeugt die Rückgabeliste der Testabfrage als List<T>. Prüft
     * resultSet auf null.
     *
     * @throws SQLException
     */
    @Override
    protected void createResultList() throws SQLException {
        if(resultSet != null) {
            long loc_id = resultSet.getLong(1);
            resultList.add(loc_id);
        }
    }

    /**
     * Gibr die auszuführende SQL Abfrage als String zurück.
     * @return SQL Abfrage String.
     */
    @Override
    protected String returnQueryString() {
        return String.format(   "SELECT loc_id " +
                                "FROM geodb_hierarchies " +
                                "WHERE level=%s;", level);
    }
}
