package performancetests.converter.impl.jdbcexecuter;

import performancetests.JdbcQueryExecuter;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/24/12
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt alle Id aus dem höheren Level aus der Relationalen Datenbank
 */
public class GetParentLevelIdsJdbcExecuter extends JdbcQueryExecuter<Long> {
    private final int level;
    private final long levelId;
    private final int parentLevel;

    /**
     * Konstruktor
     * @param level Levelnummer aktuelles Level
     * @param levelId Id des Datensatzes
     * @param parentLevel Levelnummer gewünschtes Level
     */
    public GetParentLevelIdsJdbcExecuter(int level, long levelId, int parentLevel) {
        this.level = level;
        this.levelId = levelId;
        this.parentLevel = parentLevel;
    }

    /**
     * Erzeugt die Rückgabeliste der Testabfrage als List<T>. Prüft
     * resultSet auf null.
     *
     * @throws SQLException
     */
    @Override
    protected void createResultList() throws SQLException {
        if(resultSet != null){
            long loc_id = resultSet.getLong(1);
            resultList.add(loc_id);
        }
    }

    /**
     * Gibr die auszuführende SQL Abfrage als String zurück.
     * @return SQL Abfrage String.
     */
    @Override
    protected String returnQueryString() {
        String test =   String.format(  "SELECT id_lvl%s " +
                                        " FROM geodb_hierarchies " +
                                        " WHERE loc_id=%s" +
                                        " and id_lvl%s IS NOT NULL",parentLevel, levelId, parentLevel);

        return String.format(test);
    }
}
