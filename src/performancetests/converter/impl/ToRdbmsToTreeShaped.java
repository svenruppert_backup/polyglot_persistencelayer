package performancetests.converter.impl;

import org.neo4j.graphdb.Node;
import performancetests.converter.AbstractRdbmsToGraphConverter;
import performancetests.converter.IAttributePersistingStrategie;
import performancetests.converter.impl.jdbcexecuter.GetLevelIdsJdbcExecuter;
import performancetests.converter.impl.jdbcexecuter.GetParentLevelIdsJdbcExecuter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/26/12
 * Time: 4:01 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Convertiert die OpengeoDb in einen baumförmigen Graphen
 */
public class ToRdbmsToTreeShaped extends AbstractRdbmsToGraphConverter {

    /**
     * Konstruktor
     * @param attributePersistingStrategie  Strategie nach der die Daten persistiert werden sollen
     */
    public ToRdbmsToTreeShaped(IAttributePersistingStrategie attributePersistingStrategie) {
        super(ToRdbmsToTreeShaped.class, attributePersistingStrategie.getClass());
    }

    /**
     * Konstruktor, der den Namen der Datenbank und eine Instanz der Datenspeicherungsstrategoe
     * als Parameter übernimmt.
     *
     * @param dbName Der Pfad zur Graphendatenbank in Stringform.
     * @param valueStrategie Instanz der Datenspeicherungsstrategie.
     */
    public ToRdbmsToTreeShaped(String dbName, IAttributePersistingStrategie valueStrategie){
        super(dbName, valueStrategie);
    }

    /**
     * Erzeugt eine baumförmige Graphendatenbank, die
     * alle Einträge der Tabelle opengeodb_herarchies enthält.
     * Der Graph wird von unten nach oben aufgebaut.
     *
     * @param level zu convertierendes Level
     */
    @Override
    protected void convert(int level) {
        int i = 0;
        System.out.println("converting lvl ->" + level);
        int parentLevel = level - 1;
        final List<Long> levelIds = new GetLevelIdsJdbcExecuter(level).execute();

        for (Long lvlId : levelIds) {
            final Node levelNode = getNodeByIdOrCreate(lvlId, level);

            if(level == CITY_LEVEL)
                attributePersistingStrategie.persistAttributes(lvlId, levelNode, graph);

            int parentLevelCounter = parentLevel+1;
            List<Long> parentLevelIds;
            do{
                parentLevelCounter--;
                parentLevelIds = new GetParentLevelIdsJdbcExecuter(level, lvlId, parentLevelCounter).execute();
            }while(parentLevelCounter > 0 && parentLevelIds.size()  < 1);

            if (parentLevelIds != null && parentLevelIds.size() > 0) {
                final long parendLvlId = parentLevelIds.get(0);
                final Node parentNode = getNodeByIdOrCreate(parendLvlId, parentLevelCounter);
                parentNode.createRelationshipTo(levelNode, relationsRegistry.getRelation(level));
            }
        }
    }
}
