package performancetests.converter;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 8/27/12
 * Time: 11:33 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IAttributePersistingStrategie  {

    /**
     * Speichert alle zur Erstellung der Ergebnisliste benötigten Daten in einer
     * Graphendatenbank.
     *
     * @param lvlId ID des zu speichernden Datensatzes.
     * @param node Node, zu der diese Daten gespeichert werden sollen.
     * @param graphDbService Instanz der Graphendatenbank.
     */
    public void persistAttributes(long lvlId, Node node, GraphDatabaseService graphDbService);
}
