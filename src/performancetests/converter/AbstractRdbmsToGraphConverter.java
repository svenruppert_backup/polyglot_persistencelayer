package performancetests.converter;

import org.neo4j.graphdb.Node;
import performancetests.GraphDbServiceRegistry;
import performancetests.GraphTxExecuter;
import performancetests.querying.neo4j.IResultBuilder;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/24/12
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Convertiert die OpengeoDB in einen Graphen.
 */
public abstract class AbstractRdbmsToGraphConverter extends GraphTxExecuter {
    protected final int CITY_LEVEL  = 6;
    protected IAttributePersistingStrategie attributePersistingStrategie;
    protected int level;

    /**
     * Konstruktor.
     * Nimmt Class Objekte von sich selber abgeleitete Klassen, und  zur Datenspeicherungsstrategie
     * als Parameter entgegen, und stößt auf deren Grundlage ein Mapping zu der ZielgrapheDb
     * an.
     *
     * @param graphShape Von sich selber abgeleitete Klasse.
     * @param valueStrategie Datenspeicherungsstrategoe.
     */
    protected AbstractRdbmsToGraphConverter(Class<? extends AbstractRdbmsToGraphConverter> graphShape,
                                            Class<? extends IAttributePersistingStrategie> valueStrategie) {
        super(prepare(graphShape, valueStrategie));
        try{
            IAttributePersistingStrategie instace = valueStrategie.newInstance();
            attributePersistingStrategie = instace;
        } catch (InstantiationException | IllegalAccessException e ) {
            e.printStackTrace();  //To change body of catch statement use File | Sett.ings | File Templates.
        }
    }

    /**
     * Statische Methode, die vorm Mappen die Parameter prüft, und setzt aus den Klassen
     * den Dateinamen zusammen.
     *
     * @param graphShape Abgeleitete Converter Klasse,
     * @param valueStrategie Datenspeicherungsstrategie.
     * @return Dateinamen.
     */
    private static String prepare(Class<? extends AbstractRdbmsToGraphConverter> graphShape,
                                  Class<? extends IAttributePersistingStrategie> valueStrategie) {
        if(graphShape == null)
            throw new NullPointerException("AbstractGraphDbPerformancetestQuery: Given Class Class (extending AbstractGraphDbPerformanceQuery) was null.");
        if(valueStrategie == null)
            throw new NullPointerException("AbstractGraphDbPerformancetestQuery: Given Class Class (extending IResultBuilder) was null..");
        return new GraphDbServiceRegistry().getGraphDbName(graphShape,valueStrategie);//graphShape.getSimpleName()  + valueStrategie.getSimpleName() + ".txt";
    }

    protected AbstractRdbmsToGraphConverter(String dbName, IAttributePersistingStrategie valueStrategie){
        super(dbName);
        if (valueStrategie == null) {
            throw new  NullPointerException("valueStrategie must not be null");
        }
        attributePersistingStrategie = valueStrategie;
    }


    /**
     * Stößt das convertieren aller level der opengeodb an.
     */
    @Override
    protected void doTransaction() {
        for(int i=9; i>0; i--)
            convert(i);
    }

    /**
     * Routine nach der das gegebene Level convertiert wird um es zu persitieren.
     * @param level zu convertierendes Level
     */
    protected abstract void convert(int level);

    protected Node getNodeByIdOrCreate(Long lvlId, int level) {
        Node newNode = null;
        newNode = indexRegistry.getLocIdIndex().get("loc_id", lvlId).getSingle();

        if(newNode == null){
            newNode = graph.createNode();
            newNode.setProperty("loc_id", lvlId);
            indexRegistry.getLocIdIndex().add(newNode, "loc_id", newNode.getProperty("loc_id"));
        }
        return newNode;
    }
}
