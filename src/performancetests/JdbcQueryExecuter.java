package performancetests;


import connectionfactory.rdbmns.JdbcConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 20.06.12
 * Time: 14:15
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer für ein Query über den JDBC Treiber
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public abstract class JdbcQueryExecuter<T> {

    protected Connection connection;
    protected List<T> resultList = new ArrayList<>();
    protected ResultSet resultSet;
    protected String query;

    /**
     * Routine nach der die Resultliste gebaut wird
     * @throws SQLException
     */
    protected abstract void createResultList() throws SQLException;

    /**
     * Abfrage mit der die Datenbank durchsucht werden soll.
     * @return Liste mit den ermittelten Ergebnissen
     * @throws SQLException
     */
    protected abstract String returnQueryString() throws SQLException;

    public List<T> execute() {
        JdbcConnectionFactory conFactory = new JdbcConnectionFactory();
        connection = conFactory.getJdbcConnection();
        try{
            Statement statement = connection.createStatement();
            query = returnQueryString();
            if(query == null)
                throw new NullPointerException("JdbcExecuter: returnQueryString() should not return null.");
            if(query.isEmpty())
                throw new IllegalArgumentException("JdbcExecuter: returnQueryString() should not return an empty String.");

            resultSet = statement.executeQuery(query);
            while(resultSet.next())
                createResultList();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        conFactory.closeConnection(connection);

        return resultList;
    }




}
