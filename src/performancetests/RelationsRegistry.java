package performancetests;

import org.neo4j.graphdb.DynamicRelationshipType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 26.06.12
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */

/**
 * Registry der Relationen der OpengeoDb
 */
public class RelationsRegistry {
    private final Map<Integer, DynamicRelationshipType> relationMap = new HashMap<>();

    /**
     * Konstruktor.
     * Erzeugt eine Map, die alle Relationen zu einem bestimmten
     * Level hin enthält.
     */
    public RelationsRegistry(){
        relationMap.put(1,DynamicRelationshipType.withName("KONTINENT"));
        relationMap.put(2,DynamicRelationshipType.withName("LAND"));
        relationMap.put(3,DynamicRelationshipType.withName("BUNDESLAND"));
        relationMap.put(4,DynamicRelationshipType.withName("REGIERUNGSBEZIRK"));
        relationMap.put(5,DynamicRelationshipType.withName("LANDKREIS"));
        relationMap.put(6,DynamicRelationshipType.withName("POL_GLIEDERUNG"));
        relationMap.put(7,DynamicRelationshipType.withName("ORTSCHAFT"));
        relationMap.put(8,DynamicRelationshipType.withName("POSTLEITZAHLENGEBIET"));
        relationMap.put(9,DynamicRelationshipType.withName("ORTSTEIL"));
    }

    /**
     * Gibt die Relation des entsprechenden Levels wieder.
     * @param lvl id des levels
     * @return  Relation
     */
    public DynamicRelationshipType getRelation(int lvl){
        DynamicRelationshipType relation = null;
        if(relationMap.containsKey(lvl))
            relation = relationMap.get(lvl);
        else
            throw new IllegalArgumentException("There is not index defined for level:" + lvl);

        return relation;
    }

}
