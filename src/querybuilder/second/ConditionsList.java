package querybuilder.second;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 26.07.12
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */

/**
 *  Datatype for the querybuilder.
 *  compField: contains the name of the field
 *  compValue: contains the value of the field
 *  compOp:    contains the operator (equals, less than, ...)
 *  extension: contains the condition (getAll, and, or, ...)
 */
public class ConditionsList {
    private String compField;
    private String compOp;
    private String compValue;
    private String extension;

    public String getCompField() {
        return compField;
    }

    public void setCompField(String compField) {
        this.compField = compField;
    }

    public String getCompOp() {
        return compOp;
    }

    public void setCompOp(String compOp) {
        this.compOp = compOp;
    }

    public String getCompValue() {
        return compValue;
    }

    public void setCompValue(String compValue) {
        this.compValue = compValue;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return "ConditionsList{" +
                "extension='" + extension + '\'' +
                ", compField='" + compField + '\'' +
                ", compOp='" + compOp + '\'' +
                ", compValue='" + compValue + '\'' +

                '}';
    }
}
