package querybuilder.second;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 26.07.12
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */

/**
 *  Querybuilder für SQL-Statements.
 *  Ermöglicht es punktsepariert ein Statement aufzubauen.
 *  Durch interne Klassen ist die logische Abhängigkeit gewährleistet.
 *
 */
public class Query {
    final Query thisQuery;
    StringBuilder str = new StringBuilder();
    List<ConditionsList> list = new ArrayList<>();
    ConditionsList co;


    public Query() {
        thisQuery = this;
        co = new ConditionsList();
    }

    @Override
    public String toString() {
        return str.toString();
    }

    public List<ConditionsList> getList() {
        return list;
    }

    public Clause getAll() {
        str.append("getAll ");
        co.setExtension("getAll");
        return new Clause();
    }

    public Clause getSingle() {
        str.append("getSingle ");
        co.setExtension("getSingle");
        return new Clause();
    }

    public Clause and() {
        str.append("and ");
        co = new ConditionsList();
        co.setExtension("and");
        return new Clause();
    }

    public Clause or() {
        str.append("or ");
        co = new ConditionsList();
        co.setExtension("or");
        return new Clause();
    }


    public class Clause {

        public Condition where(String field) {
            str.append("where ");
            co.setCompField(field);
            return new Condition();
        }
    }

    public class Condition {

        public Query isEqual(String value) {
            str.append("isEqual ");
            co.setCompOp("eq");
            co.setCompValue(value);
            list.add(co);
            return thisQuery;
        }

        public Query lessThan(String value) {
            str.append("lessThan ");
            co.setCompOp("lt");
            co.setCompValue(value);
            list.add(co);
            return thisQuery;
        }

        public Query greaterThan(String value) {
            str.append("greaterThan ");
            co.setCompOp("gt");
            co.setCompValue(value);
            list.add(co);
            return thisQuery;
        }
    }
}
