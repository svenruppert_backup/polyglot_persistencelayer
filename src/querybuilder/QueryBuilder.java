package querybuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 26.07.12
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
public class QueryBuilder {

    private String queriedClass;
    private List<QueryCondition> queryConditionList = new ArrayList<>();
    private QueryCondition condition;
    private QueryBuilder myInstance;

    public QueryBuilder(){
        myInstance = this;
    }

    public void print(){
        for(int i=0; i< queryConditionList.size(); i++){
            QueryCondition tmpCondition = queryConditionList.get(i);
            String sout = String.format("%d -> \t bool: %s \t| field: %s \t| comp: %s \t| value: %s",
                    i, tmpCondition.getBool(), tmpCondition.getComparisonField(), tmpCondition.getComparisonType(), tmpCondition.getComparisonValue());
            System.out.println(sout);
        }
    }

    public Clause getSingle(String clazz){
        queriedClass = clazz;
        condition = new QueryCondition();
        queryConditionList.add(condition);
        condition.setBool("-");
        return new Clause();
    }


    public class Clause{
        public Comparison where(String field){
            condition.setComparisonField(field);
            return new Comparison();
        }
    }

    public class Comparison {
        public QueryBuilder eq(String value){
            condition.setComparisonType("eq");
            condition.setComparisonValue(value);
            return myInstance;
        }

        public QueryBuilder lt(String value){
            condition.setComparisonType("lt");
            condition.setComparisonValue(value);
            return myInstance;
        }

        public QueryBuilder gt(String value){
            condition.setComparisonType("gt");
            condition.setComparisonValue(value);
            return myInstance;
        }
    }
    public Clause and(){
        condition = new QueryCondition();
        queryConditionList.add(condition);
        condition.setBool("and");
        return new Clause();
    }

    public Clause or(){
        condition = new QueryCondition();
        queryConditionList.add(condition);
        condition.setBool("or");
        return new Clause();
    }

}
