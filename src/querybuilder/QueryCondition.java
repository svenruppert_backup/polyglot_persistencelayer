package querybuilder;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 26.07.12
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class QueryCondition {

    private String comparisonField;
    private String comparisonType;
    private String comparisonValue;
    private String bool;

    public String getComparisonField() {
        return comparisonField;
    }

    public void setComparisonField(String comparisonField) {
        this.comparisonField = comparisonField;
    }

    public String getComparisonType() {
        return comparisonType;
    }

    public void setComparisonType(String comparisonType) {
        this.comparisonType = comparisonType;
    }

    public String getComparisonValue() {
        return comparisonValue;
    }

    public void setComparisonValue(String comparisonValue) {
        this.comparisonValue = comparisonValue;
    }

    public String getBool() {
        return bool;
    }

    public void setBool(String bool) {
        this.bool = bool;
    }
}
