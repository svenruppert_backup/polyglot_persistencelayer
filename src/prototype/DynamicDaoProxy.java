package prototype;

import java.lang.reflect.*;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */

/**
 * Proxy für die Erstellung von Daos der entsprechenden Datentypen.
 * @param <D>   Typ des Interfaces für die Methoden
 * @param <E>   Datentyp der unterstützt werden soll
 */
public class DynamicDaoProxy<D, E> implements InvocationHandler {
    private final DaoMethodRegistry<E> daoMethodRegistry;
    private final Class daoClass;

    /**
     * Konstruktor
     * @param daoClass      Klassenobjekt des Interfaces für die Methoden
     * @param entityClass   Klassenobjekt für den zu unterstützten Datentypen
     */
    public DynamicDaoProxy(Class daoClass, Class entityClass){
        this.daoClass = daoClass;
        daoMethodRegistry = new DaoMethodRegistry<>(entityClass);
    }

    /**
     * Erstellt eine neue Proxyinstanz
     * @return gibt die erstelle Proxyinstaz zurück
     */
    public D getDaoInstance(){
        return (D) Proxy.newProxyInstance(daoClass.getClassLoader(), new Class[]{daoClass}, this);
    }


    /**
     * Überladen Funktion aus dem InvocationHandler
     * Holt eine Instanz der Methode aus der Registry und führt sie dann aus.
     * @param proxy     Proxyinstanz die den aufruf ausgelöst hat
     * @param method    Name der Methode die aufgerufen wurde
     * @param args      Parameter die übergeben wurden
     * @return          Rückgabe der ausgeführten Methode
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return  daoMethodRegistry.getInstance(method.getName()).execute(args);
    }

}
