package prototype;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer für Interaktionen auf der Relationalen Datenbank mittels JDBC
 * @param <T> Datentyp für den die Methode ausgeführt werden soll.
 */
public abstract class AbstractJdbcExecuter<T> extends AbstractDatabaseQueryExecuter<T> {

    protected final List<T> resultList = new ArrayList<>();
    protected Connection connection;
    protected ResultSet resultSet;

    /**
     * Konstruktor
     * Setzt den Entitätentypen, um Rückgabewerte über reflection
     * instanziieren zu können.
     *
     * @param entityType
     */
    public AbstractJdbcExecuter(Class entityType) {
        super(entityType);
    }

    /**
     * Öffnen der Datenbankverbindung
     */
    @Override
    protected void getDatabaseConnection() {
        try {
            Class.forName ("com.mysql.jdbc.Driver").newInstance ();
            String userName = "root";
            String password = "";
            String url = "jdbc:mysql://localhost/IssueTracking";
            connection = DriverManager.getConnection(url, userName, password);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) { e.printStackTrace(); }
    }


    /**
     * Schließen der Datenbankverbindung
     */
    @Override
    protected void closeDatabaseConnection() {
        try {
            if(!connection.isClosed())
                connection.close();
        } catch (SQLException e) { e.printStackTrace(); }
    }


}
