package prototype.fluentdao.dynamicproxy;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/3/12
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */

import prototype.fluentdao.registry.FluentDaoMethodRegistry;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Proxy für die Erstellung von Daos der entsprechenden Datentypen.
 * @param <D>   Typ des Interfaces für die Methoden
 */
public class DynamicDaoProxy<D> implements InvocationHandler {
    private final FluentDaoMethodRegistry daoMethodRegistry;
    private final Class daoClass;

    /**
     * Konstruktor
     * @param daoClass      Klassenobjekt des Interfaces für die Methoden
     * @param entityClass   Klassenobjekt für den zu unterstützten Datentypen
     */
    public DynamicDaoProxy(Class daoClass, Class entityClass){
        this.daoClass = daoClass;
        daoMethodRegistry = new FluentDaoMethodRegistry();
    }

    /**
     * Erstellt eine neue Proxyinstanz
     * @return gibt die erstelle Proxyinstaz zurück
     */
    public D getDaoInstance(){
        return (D) Proxy.newProxyInstance(daoClass.getClassLoader(), new Class[]{daoClass}, this);
    }


    /**
     * Überladen Funktion aus dem InvocationHandler
     * Holt eine Instanz der Methode aus der Registry und führt sie dann aus.
     * @param proxy     Proxyinstanz die den aufruf ausgelöst hat
     * @param method    Name der Methode die aufgerufen wurde
     * @param args      Parameter die übergeben wurden
     * @return          Rückgabe der ausgeführten Methode
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Class daoClass = daoMethodRegistry.getClassForMethod(method);
        Object daoClassInstance = daoClass.newInstance();
        Object returnValue = method.invoke(daoClassInstance);
        return  returnValue;
    }

}