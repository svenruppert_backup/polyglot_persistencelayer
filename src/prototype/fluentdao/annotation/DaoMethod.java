package prototype.fluentdao.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/3/12
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Annotation mit deren Hilfe Methoden als DAO Methoden
 * gekennzeichnet werden sollen.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DaoMethod {
}
