package prototype.fluentdao.daos;


import prototype.fluentdao.annotation.DaoMethod;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.GetDuplicates;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.SetAsDuplicate;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/3/12
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class GraphDao {

    /**
     * Gibt eine Liste an Duplikaten zurück.
     *
     * @param issue Issue, dessen Duplikate gesucht werden.
     * @return Liste an Duplikaten.
     */
    @DaoMethod
    public List<IssueBase> getDuplicates(IssueBase issue) {
        GetDuplicates method = new GetDuplicates<IssueBase>(IssueBase.class);
        return method.execute(new Object[]{issue});
    }

    /**
     * Setzt zwei gegebene Duplikate. Für Details siehe
     * ExecuterImplementierung (SetAsDuplicate<T>.
     * Es ist hier egal, ob und welches der beiden
     * Elemente bereits in der Graphendatenbank gespeichert ist.
     *
     * @param issue Erstes issue.
     * @param issue2 Zweites issue.
     * @return
     */
    @DaoMethod
    public void setAsDuplicate(IssueBase issue, IssueBase issue2) {
        SetAsDuplicate method = new SetAsDuplicate<IssueBase>(IssueBase.class);
        method.execute(new Object[]{issue, issue2});
    }


}
