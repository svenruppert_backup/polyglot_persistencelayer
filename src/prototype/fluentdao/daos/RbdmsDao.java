package prototype.fluentdao.daos;

import prototype.fluentdao.annotation.DaoMethod;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.Create;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.Delete;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.GetAll;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.GetById;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/3/12
 * Time: 3:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class RbdmsDao {

    /**
     * Erzeugt ein Objekt im RDBMS
     * @param issue Objekt, das angelegt werden soll.
     * @return Boolscher Wert, der angibt ob die Aktion
     * erfolgreich durchgeführt wurde,
     */
    @DaoMethod
    public boolean create(IssueBase issue) {
        boolean retVal = true;
        Create method = new Create<IssueBase>(IssueBase.class);
        if (method.execute(new Object[] {issue}) == null)
            retVal = false;
        return retVal;
    }

    /**
     * Löscht ein Objekt aus dem RDBMS
     *
     * @param issue
     * @return Boolscher Wert, der angibt ob die Aktion
     * erfolgreich durchgeführt wurde,
     */
    @DaoMethod
    public boolean delete(IssueBase issue) {
        boolean retVal = true;
        Delete method = new Delete<IssueBase>(IssueBase.class);
        if (method.execute(new Object[] {issue}) == null)
            retVal = false;
        return retVal;
    }

    /**
     * Lädt alle IssueBase Objekte.
     * @return Liste aller gespeicherten IssueBase Objekte
     */
    @DaoMethod
    public List<IssueBase> getAll() {
        GetAll method = new GetAll<IssueBase>(IssueBase.class);
        return method.execute(null);
    }

    /**
     * Lädt ein Issue anhand seiner ID.
     * @param id Id des issues, das geladen werden soll
     * @return Einzelnes IssueBase Objekt.
     */
    @DaoMethod
    public IssueBase getById(long id) {
        GetById method = new GetById<IssueBase>(IssueBase.class);
        return (IssueBase) method.execute(new Object[] {id}).get(0);
    }
}
