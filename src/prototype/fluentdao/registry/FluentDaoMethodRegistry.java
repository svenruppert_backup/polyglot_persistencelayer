package prototype.fluentdao.registry;


import prototype.fluentdao.annotation.DaoMethod;
import prototype.fluentdao.daos.GraphDao;
import prototype.fluentdao.daos.RbdmsDao;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: donnie
 * Date: 9/3/12
 * Time: 3:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class FluentDaoMethodRegistry {

    private Map<Method, Class> daoMethodMap = new HashMap<>();

    /**
     * Konstruktor.
     * Fügt die beiden Prototypdaos der Map hinzu.
     */
    public FluentDaoMethodRegistry(){
        addDaoClass(GraphDao.class);
        addDaoClass(RbdmsDao.class);
    }

    /**
     * Fügt alle als DaoMethod annotierten Klassen
     * der Map hinzu.
     *
     * @param daoClass
     */
    public void addDaoClass(Class daoClass){
        Method[] classMethodsArray = daoClass.getDeclaredMethods();
        for(Method method : classMethodsArray)
            if(method.isAnnotationPresent(DaoMethod.class))
                daoMethodMap.put(method, daoClass);
    }


            /**
            * Gibt zu einer gegtebenen MEthode eine DAO Klasse zurück,
            * die diese Methode enthält.
            * @param method Methode, zu der eine passende Klasse
             *              gefunden werden soll..
            */
    public Class getClassForMethod(Method method){
        Class daoClass = null;
        if(method != null && daoMethodMap.containsKey(method)){
            daoClass = daoMethodMap.get(method);
        }
        else {
            String parameterList = method.getParameterTypes().toString();
            String methodName = method.getName();
            String returnType = method.getReturnType().toString();
            String message  = String.format("FLuenDaoMethodRegistry: No Mapping found for Method with siganture: %s %s(%s)"
                    , returnType, methodName, parameterList);
            throw new IllegalArgumentException(message);
        }

        return daoClass;
    }

}
