package prototype;

import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */

/**
 * Registry für dieVerwaltung der ausführbaren Methoden
 *
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public class DaoMethodRegistry<T> {

    private Map<String, Class> daoMethodRegistry = new HashMap<>();
    private Class entityClass;

    /**
     * Konstruktor
     * Initialisiert die Registry mit den wichtigsten Methoden.
     *
     * @param entitiyClass Klassenobjekt von T
     */
    public DaoMethodRegistry(Class entitiyClass){
        this.entityClass = entitiyClass;
        addDaoMethodImplementation(GetAll.class);
        addDaoMethodImplementation(GetById.class);
        addDaoMethodImplementation(Create.class);
        addDaoMethodImplementation(Delete.class);
        addDaoMethodImplementation(SetAsDuplicate.class);
        addDaoMethodImplementation(GetDuplicates.class);

    }

    /**
     * Liefert eine Instanz der Methode die ausgeführt werden soll.
     *
     * @param methodName Name der Methode die ausgeführt werden soll.
     * @return  Instanz der auszuführenden Methode
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public AbstractDatabaseQueryExecuter getInstance(String methodName) throws IllegalAccessException, InstantiationException {
        AbstractDatabaseQueryExecuter<T> concreteImplementation = null;
        if(daoMethodRegistry.containsKey(methodName.toLowerCase())){
            try {
                Class daoMethodClass = daoMethodRegistry.get(methodName.toLowerCase());
                Constructor daoConstructor = daoMethodClass.getDeclaredConstructor(Class.class);
                concreteImplementation = (AbstractDatabaseQueryExecuter) daoConstructor.newInstance(entityClass);
            } catch (NoSuchMethodException | InvocationTargetException e) { e.printStackTrace(); }
        }
        return concreteImplementation;
    }


    /**
     * Registriert eine neue Implementierung in der Registry
     * @param implClass Klasse der Implementierung
     */
    public void addDaoMethodImplementation(Class implClass){
        String methodName = implClass.getSimpleName().toLowerCase();
        daoMethodRegistry.put(methodName, implClass);
    }

}
