package prototype;

import org.neo4j.graphdb.DynamicRelationshipType;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Relationship;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/22/12
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Factory für Relationstypen
 */
public class RelationShipFactory {

    /**
     * Erstellt eine Reltion vom Typen Duplicate
     *
     * @param entityType Klassenobjekt des Datentypes
     * @return Gibt eine Duplicate relation zurück
     */
    public DynamicRelationshipType getDuplicateRelationShipForType(Class entityType){
        String relationShipName = String.format("%s_DUPLICATE", entityType.getSimpleName());
        DynamicRelationshipType duplicateRelation = DynamicRelationshipType.withName(relationShipName);
        return duplicateRelation;
    }
}
