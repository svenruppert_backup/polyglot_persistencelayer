package prototype;

import performancetests.GraphServiceFactory;
import org.neo4j.graphdb.GraphDatabaseService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer für Interaktionen mit der Graphendatenbank
 * @param <T> Datentyp für den die Methode ausgeführt werden soll.
 */
public abstract class AbstractGraphExecuter<T> extends AbstractDatabaseQueryExecuter<T> {
    protected final GraphDatabaseService graphDbService = GraphServiceFactory.getGraphService(GraphServiceFactory.ISSUE_TRACKING);
    protected final GraphDbUtillityMethods graphDbUtilities = new GraphDbUtillityMethods();

    public AbstractGraphExecuter( Class entityType){
        super(entityType);
    }

    /**
     * Öffnen der Datenbankverbindung
     */
    @Override
    protected void getDatabaseConnection() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Schließen der Datenbankverbindung
     */
    @Override
    protected void closeDatabaseConnection() {
        GraphServiceFactory.shutDownDatabase(GraphServiceFactory.ISSUE_TRACKING);
    }

    /**
     * Durchführen der Testabfrage.
     * @param args
     * @return
     */
    @Override
    protected abstract List<T> executeQuery(Object[] args);

}
