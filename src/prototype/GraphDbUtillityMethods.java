package prototype;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.index.Index;

import javax.management.ListenerNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/22/12
 * Time: 2:40 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Reine Utility-Klasse zur leichteren Handhabung der GraphenDB
 */
public class GraphDbUtillityMethods {

    /**
     * Löscht einen Knoten sicher aus der Datenbank
     * @param node Zu löschender Knoten
     */
    public void safeDeleteNode(Node node){
        Set<RelationshipType> relationshipTypeSet = getallRelationshipTypes(node);

        for(RelationshipType relationshipType : relationshipTypeSet){
            if(isRoodNode(node, relationshipType))
                reconnectRootNodesChildNodes(node, relationshipType);
            else
                reconnectChildreanToParent(node, relationshipType);

            node.delete();
        }
    }

    /**
     * Holt den Elternknoten
     * @param childNode     Knoten zu dem der Elternknoten gesucht werden soll
     * @param relationship  Relation nach der gesucht werden soll.
     * @return gibt den Elternknoten zurück
     */
    public Node getParentNode(Node childNode, RelationshipType relationship){
        Node parentNode = null;

        if(childNode.hasRelationship(relationship, Direction.INCOMING))
            parentNode = childNode.getSingleRelationship(relationship, Direction.INCOMING).getStartNode();

        return parentNode;
    }

    /**
     * Verbinde eine Knoten wieder zu Rootknoten des Baumes
     * @param rootNode          Knoten zu verbunden wird
     * @param relationshipType  Relationstyp mit dem verbunden wird
     */
    public void reconnectRootNodesChildNodes(Node rootNode, RelationshipType relationshipType){
        Node newRootNode = null;

        for(Relationship relationship : rootNode.getRelationships(relationshipType, Direction.OUTGOING)){
            if(newRootNode == null)
                newRootNode  = relationship.getEndNode();
            else
                newRootNode.createRelationshipTo(relationship.getEndNode(), relationshipType);

            relationship.delete();
        }
    }

    /**
     * Verbindet Kindknoten zu den Elterknoten
     * @param node
     * @param relationshipType
     */
    public void reconnectChildreanToParent(Node node, RelationshipType relationshipType){
        Node parentNode = getParentNode(node, relationshipType);

        for(Relationship relationship : node.getRelationships(relationshipType, Direction.OUTGOING)){
            parentNode.createRelationshipTo(relationship.getEndNode(), relationshipType);
            relationship.delete();
        }

        node.getSingleRelationship(relationshipType, Direction.INCOMING).delete();
    }

    /**
     * Hole alle Relationstypen eines Knotens aus der Datenbank
     * @param node Knoten zu dem die Relationstypen erfasst werden
     * @return Gibt alle Relationstypen zurück
     */
    public Set<RelationshipType> getallRelationshipTypes(Node node){
        Set<RelationshipType> relationshipTypeList = new HashSet<>();

        for(Relationship relationship : node.getRelationships())
            relationshipTypeList.add(relationship.getType());

        return relationshipTypeList;
    }

    /**
     * Prüft ob der Knoten ein Rootknoten darstellt
     * @param node      zu prüfender Knoten
     * @param relationshipType zu prüfender Relationstyp
     * @return true, wenn Knoten eine RootKnoten ist, ansonsten false
     */
    public boolean isRoodNode(Node node, RelationshipType relationshipType){
        boolean isRootNode = false;

        if(node.hasRelationship(relationshipType, Direction.INCOMING))
            isRootNode = false;
        else
            isRootNode = true;

        return isRootNode;
    }

    /**
     * Gibt für einen Baumartigen Graphen den Rootknoten des Baumes zurück.
     * ACHTUNG: Es wird nicht auf eine Baumform geprüft!
     *
     * @param treeNode Knoten dessen Rootknoten gesuch woird.
     * @param relationshipType Relation, die den Baum Afuspannt.
     * @return gibt den Rootknoten des Baumes zurück.
     */
    public Node getRootNode(Node treeNode, RelationshipType relationshipType){
        Node parentNode = treeNode;

        if(treeNode.hasRelationship(relationshipType,Direction.INCOMING))
            while (parentNode.hasRelationship(relationshipType, Direction.INCOMING))
                parentNode = parentNode.getSingleRelationship(relationshipType, Direction.INCOMING).getStartNode();
        else if(treeNode.hasRelationship(relationshipType, Direction.OUTGOING))
            parentNode = treeNode;
        else
            parentNode = null;

        return parentNode;
    }

    /**
     * Lädt einen Knoten anhand seiner Id, oder erzeugt diesen, falls nicht vorhanden,
     * setzt die ID und speichert sie im Index,
     *
     * @param className Klassenname der Entität.
     * @param id Id der Entität.
     * @param graphDbService Instanz einer Graphendatenbank.
     * @return Gibt eine Knoten mit der id Id zurück.
     */
    public Node getByIndexOrCreate(String className, Object id, GraphDatabaseService graphDbService){
        Index<Node> indexManager = graphDbService.index().forNodes(className);
        Node requestedNode;
        requestedNode = indexManager.get(className, id).getSingle();
        if(requestedNode == null){
            requestedNode = graphDbService.createNode();
            requestedNode.setProperty("id", id);
            indexManager.add(requestedNode, className, requestedNode.getProperty("id"));
        }
        return requestedNode;
    }

}
