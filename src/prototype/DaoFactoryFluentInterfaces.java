package prototype;

import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssueBaseFluentInterface;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 21.08.12
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */

/**
 * Factory für Dao-Objekte
 */
public class DaoFactoryFluentInterfaces {

    /**
     * Liefert eine Instanz für eine IssueBaseDao
     * @return IssueBaseDao Instanz
     */
    public static IssueBaseFluentInterface getIssueBaseDao() {
        return (IssueBaseFluentInterface) new DynamicDaoProxy<IssueBaseFluentInterface,IssueBase>(IssueBaseFluentInterface.class, IssueBase.class).getDaoInstance();
    }
}
