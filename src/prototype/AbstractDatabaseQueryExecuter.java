package prototype;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDatabaseQueryExecuter<T> {

    protected Class entityType;

    /**
     * Konstruktor, nimmt die Entitätenklassen als Parameter
     * entgegen.
     *
     * @param entityType
     */
    public AbstractDatabaseQueryExecuter(Class entityType){
        this.entityType = entityType;
    }

    protected abstract void getDatabaseConnection();
    protected abstract void closeDatabaseConnection();
    protected abstract List<T> executeQuery(Object[] args);

    /**
     * Ausführung aller drei abstrakter Methoden.
     * @param args
     * @return
     */
    public List<T> execute(Object[] args){
        getDatabaseConnection();
        List<T> resultList = executeQuery(args);
        closeDatabaseConnection();
        return resultList;
    }

    /**
     * Holt aus dem übergebenen Objekt die Id
     * @param entity Datentyp aus der Datenbank mit Id
     * @return id des Objektes
     */
    protected long getIdFromEntity(Object entity){
        long id=0;
        Field[] fieldNames = entity.getClass().getDeclaredFields();

        for (Field field : fieldNames)
            if (field.isAnnotationPresent(Id.class)) {
                boolean isAccessible = field.isAccessible();
                field.setAccessible(true);
                try {
                    id = (Long) field.get(entity);
                } catch (IllegalAccessException e) { e.printStackTrace(); }
                field.setAccessible(isAccessible);
                break;
            }

        return id;
    }
}
