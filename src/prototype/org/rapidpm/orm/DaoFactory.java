package prototype.org.rapidpm.orm;

/**
 * RapidPM - www.rapidpm.org
 * User: svenruppert
 * Date: 11.01.12
 * Time: 14:27
 * This is part of the RapidPM - www.rapidpm.org project. please contact sven.ruppert@neoscio.de
 */

import org.apache.log4j.Logger;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.RegistrationDAO;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.RegistrationStatusDAO;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.anfragen.KontaktAnfrageDAO;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.anfragen.ProjektanfrageDAO;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.msgcenter.MessageDAO;
import prototype.org.rapidpm.orm.prj.bewegungsdaten.msgcenter.msg.PersonalMessageDAO;
import prototype.org.rapidpm.orm.prj.book.BuchDAO;
import prototype.org.rapidpm.orm.prj.book.BuchKapitelDAO;
import prototype.org.rapidpm.orm.prj.book.BuchSeiteDAO;
import prototype.org.rapidpm.orm.prj.book.BuchSeitenFussnoteDAO;
import prototype.org.rapidpm.orm.prj.book.kommentar.BuchKapitelKommentarDAO;
import prototype.org.rapidpm.orm.prj.book.kommentar.BuchKommentarDAO;
import prototype.org.rapidpm.orm.prj.book.kommentar.BuchSeitenKommentarDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.ProjectDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.ProjectNameDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssueCommentDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssuePriorityDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssueStatusDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.IssueTimeUnitDAO;
import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBaseDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.organisationseinheit.metadata.AusbildungseinheitDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.organisationseinheit.metadata.VerwaltungseinheitDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.address.*;
import prototype.org.rapidpm.orm.prj.stammdaten.organisationseinheit.metadata.WirtschaftseinheitDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.web.WebDomainDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.web.WebDomainKlassifizierungDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.web.WebDomainMetaDataDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.person.*;
import prototype.org.rapidpm.orm.rohdaten.OntologieConnectionDAO;
import prototype.org.rapidpm.orm.rohdaten.OntologieDAO;
import prototype.org.rapidpm.orm.rohdaten.OntologieEntryDAO;
import prototype.org.rapidpm.orm.system.logging.LogginEntityActionDAO;
import prototype.org.rapidpm.orm.system.logging.LogginEntityEntryDAO;
import prototype.org.rapidpm.orm.system.logging.LoggingEventEntryDAO;
import prototype.org.rapidpm.orm.system.security.berechtigungen.BerechtigungDAO;
import prototype.org.rapidpm.orm.prj.stammdaten.kommunikation.*;
import prototype.org.rapidpm.orm.prj.stammdaten.organisationseinheit.*;
import prototype.org.rapidpm.orm.system.security.*;

public class DaoFactory extends BaseDaoFactory {
    private static final Logger logger = Logger.getLogger(DaoFactory.class);

    public DaoFactory(final String persistenceUnitName) {
        super(persistenceUnitName);
    }

    public DaoFactory() {
    }

    // pkg logging

    public LoggingEventEntryDAO getLoggingEventEntryDAO() {
        return new LoggingEventEntryDAO(getEntityManager());
    }

    public LogginEntityEntryDAO getLogginEntityEntryDAO() {
        return new LogginEntityEntryDAO(getEntityManager());
    }

    public LogginEntityActionDAO getLogginEntityActionDAO() {
        return new LogginEntityActionDAO(getEntityManager());
    }

    //pkg security

    public BenutzerGruppeDAO getBenutzerGruppeDAO() {
        return new BenutzerGruppeDAO(getEntityManager());
    }

    public BenutzerWebapplikationDAO getBenutzerWebapplikationDAO() {
        return new BenutzerWebapplikationDAO(getEntityManager());
    }

    public BenutzerDAO getBenutzerDAO() {
        return new BenutzerDAO(getEntityManager());
    }

    public MandantengruppeDAO getMandantengruppeDAO() {
        return new MandantengruppeDAO(getEntityManager());
    }


    public NewPasswdRequestDAO getNewPasswdRequestDAO() {
        return new NewPasswdRequestDAO(getEntityManager());
    }


    public BerechtigungDAO getBerechtigungDAO() {
        return new BerechtigungDAO(getEntityManager());
    }

    //pkg webapp


    public OntologieDAO getOntologieDAO() {
        return new OntologieDAO(getEntityManager());
    }

    public OntologieConnectionDAO getOntologieConnectionDAO() {
        return new OntologieConnectionDAO(getEntityManager());
    }

    public OntologieEntryDAO getOntologieEntryDAO() {
        return new OntologieEntryDAO(getEntityManager());
    }

    //pkg webapp

    public OrganisationseinheitDAO getOrganisationseinheitDAO() {
        return new OrganisationseinheitDAO(getEntityManager());
    }


    public WebDomainDAO getWebDomainDAO() {
        return new WebDomainDAO(getEntityManager());
    }

    // pkg address
    //    public AdressDAO getAdressDAO(){}

    public AddressKlassifizierungDAO getAddressKlassifizierungDAO() {
        return new AddressKlassifizierungDAO(getEntityManager());
    }

    public AdresseDAO getAdresseDAO() {
        return new AdresseDAO(getEntityManager());
    }


    public StateDAO getStateDAO() {
        return new StateDAO(getEntityManager());
    }

    public StateKlassifizierungDAO getStateKlassifizierungDAO() {
        return new StateKlassifizierungDAO(getEntityManager());
    }

    public LandDAO getLandDAO() {
        return new LandDAO(getEntityManager());
    }


    //book
    public BuchDAO getBuchDAO() {
        return new BuchDAO(getEntityManager());
    }

    public BuchKapitelDAO getBuchKapitelDAO() {
        return new BuchKapitelDAO(getEntityManager());
    }

    public BuchSeiteDAO getBuchSeiteDAO() {
        return new BuchSeiteDAO(getEntityManager());
    }

    public BuchSeitenFussnoteDAO getBuchSeitenFussnoteDAO() {
        return new BuchSeitenFussnoteDAO(getEntityManager());
    }

    public BuchKommentarDAO getBuchKommentarDAO() {
        return new BuchKommentarDAO(getEntityManager());
    }

    public BuchKapitelKommentarDAO getBuchKapitelKommentarDAO() {
        return new BuchKapitelKommentarDAO(getEntityManager());
    }

    public BuchSeitenKommentarDAO getBuchSeitenKommentarDAO() {
        return new BuchSeitenKommentarDAO(getEntityManager());
    }


    //IssueTracking

    public IssueBaseDAO getIssueBaseDAO() {
        return new IssueBaseDAO(getEntityManager());
    }

    public IssueCommentDAO getIssueCommentDAO() {
        return new IssueCommentDAO(getEntityManager());
    }

    public ProjectDAO getProjectDAO() {
        return new ProjectDAO(getEntityManager());
    }

    public IssueTimeUnitDAO getTimeUnitDAO() {
        return new IssueTimeUnitDAO(getEntityManager());
    }

    public IssuePriorityDAO getIssuePriorityDAO() {
        return new IssuePriorityDAO(getEntityManager());
    }

    public IssueStatusDAO getIssueStatusDAO() {
        return new IssueStatusDAO(getEntityManager());
    }

    public ProjectNameDAO getProjectNameDAO() {
        return new ProjectNameDAO(getEntityManager());
    }

    public IssueTimeUnitDAO getIssueTimeUnitDAO() {
        return new IssueTimeUnitDAO(getEntityManager());
    }


    //pkg Kommunikation

    public KommunikationsServiceDAO getKommunikationsServiceDAO() {
        return new KommunikationsServiceDAO(getEntityManager());
    }

    public KommunikationsServiceKlassifizierungDAO getKommunikationsServiceKlassifizierungDAO() {
        return new KommunikationsServiceKlassifizierungDAO(getEntityManager());
    }

    public KommunikationsServiceUIDDAO getKommunikationServiceUIDDAO() {
        return new KommunikationsServiceUIDDAO(getEntityManager());
    }

    public KommunikationsServiceUIDPartDAO getKommunikationServiceUIDPartDAO() {
        return new KommunikationsServiceUIDPartDAO(getEntityManager());
    }

    public KommunikationsServiceUIDPartKlassifikationDAO getKommunikationsServiceUIDPartKlassifikationDAO() {
        return new KommunikationsServiceUIDPartKlassifikationDAO(getEntityManager());
    }


    //pkg msgCenter

    public MessageDAO getMessageDAO() {
        return new MessageDAO(getEntityManager());
    }

    public PersonalMessageDAO getPersonalMessageDAO() {
        return new PersonalMessageDAO(getEntityManager());
    }

    //pkg organisationseinheiten

    public BrancheDAO getBrancheDAO() {
        return new BrancheDAO(getEntityManager());
    }

    public BrancheAssocDAO getBranchenAssocDAO() {
        return new BrancheAssocDAO(getEntityManager());
    }

    public BranchenKlassifizierungDAO getBranchenKlassifizierungDAO() {
        return new BranchenKlassifizierungDAO(getEntityManager());
    }

    public GesellschaftsformDAO getGesellschaftsformDAO() {
        return new GesellschaftsformDAO(getEntityManager());
    }

    public OrganisationseinheitMetaDataDAO getOrganisationseinheitMetaDataDAO() {
        return new OrganisationseinheitMetaDataDAO(getEntityManager());
    }

    public AusbildungseinheitDAO getAusbildungseinheitDAO() {
        return new AusbildungseinheitDAO(getEntityManager());
    }

    public VerwaltungseinheitDAO getVerwaltungseinheitDAO() {
        return new VerwaltungseinheitDAO(getEntityManager());
    }

    public WirtschaftseinheitDAO getWirtschaftseinheitDAO() {
        return new WirtschaftseinheitDAO(getEntityManager());
    }


    public TaetigkeitsfeldKlassifizierungDAO getTaetigkeitsklassifizierungDAO() {
        return new TaetigkeitsfeldKlassifizierungDAO(getEntityManager());
    }

    public TaetigkeitsfeldDAO getTaetigkeitsfeldDAO() {
        return new TaetigkeitsfeldDAO(getEntityManager());
    }

    public TaetigkeitsfeldAssocDAO getTaetigkeitsfeldAssocDAO() {
        return new TaetigkeitsfeldAssocDAO(getEntityManager());
    }

    public PositionDAO getPositionDAO() {
        return new PositionDAO(getEntityManager());
    }

    //pkg person

    public PersonDAO getPersonDAO() {
        return new PersonDAO(getEntityManager());
    }

    public PersonenNameDAO getPersonenNameDAO() {
        return new PersonenNameDAO(getEntityManager());
    }

    public AnredeDAO getAnredeDAO() {
        return new AnredeDAO(getEntityManager());
    }

    public GeschlechtDAO getGeschlechtDAO() {
        return new GeschlechtDAO(getEntityManager());
    }

    public NamensKlassifizierungDAO getNamensKlassifizierungDAO() {
        return new NamensKlassifizierungDAO(getEntityManager());
    }

    public TitelDAO getTitelDAO() {
        return new TitelDAO(getEntityManager());
    }


    //pgk web//webdomains

    public WebDomainKlassifizierungDAO getWebDomainKlassifizierungDAO() {
        return new WebDomainKlassifizierungDAO(getEntityManager());
    }

    public WebDomainMetaDataDAO getWebDomainMetaDataDAO() {
        return new WebDomainMetaDataDAO(getEntityManager());
    }


    //pkg Bewegungsdaten
    public RegistrationDAO getRegistrationDAO() {
        return new RegistrationDAO(getEntityManager());
    }

    public RegistrationStatusDAO getRegistrationStatusDAO() {
        return new RegistrationStatusDAO(getEntityManager());
    }

    public KontaktAnfrageDAO getKontaktAnfrageDAO() {
        return new KontaktAnfrageDAO(getEntityManager());
    }

    public ProjektanfrageDAO getProjektanfrageDAO() {
        return new ProjektanfrageDAO(getEntityManager());
    }

}
