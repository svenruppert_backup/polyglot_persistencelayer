package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractJdbcExecuter;
import javax.persistence.Basic;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 21.08.12
 * Time: 10:28
 * To change this template use File | Settings | File Templates.
 */

/**
 * Create Methode.
 * Erstellt einen neuen Datensatz in der Datenbank.
 *
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public class Create<T> extends AbstractJdbcExecuter<T>{

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public Create(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = Objekte welches in der Datenbank erstellt werden soll.
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 1.
     *
     * @throws NullPointerException
     *              Wird geworfen wenn null als Objekt übergeben wird.
     *
     * @return Eine leere Arraylist wird bei Erfolg zurückgegeben.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if (args == null || args.length != 1)
            throw new IllegalArgumentException("Error: Wrong parameter count! One argument has to be passed!");

        T entity = (T) args[0];

        if(entity == null)
            throw new NullPointerException("Could not cast entity to entitytype");

        int i=0;
        Field[] fieldNames = entityType.getDeclaredFields();
        Object[] fieldValues = new Object[fieldNames.length];
        Field[] usedFields = new Field[fieldNames.length];

        for (Field field : fieldNames) {

            if (field.isAnnotationPresent(Basic.class) && field.getName() != "fakturierbar") {
                boolean isAccessible = field.isAccessible();
                field.setAccessible(true);
                try {
                    Object tmp = field.get(entity);
                    if (tmp == null) tmp = "NULL";
                    fieldValues[i] = tmp;
                    usedFields[i++] = field;
                } catch (IllegalAccessException e) { e.printStackTrace(); }
                field.setAccessible(isAccessible);
            }
        }

        Statement statement = null;
        String columns = "", values="";
        for (int j=0; j < i; j++){
            if (j != 0) {
                columns += ", ";
                values += ", ";
            }
            columns = columns + usedFields[j].getName();
            if (Number.class.isAssignableFrom(usedFields[j].getType()) ||
                    fieldValues[j].toString() == "NULL") {
                values += fieldValues[j].toString();
            } else {

                values += "'" + fieldValues[j].toString() + "'";
            }
        }

        try {
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO Issue (" + columns + ") VALUES ( " + values + ");");
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                connection.rollback();
                connection.setAutoCommit(true);
            } catch (SQLException e1) { e1.printStackTrace(); }
            return null;
        }
        return new ArrayList<T>();
    }

}
