package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking;

import prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.type.IssueBase;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 20.08.12
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
public interface IssueBaseFluentInterface {
    public List<IssueBase> getAll();
    public List<IssueBase> getById(long id);
    public void setAsDuplicate(IssueBase e1, IssueBase e2);
    public List<IssueBase> getDuplicates(IssueBase e1);

    public void delete(IssueBase issue);
    public void create(IssueBase issue);
}
