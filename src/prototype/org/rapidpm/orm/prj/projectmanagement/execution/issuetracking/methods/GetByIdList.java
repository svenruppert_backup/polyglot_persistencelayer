package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractJdbcExecuter;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/22/12
 * Time: 7:28 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt alle Objekte mittles der ID aus der übergebenen Liste.
 *
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public class GetByIdList<T> extends AbstractJdbcExecuter<T> {

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public GetByIdList(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = List der Ids
     *
     * @return Gibt die Liste der Ergebnisse zurück.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        Statement statement = null;
        List<Long> idList = (List<Long>) args[0];
        if (idList.size() == 0)
            return resultList;

        try {
            statement = connection.createStatement();
            StringBuilder idListBuilder = new StringBuilder();
            int listSize = idList.size();
            for(int i=0; i< listSize; i++)
                idListBuilder.append(idList.get(i)).append(",");

            String idListString = idListBuilder.substring(0, idListBuilder.lastIndexOf(","));
            resultSet = statement.executeQuery("SELECT * FROM `Issue` WHERE id IN (" + idListString + ");");

            while(resultSet.next()){
                try {
                    T resultObject = (T) entityType.newInstance();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {

                        Field field = entityType.getDeclaredField(resultSet.getMetaData().getColumnName(i));
                        boolean isAccessible = field.isAccessible();
                        field.setAccessible(true);

                        Object fieldValue = resultSet.getObject(field.getName());
                        field.set(resultObject, fieldValue);
                        field.setAccessible(isAccessible);
                    }
                    resultList.add(resultObject);
                } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) { e.printStackTrace(); }
            }

        } catch (SQLException e) { e.printStackTrace(); }

        return resultList;
    }
}

