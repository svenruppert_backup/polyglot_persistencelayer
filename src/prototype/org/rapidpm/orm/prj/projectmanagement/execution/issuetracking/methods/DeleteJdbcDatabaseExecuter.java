package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractJdbcExecuter;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/22/12
 * Time: 5:47 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Executer für die Delete Methode.
 * Prüft vor dem Löschen aus der relationalen, ob Verbindungen in der Graphendatenbank existieren und löscht diese.
 */
public class DeleteJdbcDatabaseExecuter extends AbstractJdbcExecuter<Boolean> {

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public DeleteJdbcDatabaseExecuter(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = Objekte welches aus der Datenbank gelöscht werden soll.
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 1.
     *
     * @throws NullPointerException
     *              Wird geworfen wenn null als Objekt übergeben wird.
     *
     * @return Eine leere Arraylist wird bei Erfolg zurückgegeben.
     */
    @Override
    protected List<Boolean> executeQuery(Object[] args) {
        if (args == null || args.length != 1)
            throw new IllegalArgumentException("Wrong parameter count! Only one parameter valid");

        if (args[0] == null)
            throw new NullPointerException("Parameter must not be null!");

        Statement statement = null;
        long id = getIdFromEntity( args[0] );
        List<Boolean> success = new ArrayList<Boolean>();

        try {
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.execute("DELETE FROM Issue WHERE id=" + id + ";");
            connection.commit();
            connection.setAutoCommit(true);
            success.add(true);
        } catch (SQLException e) {
            try {
                success.add(false);
                e.printStackTrace();
                connection.rollback();
                connection.setAutoCommit(true);
            } catch (SQLException e1) { e.printStackTrace(); }
        }
        return success;
    }
}

