package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractGraphExecuter;
import org.neo4j.graphdb.*;
import prototype.RelationShipFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/21/12
 * Time: 10:24 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt alle Objekte die als Duplikate des gegebenen Objektes gekennzeichnet sind.
 *
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public class GetDuplicates<T> extends AbstractGraphExecuter<T>{

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public GetDuplicates(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Darf keine Elemente enthalten.
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 1.
     *
     * @return Gibt eine Liste der Ergebnisse zurück.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if (args == null || args.length != 1)
            throw new IllegalArgumentException("Only one entitiy must be given.");

        T rootEntity = (T) args[0];

        if (rootEntity == null)
            throw new NullPointerException("The given Entity is null.");

        DynamicRelationshipType duplicateRelationShip = new RelationShipFactory().getDuplicateRelationShipForType(entityType);
        long rootNodeId = getIdFromEntity(rootEntity);

        List<Long> duplicatesIdList = null;
        Transaction tx = graphDbService.beginTx();
        try{
            Node rootNode = graphDbUtilities.getByIndexOrCreate(entityType.getSimpleName(), rootNodeId, graphDbService);
            duplicatesIdList = getDuplicates(rootNode, rootNode, duplicateRelationShip);
        }
        finally{
            tx.finish();
        }

        Object[] newArgs = {duplicatesIdList};
        return new GetByIdList<T>(entityType).execute(newArgs);
    }


    /**
     * Holt alle Objekte aus der Datenbank, die mit der gegebene relation mit dem Parentknoten verbunden sind.
     *
     * @param rootNode
     * @param parentNode    Knoten von dem aus gesucht werden soll.
     * @param relationShip  Relationstyp nach dem gescuht werden soll.
     * @return Gibt eine List der Ids der Ergebnissobjekte zurück.
     */
    private List<Long> getDuplicates(Node rootNode, Node parentNode,  DynamicRelationshipType relationShip){
        List<Long> idList = new ArrayList<>();
        for(Relationship relationshipType : rootNode.getRelationships(relationShip, Direction.BOTH)){
            Node relatedNode = null;
            if(relationshipType.getEndNode().equals(rootNode))
                relatedNode = relationshipType.getStartNode();
            else
                relatedNode = relationshipType.getEndNode();

            if(!relatedNode.equals(parentNode)){
                long id = (Long) relatedNode.getProperty("id");
                idList.add(id);
                List<Long> duplicatesOfDuplicates = getDuplicates(relatedNode,rootNode, relationShip);
                idList.addAll(duplicatesOfDuplicates);
            }
        }

        return idList;
    }
}



