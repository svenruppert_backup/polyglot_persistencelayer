package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractJdbcExecuter;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 25.07.12
 * Time: 11:20
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt alle Datensätze aus der Datenbank.
 * @param <T>   Datentyp für den die Methode ausgeführt werden soll.
 */
public class GetAll<T> extends AbstractJdbcExecuter<T> {

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public GetAll(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Darf keine Elemente enthalten.
     *
     * @throws IllegalArgumentException
     *              Wird geworfen wenn ein Objekt übergeben wird.
     *
     * @return Gibt das Ergebnis als Liste mit einem Objekt zurück.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if (args != null)
            throw new IllegalArgumentException("This method has no parameters.");


        Statement statement = null;
        try {
            statement = connection.createStatement();

            String tableName = "issue";
            //Field[] columnNames = null;// = entityType.getDeclaredFields();
            resultSet = statement.executeQuery("SELECT * FROM  Issue");

            while(resultSet.next()){
                try {
                    T resultObject = (T) entityType.newInstance();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {

                        Field field = entityType.getDeclaredField(resultSet.getMetaData().getColumnName(i));
                        boolean isAccessible = field.isAccessible();
                        field.setAccessible(true);

                        Object fieldValue = resultSet.getObject(field.getName());
                        field.set(resultObject, fieldValue);
                        field.setAccessible(isAccessible);
                    }
                    resultList.add(resultObject);
                } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) { e.printStackTrace(); }
            }

        } catch (SQLException e) { e.printStackTrace(); }

        return resultList;
    }
}
