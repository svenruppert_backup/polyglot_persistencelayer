package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import performancetests.GraphServiceFactory;
import prototype.AbstractGraphExecuter;
import org.neo4j.graphdb.*;
import prototype.GraphDbUtillityMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 20.08.12
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */

/**
 * Delete Methode.
 * Löscht einen Datensatz aus der Datenbank.
 *
 * @param <T>  Datentyp für den die Methode ausgeführt werden soll.
 */
public class Delete<T> extends AbstractGraphExecuter<T> {
    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public Delete(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = Objekte welches aus der Datenbank gelöscht werden soll.
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 1.
     *
     * @throws NullPointerException
     *              Wird geworfen wenn null als Objekt übergeben wird.
     *
     * @return Bei Erfolg wird null zurückgegeben.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if(args == null || args.length != 1)
            throw new IllegalArgumentException("Method : List<T> delete(T t) - wrong parameter count.");

        T entity = (T) args[0];

        if(entity == null)
            throw new NullPointerException("The entity which should be deleted was null.");

        String className = entityType.getSimpleName();
        GraphDbUtillityMethods graphUtils = new GraphDbUtillityMethods();
        long id = getIdFromEntity(entity);
        if(new DeleteJdbcDatabaseExecuter(entityType).execute(args).get(0)){
            Transaction tx = graphDbService.beginTx();
            try{
                Node nodeToDelete = graphUtils.getByIndexOrCreate(className, id, graphDbService);
                if (nodeToDelete != null){
                    graphDbUtilities.safeDeleteNode(nodeToDelete);
                    tx.success();
                }
            }
            finally {
                tx.finish();
                GraphServiceFactory.shutDownDatabase(GraphServiceFactory.ISSUE_TRACKING);
                return new ArrayList<>();
            }
        }
        return null;
    }

}
