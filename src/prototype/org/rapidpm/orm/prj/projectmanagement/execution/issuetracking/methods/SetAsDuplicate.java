package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractGraphExecuter;
import org.neo4j.graphdb.*;
import prototype.RelationShipFactory;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniel MacDonald
 * Date: 8/20/12
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Verbindet zwei Objekte als Duplikate.
 *
 * @param <T>   Datentyp für den die Methode ausgeführt werden soll.
 */
public class SetAsDuplicate<T> extends AbstractGraphExecuter<T> {

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public SetAsDuplicate(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = Erstes Objekt das verbunden werden soll.
     *             Index 1 = Zweites Objekt das verbunden werden soll.
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 2.
     *
     * @throws NullPointerException
     *              Wird geworfen wenn eines der übergebenen Objekte null ist.
     *
     *
     * @return Bei Erfolg wird null zurückgegeben.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if (args == null || args.length != 2)
            throw new IllegalArgumentException("To set Duplicates two Entities are needed.");

        T duplicate1 = (T) args[0];
        T duplicate2 = (T) args[1];

        if (duplicate1 == null || duplicate2 == null)
            throw new NullPointerException("To set Duplicates two Entities are needed.");

        if (duplicate1 == duplicate2)
            throw new IllegalArgumentException("An object can not be an duplicate of itself.");

        DynamicRelationshipType duplicateRelationShip = new RelationShipFactory().getDuplicateRelationShipForType(entityType);

        long duplicate1Id = getIdFromEntity(args[0]);
        long duplicate2Id = getIdFromEntity(args[1]);

        String className = entityType.getSimpleName();
        Transaction tx = graphDbService.beginTx();
        try{
            Node firstDuplicate = graphDbUtilities.getByIndexOrCreate(className, duplicate1Id, graphDbService);
            Node secondDuplicate = graphDbUtilities.getByIndexOrCreate(className, duplicate2Id, graphDbService);

            Node firstRootNode = graphDbUtilities.getRootNode(firstDuplicate, duplicateRelationShip);
            Node secondRootNode = graphDbUtilities.getRootNode(secondDuplicate, duplicateRelationShip);


            if(firstRootNode == null && secondRootNode == null || firstRootNode != null && secondRootNode == null)
                firstDuplicate.createRelationshipTo(secondDuplicate, duplicateRelationShip);
            else if(firstRootNode == null && secondRootNode != null)
                secondDuplicate.createRelationshipTo(firstDuplicate, duplicateRelationShip);
            else if(firstRootNode != null && secondRootNode != null && !firstRootNode.equals(secondRootNode))
                firstRootNode.createRelationshipTo(secondRootNode, duplicateRelationShip);


            tx.success();
        }
        finally{
            tx.finish();
        }

        return null;
    }

    
}
