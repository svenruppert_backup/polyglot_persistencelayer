package prototype.org.rapidpm.orm.prj.projectmanagement.execution.issuetracking.methods;

import prototype.AbstractJdbcExecuter;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Alvin Schiller
 * Date: 20.08.12
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */

/**
 * Holt den entsprechenden Datensatz aus der Datenbank.
 * @param <T>   Datentyp für den die Methode ausgeführt werden soll.
 */
public class GetById<T>  extends AbstractJdbcExecuter<T> {

    /**
     * Konstruktor
     * @param entityType Klassenobjekt von T
     */
    public GetById(Class entityType) {
        super(entityType);
    }

    /**
     * Überschriebene executeQuery Methode des Executers.
     *
     * @param args Index 0 = Id der gewünschten Datensatzes
     *
     * @throws IllegalArgumentException
     *              Ist args ein null Objekt oder die Anzahl an Objekten ist ungleich 1.
     *
     * @return Gibt die Liste der Ergebnisse zurück.
     */
    @Override
    protected List<T> executeQuery(Object[] args) {
        if (args == null || args.length != 1)
            throw new IllegalArgumentException("Only one parameter must be given.");

        Statement statement = null;
        try {
            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT * FROM  Issue WHERE id= " + (Long) args[0] );
            while(resultSet.next()){
                try {
                    T resultObject = (T) entityType.newInstance();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {

                        Field field = entityType.getDeclaredField(resultSet.getMetaData().getColumnName(i));
                        boolean isAccessible = field.isAccessible();
                        field.setAccessible(true);

                        Object fieldValue = resultSet.getObject(field.getName());
                        field.set(resultObject, fieldValue);
                        field.setAccessible(isAccessible);
                    }
                    resultList.add(resultObject);
                } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) { e.printStackTrace(); }
            }

        } catch (SQLException e) { e.printStackTrace(); }

        return resultList;
    }

}
